<apex:page standardController="Practice_Location__c" docType="html-5.0"
           applyHtmlTag="false" showHeader="false" sideBar="false"
           title="PATS - My Details"
           extensions="PracticeLocationController">
    <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <apex:composition template="{!$Site.Template}">
        <apex:define name="pagetitle">PATS - Practice Location Edit: {!IF(ISBLANK(Practice_Location__c.Name), 'New Practice Location', Practice_Location__c.Name)}</apex:define>

        <apex:define name="pageheader">
            <div class="slds-page-header" role="banner">
                <div class="slds-grid">
                    <div class="slds-col slds-has-flexi-truncate">
                        <div class="slds-media">
                            <div class="slds-media__figure">
                                <svg aria-hidden="true" class="slds-icon slds-icon--large slds-icon-standard-account">
                                    <use xlink:href="{!URLFOR($Resource.SLDS102,'/assets/icons/standard-sprite/svg/symbols.svg#account')}"></use>
                                </svg>
                            </div>
                            <div class="slds-media__body">
                                <p class="slds-text-heading--label">Practice Location Edit</p>
                                <div class="slds-grid">
                                    <h1 class="slds-page-header__title slds-m-right--small slds-truncate slds-align-middle"
                                        title="Record Title">{!IF(ISBLANK(Practice_Location__c.Name), 'New Practice Location', Practice_Location__c.Name)}</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </apex:define>

        <apex:define name="body">
            <apex:form >
                <div class="slds-p-around--small">
                    <fieldset class="slds-form--compound">
                        <legend class="slds-form-element__label">Address</legend>
                        <div class="form-element__group">
                            <div class="slds-form-element__row">
                                <div class="slds-form-element slds-size--1-of-1">
                                    <label class="slds-form-element__label" for="street">{!$ObjectType.Practice_Location__c.fields.Street__c.Label}</label>
                                    <apex:inputField value="{!Practice_Location__c.Street__c}" styleClass="slds-input" html-placeholder="Street" id="street"/>
                                </div>
                            </div>
                            <div class="slds-form-element__row">
                                <div class="slds-form-element slds-size--1-of-2">
                                    <label class="slds-form-element__label" for="city">{!$ObjectType.Practice_Location__c.fields.City__c.Label}</label>
                                    <apex:inputField value="{!Practice_Location__c.City__c}" styleClass="slds-input" html-placeholder="City" id="city"/>
                                </div>
                                <div class="slds-form-element slds-size--1-of-2">
                                    <label class="slds-form-element__label" for="state">{!$ObjectType.Practice_Location__c.fields.State__c.Label}</label>
                                    <apex:inputField value="{!Practice_Location__c.State__c}" styleClass="slds-select" html-placeholder="State" id="state"/>
                                </div>
                            </div>
                            <div class="slds-form-element__row">
                                <div class="slds-form-element slds-size--1-of-2">
                                    <label class="slds-form-element__label" for="postcode">{!$ObjectType.Practice_Location__c.fields.Postcode__c.Label}</label>
                                    <apex:inputField value="{!Practice_Location__c.Postcode__c}" styleClass="slds-input" html-placeholder="Postcode" id="postcode"/>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="slds-text-align--center">
                        <apex:commandButton styleClass="slds-button slds-button--neutral" value="Cancel" action="{!cancel}" id="cancelButton" />
                        <apex:outputPanel layout="none" rendered="{!$ObjectType.Practice_Location__c.updateable}">
                            <apex:commandButton styleClass="slds-button slds-button--brand" value="Save" action="{!save}" id="saveButton"/>
                        </apex:outputPanel>
                    </div>

                    <div class="slds-hide">


                    </div>
                </div>
            </apex:form>
        </apex:define>

        <apex:define name="scripts">
            <script>
                $(function () {

                    $('body').notification();
                    $('[data-aljs="popover"]').popover({
                        modifier:'tooltip'
                    });
                });
            </script>
        </apex:define>
    </apex:composition>
    </html>
</apex:page>