<!--
 - Created by Enrite Solutions on 13/02/2018.
 -->

<apex:page id="ThirdPartyUploadInvoice" standardcontroller="Claim__c" extensions="ClaimExtension" doctype="html-5.0"
           applyhtmltag="false" showheader="false" sidebar="false"
           title="PATS - Upload Invoice"
           >
    <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <style>
        .slds-form-element__label {
            margin-right: 0px !important;
        }

        .slds-checkbox span.slds-form-element__label {
            text-transform: none;
        }
        .pbHeader{
            display: none;
        }

        #trigger-upload {
            color: white;
            background-color: #00ABC7;
            font-size: 14px;
            padding: 7px 20px;
            background-image: none;
        }

        #fine-uploader-manual-trigger .qq-upload-button {
            margin-right: 15px;
        }

        #fine-uploader-manual-trigger .buttons {
            width: 36%;
        }

        #fine-uploader-manual-trigger .qq-uploader .qq-total-progress-bar-container {
            width: 60%;
        }
        button#trigger-upload, button.qq-upload-cancel-selector, span.qq-edit-filename-icon-selector {
            display: none !important;
        }
    </style>
    <apex:variable var="acceptFiles" value="{!'.txt,.text,.csv,.pdf,.doc,.docx,.xls,.xlsx,.ppt,.pptx,.eml,.msg,.rtf,.jpg,.jpeg,.png,.gif,.bmp'}" />
    <apex:variable var="acceptTypes" value="{!'text/plain,text/csv,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,message/rfc822,application/rtf,image/jpeg,image/png,image/gif,image/bmp'}" />
    <apex:stylesheet value="{!URLFOR($Resource.FileUploader, 'fine-uploader-new.min.css')}" />
    <link href="//code.jquery.com/ui/1.10.3/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css"/>

    <apex:composition template="{!$Site.Template}">

        <apex:define name="pagetitle">PATS - Upload Invoice</apex:define>
        <apex:define name="pageheader">
            <div class="slds-page-header" role="banner">
                <div class="slds-grid">
                    <div class="slds-col--padded slds-has-flexi-truncate">
                        <div class="slds-media">
                            <div class="slds-media__figure">
                                <svg aria-hidden="true" class="slds-icon slds-icon--large slds-icon-standard-user">
                                    <use xlink:href="{!URLFOR($Resource.SLDS102,'/assets/icons/standard-sprite/svg/symbols.svg#contact')}"></use>
                                </svg>
                            </div>
                            <div class="slds-media__body">
                                <p class="slds-text-heading--label">PATS Claim- UPLOAD INVOICE</p>
                                <div class="slds-grid">
                                    <h1 class="slds-page-header__title slds-m-right--small slds-truncate slds-align-middle"
                                        title="Record Title">
                                        {!Claim__c.Name}
                                    </h1>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </apex:define>
        <apex:define name="body">

            <apex:outputpanel layout="block" styleclass="slds-notify_container" rendered="{!HasMessages}">
                <div id="errMsg" class="slds-notify slds-notify--toast slds-theme--error" role="alert">
                    <span class="slds-assistive-text">Info</span>
                    <button class="slds-button slds-button--icon-inverse slds-notify__close" data-aljs-dismiss="notification">
                        <svg aria-hidden="true" class="slds-button__icon" >
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Resource.SLDS102, '/assets/icons/action-sprite/svg/symbols.svg#close')}"></use>
                        </svg>
                        <span class="slds-assistive-text">Close</span>
                    </button>
                    <div class="notify__content slds-grid">
                        <svg aria-hidden="true" class="slds-icon slds-icon--small slds-m-right--small slds-col slds-no-flex">
                            <use xlink:href="{!URLFOR($Resource.SLDS102, '/assets/icons/utility-sprite/svg/symbols.svg#warning')}"></use>
                        </svg>
                        <div id="errList" class="slds-col--padded slds-align-middle">
                            <h2 class="slds-text-heading--small"><apex:messages /></h2>
                        </div>
                    </div>
                </div>
            </apex:outputpanel>

    <apex:form enctype="application/x-www-form-urlencoded">
            <apex:outputpanel layout="block" styleclass="slds-p-around--medium"
                          >
            <apex:commandbutton styleclass="slds-button slds-button--brand"
                                value="Upload Invoice"
                                action="{!uploadInvoice}"
                                id="saveButton2" />
        </apex:outputpanel>
        <apex:outputpanel layout="block" styleclass="slds-grid slds-grid--pull-padded slds-wrap slds-p-around--medium" >
            <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1">
                <label class="slds-form-element__label" for="{!$Component.uplFile}">Select a File</label>
                <div class="slds-form-element__control slds-size--1-of-1 slds-medium-size--1-of-2">
                    <apex:inputfile contentType="{!ContentType}" value="{!FileBody}" filename="{!FileName}" id="uplFile" />
                </div>
            </div>
        </apex:outputpanel>

    <apex:outputpanel layout="block" styleclass="slds-grid slds-grid--pull-padded slds-wrap" >
        <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1 slds-p-around--medium">
            <script type="text/template" id="qq-template">
                <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="Drop files here">
                    <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
                    </div>
                    <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                        <span class="qq-upload-drop-area-text-selector"></span>
                    </div>
                    <div class="buttons">
                        <div class="qq-upload-button-selector qq-upload-button">
                            <div>Select files</div>
                        </div>
                        <button type="button" id="trigger-upload" class="btn btn-primary">
                            <i class="icon-upload icon-white"></i> Upload
                        </button>
                    </div>
                    <span class="qq-drop-processing-selector qq-drop-processing">
                                    <span>Processing dropped files...</span>
                                    <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
                                </span>
                    <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
                        <li>
                            <div class="qq-progress-bar-container-selector">
                                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                            </div>
                            <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                            <img class="qq-thumbnail-selector" qq-max-size="50" qq-server-scale="" />
                            <span class="qq-upload-file-selector qq-upload-file"></span>
                            <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                            <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text" />
                            <span class="qq-upload-size-selector qq-upload-size"></span>
                            <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">Cancel</button>
                            <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Retry</button>
                            <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Delete</button>
                            <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                        </li>
                    </ul>
                    <dialog class="qq-alert-dialog-selector">
                        <div class="qq-dialog-message-selector"></div>
                        <div class="qq-dialog-buttons">
                            <button type="button" class="qq-cancel-button-selector">Close</button>
                        </div>
                    </dialog>
                    <dialog class="qq-confirm-dialog-selector">
                        <div class="qq-dialog-message-selector"></div>
                        <div class="qq-dialog-buttons">
                            <button type="button" class="qq-cancel-button-selector">No</button>
                            <button type="button" class="qq-ok-button-selector">Yes</button>
                        </div>
                    </dialog>
                    <dialog class="qq-prompt-dialog-selector">
                        <div class="qq-dialog-message-selector"></div>
                        <input type="text" />
                        <div class="qq-dialog-buttons">
                            <button type="button" class="qq-cancel-button-selector">Cancel</button>
                            <button type="button" class="qq-ok-button-selector">Ok</button>
                        </div>
                    </dialog>
                </div>
            </script>
            <div id="fine-uploader"></div>
        </div>
    </apex:outputpanel>
    </apex:form>
        </apex:define>
    </apex:composition>
    </html>
</apex:page>