<apex:page showheader="false" standardstylesheets="false" sidebar="false" applyhtmltag="false" applybodytag="false"
           doctype="html-5.0" html-lang="en" controller="PortalTemplateController">
    <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <head>
        <title><apex:insert name="pagetitle">PATS</apex:insert></title>
        <meta name="viewport" content="initial-scale=1, maximum-scale=1" />
        <apex:includescript value="//code.jquery.com/jquery-1.11.3.min.js" />
        <apex:includescript value="{!URLFOR($Resource.moment)}" />
        <apex:includescript value="{!URLFOR($Resource.aljs, '/jquery/jquery.aljs-all.min.js')}" />
        <apex:stylesheet value="{!URLFOR($Resource.SLDS102, 'assets/styles/salesforce-lightning-design-system-vf.css')}" />
        <style>
            body {
                margin: 0px;
                background-color: #f4f6f9;
            }

            .menu {
                background-color: #01344E;
            }

            .menu > ul > li > a:link {
                display: block;
                padding: 8px;
            }

            .menu > ul > li > a:hover, .menu > ul > li > a.selected {
                /*background-color: rgba(6, 28, 63, 1);*/
                background-color: #0070d2;
            }

            .menu > ul > li > a:link,
            .menu > ul > li > a:visited {
                color: #eee;
            }

            .slds-modal__container {
                width: 80%;
                max-width: 80%;
            }

            .menu.slds-grid {
                color: #fff;
            }


            .menu .slds-dropdown-trigger {
                color: #000;
            }

            .popover-icon {
                position: absolute !important;
                text-align: left !important;
                width: 20rem !important;
                left: -154px !important;
                top: -42px !important;
            }

            .slds-page-header li .slds-text-heading--label {
                display: inline-block;
            }

            .slds {
                background: #fff !important;
            }

            img.profile {
                border-radius: 50% !important;
                width: 2rem;
                height: 2rem;
                margin: .5rem;
            }

            img.profile--large {
                width: auto;
                height: auto;
            }


            .slds ::-webkit-input-placeholder {
                color: #C7C7CD !important;
            }

            .slds ::-moz-placeholder {
                color: #C7C7CD !important;
            }

            .slds :-ms-input-placeholder {
                color: #C7C7CD !important;
            }

            .fieldset--border {
                background-color: white;
                color: #16325c;
                border: 1px solid #d8dde6 !important;
                border-radius: 4px;
                width: 100%;
                transition: border 0.1s linear, background-color 0.1s linear;
                display: inline-block;
                padding: 0 16px 0 12px !important;
                line-height: 34px;
                min-height: calc(34px + 2px);
            }

            input, select, textarea {
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
            }

            select::-ms-expand {
                display: none;
            }
            .slds-notify--toast {
                width:75%;
            }
            .slds-spinner_container {
                position: fixed !important;
                z-index: 10000;
            }


            .message {
                background-color: #c23934;
                color: white;
                border: none;
            }

            img.msgIcon {
                display: none;
            }

            .message .messageText {
                margin-left: 7px;
                color: #FFF;
            }

            .messageText span {
                display: none;
            }

            .messageTable td {
                color: #fff;
            }
        </style>
    </head>
    <body lang="en">
    <!-- REQUIRED SLDS WRAPPER -->
    <div class="slds">
        <div id="spinner" class="slds-spinner_container" style="display: none;">
            <div class="slds-spinner--brand slds-spinner slds-spinner--large" aria-hidden="false" role="alert">
                <div class="slds-spinner__dot-a"></div>
                <div class="slds-spinner__dot-b"></div>
            </div>
        </div>
        <!-- MASTHEAD -->
        <div class="menu slds-grid">
            <apex:outputpanel rendered="{!!ISPICKVAL($User.UserType, 'Guest')}" layout="none">
                <ul class="slds-list--horizontal slds-col slds-size--1-of-2 slds-medium-size--5-of-6 slds-large-size--11-of-12">
                    <apex:outputpanel layout="none" rendered="{!Tabs['MyDetails']}">
                        <li class="slds-list__item">
                            <a href="{!$Page.MyDetails}">
                                <svg aria-hidden="true" class="slds-icon">
                                    <use xlink:href="{!URLFOR($Resource.SLDS102,
                                            'assets/icons/standard-sprite/svg/symbols.svg#contact')}"></use>
                                </svg>
                                <span class="slds-assistive-text">My Details Icon</span>My Details
                            </a>
                        </li>
                    </apex:outputpanel>
                    <apex:outputpanel layout="none" rendered="{!Tabs['ClientSearch']}">
                        <li class="slds-list__item">
                            <a href="{!$Page.ClientSearch}">
                                <svg aria-hidden="true" class="slds-icon">
                                    <use xlink:href="{!URLFOR($Resource.SLDS102,
                                            'assets/icons/standard-sprite/svg/symbols.svg#client')}"></use>
                                </svg>
                                <span class="slds-assistive-text">Client Search</span>Client Search
                            </a>
                        </li>
                    </apex:outputpanel>
                    <apex:outputpanel layout="none" rendered="{!Tabs['MyApplications']}">
                        <li class="slds-list__item">
                            <a href="{!$Page.MyApplications}">
                                <svg aria-hidden="true" class="slds-icon">
                                    <use xlink:href="{!URLFOR($Resource.SLDS102,
                                            'assets/icons/standard-sprite/svg/symbols.svg#drafts')}"></use>
                                </svg>
                                <span class="slds-assistive-text">My Claims Icon</span><apex:outputText escape="false" value="{!IF($Profile.Name == 'Customer Community - Third Party', CurrentContactAccountName + '&apos;s', 'My')}"/> Claims
                            </a>
                        </li>
                    </apex:outputpanel>
                    <apex:outputpanel layout="none" rendered="{!Tabs['Notes']}">
                        <li class="slds-list__item">
                            <a href="{!$Page.Notes}">
                                <svg aria-hidden="true" class="slds-icon">
                                    <use xlink:href="{!URLFOR($Resource.SLDS102,
                                            'assets/icons/standard-sprite/svg/symbols.svg#note')}"></use>
                                </svg>
                                <span class="slds-assistive-text">Notes Icon</span>Notes
                            </a>
                        </li>
                    </apex:outputpanel>
                </ul>
                <div class="slds-dropdown-trigger slds-dropdown-trigger--click slds-align-middle slds-m-right--large" data-aljs="menu" aria-expanded="false">
                    <a href="#profile" class="slds-button" aria-haspopup="true" title="View Profile"><img src="{!userThumbPhoto}" class="profile" title="View Profile" /></a>
                    <div class="slds-dropdown slds-dropdown--right slds-dropdown--small slds-nubbin--top-right">
                        <div class="slds-grid">
                            <img src="{!userThumbPhoto}" class="profile profile--large" title="{!($User.FirstName + ' ' + $User.LastName)}" />
                            <div class="slds-col slds-align-middle slds-text-heading--small">{!($User.FirstName + ' ' + $User.LastName)}</div>
                        </div>
                        <ul class="dropdown__list slds-theme--shade slds-has-divider--top-space" role="menu">
                            <li class="slds-dropdown__item">
                                <a href="/ChangePassword" role="menuitem">
                                    <p>Change Password</p>
                                </a>
                            </li>
                            <li class="slds-dropdown__item">
                                <a href="/secur/logout.jsp" role="menuitem">
                                    <p>Logout</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </apex:outputpanel>
        </div>
        <!-- / MASTHEAD -->
        <!-- PAGE HEADER -->
        <apex:insert name="pageheader">
        </apex:insert>
        <!-- / PAGE HEADER -->
        <!-- PRIMARY CONTENT WRAPPER -->
        <div class="myapp slds-p-around--small">
            <apex:insert name="body" />
        </div>
        <!-- / PRIMARY CONTENT WRAPPER -->
        <!-- FOOTER -->
        <footer role="contentinfo" class="slds-p-around--large slds-has-divider--top-space slds-theme--shade">
            <!-- LAYOUT GRID -->
            <div class="slds-grid slds-grid--align-spread">
                <p class="slds-col"></p>
                <p class="slds-col">© 2016 Country Health SA.<br />ABN 97 643 356 590.</p>
            </div>
            <!-- / LAYOUT GRID -->
        </footer>
        <!-- / FOOTER -->
    </div>
    <!-- / REQUIRED SLDS WRAPPER -->
    </body>
    <!-- JAVASCRIPT -->
    <script>
        var assetsLocation = '{!URLFOR($Resource.SLDS102)}';

        /*validation*/
        (function($) {
            $.fn.validator = function(options) {
                var settings = $.extend({
                    assetsLocation: $.aljs.assetsLocation
                    // These are the defaults
                }, options );

                //if (this.length === 1) {
                    return this.on('click', function(e) {
                        e.stopPropagation();
                        $('.slds-theme--error').hide();
                        var hasError = validateRequiredFields();
                        if(!hasError && options && options.customValidation)
                        {
                            hasError = options.customValidation();
                        }
                        if(!hasError)
                        {
                            if(options && options.confirmation)
                            {
                                options.confirmation();
                                return false;
                            }
                            showSpinner();
                        }
                        return !hasError;
                    });
               /* } else {
                    throw new Error("This plugin can only be run with a selector targeting one container (e.g., 'body')")
                }*/
            };
        }(jQuery));

        $.aljsInit({
            assetsLocation: assetsLocation,
            scoped: true
        });

        $('.slds-theme--error ul').addClass('slds-list--dotted');

        $(function () {
            $('[data-aljs="menu"]').menu();

            $('input.error').each(function(i, el){ $(el).closest('div').addClass('slds-has-error');});

            $('.errorMsg').each(function(i, el){
                $(el).removeClass('errorMsg');
                $(el).addClass('slds-form-element__help')
            });

            var ref = window.location.pathname.replace('/apex/','/');
            if(ref.toLowerCase().indexOf("practicelocation") != -1
                    || ref.toLowerCase().indexOf("authoriser") != -1
                    || ref.toLowerCase().indexOf("mydetails") != -1
                    || ref.toLowerCase().indexOf("clientapplicant") != -1)
            {
                ref = "/MyDetails";
            }
            else if(ref.toLowerCase().indexOf("clientsearch") != -1 ||
                    ref.toLowerCase().indexOf("claimspecialist") != -1 ||
                    ref.toLowerCase().indexOf("claimdelegate") != -1)
            {
                ref = "/ClientSearch";
            }
            else if(ref.toLowerCase().indexOf("claim") != -1 ||
                    ref.toLowerCase().indexOf("claimview") != -1 ||
                    ref.toLowerCase().indexOf("thirdpartysearch") != -1 ||
                    ref.toLowerCase().indexOf("myapplications") != -1)
            {
                ref = "/MyApplications";
            }

            $('.menu li a[href="'+ref+'"]').addClass('selected');

        });

        function disableFields(selector, disable)
        {
            if(disable)
            {
                $(selector).attr('disabled', 'disabled').val('');
            }
            else
            {
                $(selector).removeAttr('disabled');
            }
        }

        function showSpinner()
        {
            $('#spinner').show();
        }

        function hideSpinner()
        {
            $('#spinner').hide();
        }

        function validateRequiredFields(jQField)
        {
            var hasError = false;
            var selector = '[required=required]';
            if (jQField !== undefined)
            {
                selector = jQField;
            }
            $(selector).each(function(i, el) {
                var closest = $(el).closest('.slds-form-element__control').length > 0?$(el).closest('.slds-form-element__control'):$(el).closest('div');
                if(!$(el).val() || $(el).val()=='') {
                    if(!closest.hasClass('slds-has-error')) {
                        closest.addClass('slds-has-error');
                        closest.append($('<div id="' + $(el).id + '-error-message" class="slds-form-element__help">This field is required</div>'));
                    }
                    if(!hasError)
                    {
                        if (jQField === undefined) {
                            window.scrollTo($(el).offset().left, $(el).offset().top-100);
                            $(el).focus();
                        }
                    }
                    hasError = true;
                }
                else if(closest.hasClass('slds-has-error')){
                    var removeError = true;
                    if(closest.find('div select, div input').length>1)
                    {
                        closest.find('div select, div input').each(function(i, el){
                            if(!$(el).val() || $(el).val()=='') removeError = false;
                        })
                    }

                    if(removeError) {
                        closest.removeClass('slds-has-error');
                        closest.find('div[id$=-error-message]').remove();
                    }
                }
            });
            return hasError;
        }
    </script>

    <apex:includeScript value="//cdnjs.cloudflare.com/ajax/libs/svg4everybody/2.0.3/svg4everybody.js" />
    <script>
        svg4everybody();

    </script>
    <apex:insert name="scripts" />
    <!-- / JAVASCRIPT -->
    </html>
</apex:page>