<apex:page standardcontroller="Contact" doctype="html-5.0"
           applyhtmltag="false" showheader="false" sidebar="false"
           title="PATS - My Details Edit: {!Contact.Name}" extensions="MyDetailsController">
    <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <apex:composition template="{!$Site.Template}">
        <apex:define name="pagetitle">PATS - My Details Edit: {!Contact.Name}</apex:define>
        <apex:define name="pageheader">
            <div class="slds-page-header" role="banner">
                <div class="slds-grid">
                    <div class="slds-col slds-has-flexi-truncate">
                        <div class="slds-media">
                            <div class="slds-media__figure">
                                <svg aria-hidden="true" class="slds-icon slds-icon--large slds-icon-standard-user">
                                    <use xlink:href="{!URLFOR($Resource.SLDS102,'/assets/icons/standard-sprite/svg/symbols.svg#contact')}"></use>
                                </svg>
                            </div>
                            <div class="slds-media__body">
                                <p class="slds-text-heading--label">Contact</p>
                                <div class="slds-grid">
                                    <h1 class="slds-page-header__title slds-m-right--small slds-truncate slds-align-middle"
                                        title="Record Title">
                                        {!Contact.Salutation}&nbsp;{!Contact.Name}
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="slds-grid slds-wrap slds-page-header__detail-row">
                    <li class="slds-col--padded slds-m-bottom--small slds-size--1-of-3">
                        <span class="slds-text-heading--label slds-truncate">
                            {!$ObjectType.Contact.fields.Practitioners_Name__c.Label}<c:hoverHelp helptext="{!$ObjectType.Contact.fields.Practitioners_Name__c.inlineHelpText}" />
                        </span>
                        <p class="slds-text-body--regular"
                           title="{!Contact.Practitioners_Name__c}">{!Contact.Practitioners_Name__c}</p>
                    </li>
                    <li class="slds-col--padded slds-m-bottom--small slds-size--1-of-3">
                        <span class="slds-text-heading--label slds-truncate">
                            {!$ObjectType.Contact.fields.My_AHPRA_Number__c.Label}<c:hoverHelp helptext="{!$ObjectType.Contact.fields.My_AHPRA_Number__c.inlineHelpText}" />
                        </span>
                        <p class="slds-text-body--regular"
                           title="{!Contact.My_AHPRA_Number__c}">{!Contact.My_AHPRA_Number__c}</p>
                    </li>
                    <li class="slds-col--padded slds-m-bottom--small slds-size--1-of-3">
                        <span class="slds-text-heading--label  slds-truncate">
                            {!$ObjectType.Contact.fields.Practitioners_AHPRA_number__c.Label}<c:hoverHelp helptext="{!$ObjectType.Contact.fields.Practitioners_AHPRA_number__c.inlineHelpText}" />
                        </span>
                        <p class="slds-text-body--regular"
                           title="{!Contact.Practitioners_AHPRA_number__c}">{!Contact.Practitioners_AHPRA_number__c}</p>
                    </li>
                </ul>
            </div>
        </apex:define>
        <apex:define name="body">
            <apex:outputpanel styleclass="slds-notify_container" rendered="{!HasMessages}">
                <div id="errMsg" class="slds-notify slds-notify--toast slds-theme--error" role="alert">
                    <span class="slds-assistive-text">Info</span>
                    <button class="slds-button slds-button--icon-inverse slds-notify__close" data-aljs-dismiss="notification">
                        <svg aria-hidden="true" class="slds-button__icon">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Resource.SLDS102, '/assets/icons/action-sprite/svg/symbols.svg#close')}"></use>
                        </svg>
                        <span class="slds-assistive-text">Close</span>
                    </button>
                    <div class="notify__content slds-grid">
                        <svg aria-hidden="true" class="slds-icon slds-icon--small slds-m-right--small slds-col slds-no-flex">
                            <use xlink:href="{!URLFOR($Resource.SLDS102, '/assets/icons/utility-sprite/svg/symbols.svg#warning')}"></use>
                        </svg>
                        <div class="slds-col slds-align-middle">
                            <h2 class="slds-text-heading--small"><apex:messages /></h2>
                        </div>
                    </div>
                </div>
            </apex:outputpanel>
            <apex:form >
                <div class="slds-p-around--medium">
                    <fieldset class="slds-form--compound slds-m-bottom--small">
                        <legend class="slds-form-element__label slds-text-heading--medium">Details</legend>
                        <div class="slds-grid slds-grid--pull-padded slds-wrap">
                            <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                <div class="slds-form-element">
                                    <label class="slds-form-element__label" for="{!$Component.FirstName}"><abbr class="slds-required" title="required">*</abbr> {!$ObjectType.Contact.fields.FirstName.Label}</label>
                                    <div class="slds-form-element__control slds-wrap slds-grid">
                                        <div class="slds-select_container slds-size--1-of-6">
                                            <apex:inputfield value="{!Contact.Salutation}" styleclass="slds-input" html-placeholder="{!$ObjectType.Contact.fields.Salutation.Label}" id="Salutation" required="true" />
                                        </div>
                                        <div class="slds-size--5-of-6">
                                            <apex:inputfield value="{!Contact.FirstName}" styleclass="slds-input" html-placeholder="{!$ObjectType.Contact.fields.FirstName.Label}" id="FirstName" required="true" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                <div class="slds-form-element">
                                    <label class="slds-form-element__label" for="{!$Component.LastName}"><abbr class="slds-required" title="required">*</abbr> {!$ObjectType.Contact.fields.LastName.Label}</label>
                                    <apex:inputfield value="{!Contact.LastName}" styleclass="slds-input" html-placeholder="{!$ObjectType.Contact.fields.LastName.Label}" id="LastName" required="true" />
                                </div>
                            </div>
                            <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                <div class="slds-form-element">
                                    <label class="slds-form-element__label" for="{!$Component.Email}"><abbr class="slds-required" title="required">*</abbr> {!$ObjectType.Contact.fields.Email.Label}</label>
                                    <apex:inputfield value="{!Contact.Email}" styleclass="slds-input" html-placeholder="{!$ObjectType.Contact.fields.Email.Label}" id="Email" required="true" />
                                </div>
                            </div>
                            <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                <div class="slds-form-element">
                                    <label class="slds-form-element__label" for="{!$Component.Practice_Phone_Number__c}"><abbr class="slds-required" title="required">*</abbr>{!$ObjectType.Contact.fields.Practice_Phone_Number__c.Label}</label>
                                    <apex:inputfield value="{!Contact.Practice_Phone_Number__c}" styleclass="slds-input" html-placeholder="{!$ObjectType.Contact.fields.Practice_Phone_Number__c.Label}" id="Practice_Phone_Number__c" required="true" />
                                </div>
                            </div>
                            <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                <apex:outputpanel styleClass="form" rendered="{!Contact.RecordType.Name == 'Referring Delegate'}" layout="block">
                                    <label class="slds-form-element__label" for="{!$Component.Provider_number__c}">{!$ObjectType.Contact.fields.Provider_number__c.Label}</label>
                                    <apex:inputfield value="{!Contact.Provider_number__c}" styleclass="slds-input" html-placeholder="{!$ObjectType.Contact.fields.Provider_number__c.Label}" id="Provider_number__c"  />
                                </apex:outputpanel>
                            </div>
                            <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2"/>
                        </div>
                    </fieldset>
                    <fieldset class="slds-form--compound slds-m-bottom--small">
                        <legend class="slds-form-element__label slds-text-heading--medium">{!$ObjectType.Contact.fields.Practice_Address__c.Label}</legend>
                        <apex:outputPanel styleClass="slds-grid slds-grid--pull-padded slds-wrap" layout="block"
                                          rendered="{!$Profile.Name == 'Customer Community - Delegate'}">
                            <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1">
                                <div class="slds-form-element">
                                    <label class="slds-form-element__label"
                                           for="{!$Component.Street__c}">
                                        <abbr class="slds-required"
                                              title="required">*</abbr>{!$ObjectType.Practice_Location__c.fields.Street__c.label}
                                    </label>
                                    <div class="slds-form-element__control slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                        <apex:inputfield value="{!LatestPracticeLocation.Street__c}" styleclass="slds-input"
                                                         html-placeholder="Street" id="Street__c"
                                                         required="true" />
                                    </div>
                                </div>
                            </div>
                            <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1">
                                <div class="slds-form-element">
                                    <label class="slds-form-element__label" for="{!$Component.City__c}">
                                        <abbr class="slds-required" title="required">*</abbr>{!$ObjectType.Practice_Location__c.fields.City__c.label}
                                    </label>
                                    <div class="slds-form-element__control slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                        <apex:inputfield value="{!LatestPracticeLocation.City__c}" styleclass="slds-input"
                                                         html-placeholder="City" id="City__c" required="true" />
                                    </div>
                                </div>
                            </div>
                            <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1">
                                <div class="slds-form-element">
                                    <label class="slds-form-element__label" for="{!$Component.State__c}">
                                        <abbr class="slds-required" title="required">*</abbr>{!$ObjectType.Practice_Location__c.fields.State__c.label}
                                    </label>
                                    <div class="slds-form-element__control slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                        <apex:inputfield value="{!LatestPracticeLocation.State__c}" styleclass="slds-input"
                                                         html-placeholder="State" id="State__c" required="true" />
                                    </div>
                                </div>
                            </div>
                            <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1">
                                <div class="slds-form-element">
                                    <label class="slds-form-element__label" for="{!$Component.Postcode__c}">
                                        <abbr class="slds-required" title="required">*</abbr>{!$ObjectType.Practice_Location__c.fields.Postcode__c.label}
                                    </label>
                                    <div class="slds-form-element__control slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                        <apex:inputfield value="{!LatestPracticeLocation.Postcode__c}" styleclass="slds-input"
                                                         html-placeholder="Postal Code" id="Postcode__c"
                                                         required="true" />
                                    </div>
                                </div>
                            </div>
                        </apex:outputPanel>
                        <apex:outputPanel styleClass="slds-grid slds-grid--pull-padded slds-wrap" layout="block"
                                          rendered="{!$Profile.Name != 'Customer Community - Delegate'}">
                            <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                <label class="slds-form-element__label" for="{!$Component.residentialAddress}">
                                    Search
                                    for your {!$ObjectType.Contact.fields.Practice_Address__c.Label}
                                </label>
                                <div class="slds-form-element__control slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                    <c:AddressAutoComplete prefix="Other" id="residentialAddress"
                                                           placeholder="Enter your {!$ObjectType.Contact.fields.Practice_Address__c.Label}"
                                                           googleApiKey="{!$Setup.PATS_Settings__c.Google_Maps_API_Key__c}" />
                                </div>
                            </div>
                            <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1">
                                <div class="slds-form-element">
                                    <label class="slds-form-element__label"
                                           for="{!$Component.OtherStreet}">
                                        <abbr class="slds-required"
                                              title="required">*</abbr>Street
                                    </label>
                                    <div class="slds-form-element__control slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                        <apex:inputfield value="{!Contact.OtherStreet}" styleclass="slds-input"
                                                         html-placeholder="Street" id="OtherStreet"
                                                         required="true" />
                                    </div>
                                </div>
                            </div>
                            <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1">
                                <div class="slds-form-element">
                                    <label class="slds-form-element__label" for="{!$Component.OtherCity}">
                                        <abbr class="slds-required" title="required">*</abbr>City
                                    </label>
                                    <div class="slds-form-element__control slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                        <apex:inputfield value="{!Contact.OtherCity}" styleclass="slds-input"
                                                         html-placeholder="City" id="OtherCity" required="true" />
                                    </div>
                                </div>
                            </div>
                            <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1">
                                <div class="slds-form-element">
                                    <label class="slds-form-element__label" for="{!$Component.OtherState}">
                                        <abbr class="slds-required" title="required">*</abbr>State
                                    </label>
                                    <div class="slds-form-element__control slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                        <apex:inputfield value="{!Contact.OtherState}" styleclass="slds-input"
                                                         html-placeholder="State" id="OtherState" required="true" />
                                    </div>
                                </div>
                            </div>
                            <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1">
                                <div class="slds-form-element">
                                    <label class="slds-form-element__label" for="{!$Component.OtherPostalCode}">
                                        <abbr class="slds-required" title="required">*</abbr>Postal Code
                                    </label>
                                    <div class="slds-form-element__control slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                        <apex:inputfield value="{!Contact.OtherPostalCode}" styleclass="slds-input"
                                                         html-placeholder="Postal Code" id="OtherPostalCode"
                                                         required="true" />
                                    </div>
                                </div>
                            </div>
                            <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1">
                                <div class="slds-form-element">
                                    <label class="slds-form-element__label"
                                           for="{!$Component.OtherCountry}">
                                        <abbr class="slds-required"
                                              title="required">*</abbr>Country
                                    </label>
                                    <div class="slds-form-element__control slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                        <apex:inputfield value="{!Contact.OtherCountry}" styleclass="slds-input"
                                                         html-placeholder="Country" id="OtherCountry"
                                                         required="true" />
                                    </div>
                                </div>
                            </div>
                        </apex:outputPanel>
                    </fieldset>
                    <fieldset class="slds-form--compound slds-m-bottom--small slds-hide">
                        <legend class="slds-form-element__label slds-text-heading--medium">Authoriser</legend>
                        <div class="slds-grid slds-grid--pull-padded slds-wrap">
                            <apex:outputpanel rendered="{!Contact.Application_For_Access_As__c == 'Specialist Delegate'}" layout="none">
                                <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                    <div class="slds-form-element">
                                        <c:hoverHelp helptext="{!$ObjectType.Contact.fields.Practitioners_Name__c.inlineHelpText}" />
                                        <label class="slds-form-element__label" for="{!$Component.Practitioners_Name__c}"><abbr class="slds-required" title="required">*</abbr>{!$ObjectType.Contact.fields.Practitioners_Name__c.Label}</label>
                                        <apex:outputfield value="{!Contact.Practitioners_Name__c}" styleclass="slds-input" html-placeholder="{!$ObjectType.Contact.fields.Practitioners_Name__c.Label}" id="Practitioners_Name__c"  />
                                    </div>
                                </div>
                                <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                    <div class="slds-form-element">
                                        <c:hoverHelp helptext="{!$ObjectType.Contact.fields.Practitioners_AHPRA_number__c.inlineHelpText}" />
                                        <label class="slds-form-element__label" for="{!$Component.Practitioners_AHPRA_number__c}"><abbr class="slds-required" title="required">*</abbr>{!$ObjectType.Contact.fields.Practitioners_AHPRA_number__c.Label}</label>
                                        <apex:outputfield value="{!Contact.Practitioners_AHPRA_number__c}" styleclass="slds-input" html-placeholder="{!$ObjectType.Contact.fields.Practitioners_AHPRA_number__c.Label}" id="Practitioners_AHPRA_number__c" />
                                    </div>
                                </div>
                            </apex:outputpanel>
                            <apex:outputpanel rendered="{!Contact.Application_For_Access_As__c != 'Specialist Delegate'}" layout="none">
                                <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                    <div class="slds-form-element">
                                        <c:hoverHelp helptext="{!$ObjectType.Contact.fields.My_AHPRA_Number__c.inlineHelpText}" />
                                        <label class="slds-form-element__label" for="{!$Component.My_AHPRA_Number__c}"><abbr class="slds-required" title="required">*</abbr>{!$ObjectType.Contact.fields.My_AHPRA_Number__c.Label}</label>
                                        <apex:outputfield value="{!Contact.My_AHPRA_Number__c}" styleclass="slds-input" html-placeholder="{!$ObjectType.Contact.fields.My_AHPRA_Number__c.Label}" id="My_AHPRA_Number__c"  />
                                    </div>
                                </div>
                            </apex:outputpanel>
                            <div class="slds-col--padded slds-m-bottom--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                                <div class="slds-form-element">
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="slds-text-align--center">
                        <apex:commandbutton styleclass="slds-button slds-button--neutral" value="Cancel" action="{!cancel}" id="cancelButton" immediate="true" />
                        <apex:outputpanel layout="none" rendered="{!$ObjectType.Contact.updateable}">
                            <apex:commandbutton styleclass="slds-button slds-button--brand" value="Save" html-data-aljs="validator" action="{!save}" id="saveButton" />
                        </apex:outputpanel>
                    </div>
                </div>
            </apex:form>
        </apex:define>
        <apex:define name="scripts">
            <script>
                $(function () {

                    $('body').notification();
                    $('[data-aljs="popover"]').popover({
                        modifier:'tooltip'
                    });

                    $('[data-aljs="validator"]').validator();
                });
            </script>
        </apex:define>
    </apex:composition>
</html>
</apex:page>