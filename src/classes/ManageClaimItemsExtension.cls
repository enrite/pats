public with sharing class ManageClaimItemsExtension
{
    public ManageClaimItemsExtension(ApexPages.StandardController c)
    {
        if(!Test.isRunningTest())
        {
            c.addFields(new List<String>{Claim__c.Forward_Travel_Date__c.getDescribe().getName(),
                                        Claim__c.Return_Travel_Date__c.getDescribe().getName()});
        }
        record = (Claim__c) c.getRecord();
    }

    private Claim__c record;

    private final String IN_PROGRESS = 'In Progress';
    private final String CREATED = 'Created';

    public List<ClaimItemWrapper> ClaimItems
    {
        get
        {
            if(ClaimItems == null)
            {
                ClaimItems = new List<ClaimItemWrapper>();
                for(Claim_Item__c it : [SELECT ID,
                                                Name,
                                                Who_is_this_for__c,
                                                Claim_Item_Status__c,
                                                Date_from__c,
                                                Date_to__c,
                                                Invoice_Amount_inc_GST__c,
                                                Payee_Invoice_Number__c,
                                                Rate__c,
                                                Total_Approved_to_Pay__c,
                                                Nights__c,
                                                Approved_to_Pay_Account__c,
                                                Payment_Batch_Item__c,
                                                Assessor_Approval_Status__c,
                                                Total_to_Pay__c,
                                                Total_Travel_Payable__c
                                        FROM    Claim_Item__c
                                        WHERE   Claim__c = :record.ID
                                        ORDER BY ID])
                {
                    ClaimItems.add(new ClaimItemWrapper(it, ClaimItems.size()));
                }
            }
            return ClaimItems;
        }
        set;
    }

    private Map<String, RecordType> RecordTypes
    {
        get
        {
            if(RecordTypes == null)
            {
                RecordTypes = new Map<String, RecordType>();

                for(RecordType rt : [
                        SELECT  Id,
                                Name,
                                DeveloperName
                        FROM    RecordType
                        WHERE   sObjectType = 'Claim_Item__c'
                ])
                {
                    RecordTypes.put(rt.DeveloperName, rt);
                }
            }
            return RecordTypes;
        }
        set;
    }

    public Map<ID, RecordType> RecordTypesByID
    {
        get
        {
            if(RecordTypesByID == null)
            {
                RecordTypesByID = new Map<ID, RecordType>();
                for(RecordType rt : RecordTypes.values())
                {
                    RecordTypesByID.put(rt.ID, rt);
                }
            }
            return RecordTypesByID;
        }
        private set;
    }

    public void addClaimItem()
    {
        try
        {
            validateClaim();
            if(ApexPages.hasMessages(ApexPages.Severity.ERROR))
            {
                return;
            }

            String itemType = Utilities.getParameter('itType');
            Claim_Item__c claimItem = new Claim_Item__c(Claim__c = record.Id);
            claimItem.RecordTypeId = RecordTypes.get(itemType).Id;
            claimItem.Assessor_Approval_Status__c = IN_PROGRESS;
            claimItem.Claim_Item_Status__c = CREATED;
            claimItem.Date_from__c = record.Forward_Travel_Date__c;
            claimItem.Date_to__c = record.Return_Travel_Date__c;
            ClaimItems.add(new ClaimItemWrapper(claimItem, ClaimItems.size()));
        }
        catch(Exception e)
        {
            Utilities.addError(e);
        }
    }


    public void deleteClaimItem()
    {
        try
        {
            String index = Utilities.getParameter('index');
            Integer rowIndex = Integer.valueOf(index);
            Claim_Item__c item = ClaimItems[rowIndex].ClaimItem;
            if(item.ID != null)
            {
                delete item;
            }
            ClaimItems.remove(rowIndex);
        }
        catch(Exception e)
        {
            Utilities.addError(e);
        }
    }

    public void saveItems()
    {
        try
        {
            if(!doSave(true))
            {
                Utilities.addError('Please correct errors');
            }
        }
        catch(Exception e)
        {
            Utilities.addError(e);
        }
    }

    public void saveAndApprove()
    {
        Savepoint sp = Database.setSavepoint();
        try
        {
            if(!doSave(false))
            {
                Utilities.addError('Please correct errors');
                return;
            }
            List<Approval.ProcessSubmitRequest> approvalRequests = new List<Approval.ProcessSubmitRequest>();
            List<Approval.ProcessSubmitRequest> declineRequests = new List<Approval.ProcessSubmitRequest>();
            Boolean valid = true;
            for(ClaimItemWrapper ciw : ClaimItems)
            {
                Claim_Item__c ci = ciw.ClaimItem;
                if(ci.Claim_Item_Status__c == 'Created')
                {
                    if(ci.Assessor_Approval_Status__c == 'Assessor Approved' || ci.Assessor_Approval_Status__c == 'Assessor Declined') {
                        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                        req.setObjectId(ci.ID);
                        if(ci.Assessor_Approval_Status__c == 'Assessor Approved') {
                            if(!validateClaimItem(ciw))
                            {
                                valid = false;
                            }
                            approvalRequests.add(req);
                        }
                        else {
                            declineRequests.add(req);
                        }
                    }
                }
            }
            if(!valid)
            {
                Utilities.addError('Please correct errors');
                return;
            }
            List<Approval.ProcessWorkitemRequest> pwiRequests = new List<Approval.ProcessWorkitemRequest>();
            List<Approval.ProcessResult> submitResults = DMLNoSharing.submitApprovalRequests(approvalRequests);
            processRequests(submitResults, pwiRequests, 'Approve');
            DMLNoSharing.submitWorkItemRequests(pwiRequests);

            pwiRequests = new List<Approval.ProcessWorkitemRequest>();
            submitResults = DMLNoSharing.submitApprovalRequests(declineRequests);
            processRequests(submitResults, pwiRequests, 'Reject');
            DMLNoSharing.submitWorkItemRequests(pwiRequests);

            ClaimItems = null;
        }
        catch(Exception e)
        {
            Utilities.addError(e, sp);
        }
    }

    private Boolean doSave(Boolean refreshList)
    {
        validateClaim();
        if(ApexPages.hasMessages(ApexPages.Severity.ERROR))
        {
            return false;
        }
        List<Claim_Item__c> itemsToUpdate = new List<Claim_Item__c>();
        Boolean valid = true;
        for(ClaimItemWrapper ciw : ClaimItems)
        {
            ciw.Error = '';
            Claim_Item__c ci = ciw.ClaimItem;
            if(ci.Claim_Item_Status__c == 'Created')
            {
                itemsToUpdate.add(ci);
                if(String.isBlank(ci.Who_is_this_for__c))
                {
                    ciw.Error = 'Who is this for is required';
                    valid = false;
                }
            }
        }
        if(!valid)
        {
            return false;
        }
        upsert itemsToUpdate;
        if(refreshList)
        {
            ClaimItems = null;
        }
        return true;
    }

    private void validateClaim()
    {
        if(record.Status__c == 'Created')
        {
            Utilities.addError('Claim has not been submitted');
        }
        if(record.Status__c == 'Declined')
        {
            Utilities.addError('Claim has been declined');
        }
    }

    private Boolean validateClaimItem(ClaimItemWrapper ciw)
    {
        Claim_Item__c item = ciw.ClaimItem;
        String recordType = RecordTypesByID.get(ciw.ClaimItem.RecordTypeId).DeveloperName;
        ciw.Error = '';
        if(recordType == 'Accommodation')
        {
            if(item.Rate__c == null)
            {
                ciw.Error = Claim_Item__c.Rate__c.getDescribe().getLabel() + ' ' + Label.All_IsRequired + '<br/>';
            }
            if(item.Nights__c == null)
            {
                ciw.Error += Claim_Item__c.Nights__c.getDescribe().getLabel() + ' ' + Label.All_IsRequired;
            }
        }
        else if(item.Total_Travel_Payable__c == null)
        {
            ciw.Error = Claim_Item__c.Total_Travel_Payable__c.getDescribe().getLabel() + ' ' + Label.All_IsRequired;
        }
        ciw.Error.removeEnd('<br/>');
        return String.isBlank(ciw.Error);
    }

    private void processRequests(List<Approval.ProcessResult> submitResults, List<Approval.ProcessWorkitemRequest> pwiRequests, String action)
    {
        for(Approval.ProcessResult submitResult : submitResults)
        {
            if(submitResult.isSuccess())
            {
                for(ID workItemID : submitResult.getNewWorkitemIds())
                {
                    Approval.ProcessWorkitemRequest pwir = new Approval.ProcessWorkitemRequest();
                    pwir.setWorkitemId(workItemID);
                    pwir.setAction(action);
                    pwiRequests.add(pwir);
                }
            }
        }
    }

    public class ClaimItemWrapper
    {
        public ClaimItemWrapper(Claim_Item__c item, Integer i)
        {
            ClaimItem = item;
            Index = i;
        }

        public Integer Index
        {
            get;
            private set;
        }

        public String Error
        {
            get;
            set;
        }

        public Claim_Item__c ClaimItem
        {
            get;
            set;
        }
    }
}