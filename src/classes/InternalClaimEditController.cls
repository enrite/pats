/**
 * Created by Enrite Solutions on 22/03/2019.
 */

public with sharing class InternalClaimEditController {

    Claim__c Record;
    public InternalClaimEditController(ApexPages.StandardController controller){
        this.Record = (Claim__c)controller.getRecord();
    }

    public PageReference saveAndNew (){
        try {
            upsert this.Record;
        } catch (Exception e){
            return null;
        }
        PageReference pageRef = new PageReference('/' + Claim__c.SObjectType.getDescribe().getKeyPrefix() + '/e');
        pageRef.setRedirect(true);
        return pageRef;
    }
}