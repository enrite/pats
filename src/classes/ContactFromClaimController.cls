/**
 * Created by petermoustrides on 16/06/2016.
 */

public with sharing class ContactFromClaimController
{
    private ApexPages.StandardController controller;
    public ContactFromClaimController(ApexPages.StandardController con)
    {
        controller = con;
    }

    public PageReference create()
    {
        try
        {
            Contact contact =
            [
                    SELECT Id,
                            Name,
                            Salutation,
                            FirstName,
                            Middle_Name__c,
                            LastName,
                            BirthDate,
                            OtherStreet,
                            OtherCity,
                            OtherState,
                            OtherPostalcode,
                            OtherCountry,
                            MailingStreet,
                            MailingCity,
                            MailingState,
                            MailingPostalcode,
                            MailingCountry,
                            Medicare_Card_Number__c,
                            Medicare_Sequence_Number__c,
                            Concession_Health_Card_Card_Holder__c,
                            Card_Number__c,
                            Department_of_Veteran_Affairs_card__c,
                            DVA_Card_Number__c,
                            Display_Other_Address__c,
                            Display_Mailing_Address__c,
                            HomePhone,
                            MobilePhone,
                            Email,
                            OtherPhone,
                            Bank_Account_Name__c,
                            Bank_Account_Number__c,
                            BSB__c,
                            Method_to_receive_remittance_advise_via__c
                    FROM    Contact
                    where id = : controller.getId()
            ];


            Claim__c claim = new Claim__c(Client_First_Name__c = contact.FirstName,
                    Client_Surname__c = contact.LastName,
                    Medicare_Card_Number_Claim__c = contact.Medicare_Card_Number__c,
                    Medicare_Sequence_Number_Claim__c = contact.Medicare_Sequence_Number__c);

            Utilities.setClaimClients(new List<Claim__c>{claim });

            insert claim;

            if (claim != null)
            {
                PageReference pg = new PageReference('/' + claim.ID + '/e');
                pg.getParameters().put('retURL', claim.ID);
                return pg.setRedirect(true);
            }
            Utilities.addError('Client details could not be found');
            return null;
        }
        catch (Exception ex)
        {
            return Utilities.addError('Client details could not be found');
        }
    }

}