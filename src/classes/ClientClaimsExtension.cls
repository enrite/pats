public with sharing class ClientClaimsExtension
{
    public ClientClaimsExtension(ApexPages.StandardController c)
    {
        record = (Contact)c.getRecord();
    }

    private Contact record;

    public List<Claim__c> Claims
    {
        get
        {
            if (Claims == null)
            {
                Claims = [SELECT Id,
                                 Name,
                                 Specialist_Appointment_Date__c,
                                 Specialist_Appointment_Time__c
                          FROM Claim__c
                          WHERE Client_Name__c = :record.Id];
                                    
            }
            return Claims;
        }        
        set;
    }
}