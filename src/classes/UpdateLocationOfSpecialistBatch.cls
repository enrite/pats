/**
 * Package Name: classes
 * Project Name: PATS
 * Class Name: UpdateLocationOfSpecialistBatch
 *
 */

global class UpdateLocationOfSpecialistBatch implements Database.Batchable<sObject>, Database.Stateful {

    public Map<String, String> Errors {
        get {
            if (Errors == null) {
                Errors = new Map<String, String>();
            }
            return Errors;
        }
        set;
    }

    public Map<String, ID> Locations  {
        get {
            if (Locations == null) {
                Locations = new Map<String, Id>();
                for (Reference_Data__c rf : [SELECT ID, Name, Postcode__c, State__c FROM Reference_Data__c WHERE RecordType.DeveloperName = 'Treatment_Localities']) {
                    String local = (rf.Name+','+rf.Postcode__c+','+rf.State__c).toLowerCase().replace(' ', '');
                    Locations.put(local, rf.Id);
                }
            }
            return Locations;
        }
        set;
    }

    global List<Claim__c> start(Database.BatchableContext BC){
        return [SELECT ID,Name,Location_of_Specialist_appointment__c,Location_of_Specialist_appointment_lk__c FROM Claim__c
        WHERE Location_of_Specialist_appointment__c != null AND Location_of_Specialist_appointment__c != '' AND Location_of_Specialist_appointment_lk__c = null];
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        for (Claim__c claim : (List<Claim__c>) scope) {
            String lo = claim.Location_of_Specialist_appointment__c.replace(' ', '').toLowerCase();
            if (Locations.containsKey(lo)) {
                claim.Location_of_Specialist_appointment_lk__c = Locations.get(lo);
            }
        }

        Integer counter = 0;
        for (Database.SaveResult sr : Database.update(scope, false)) {
            if (!sr.success) {
                Claim__c c = (Claim__c)scope.get(counter);
                Errors.put(c.Name, getErrorString(sr.getErrors()));
            }
            counter++;
        }
    }

    private String getErrorString(List<Database.Error> errors) {
        String errorString = '';

        for (Database.Error error : errors) {
            errorString += error.getMessage() + '. ';
        }

        if (String.isBlank(errorString)) {
            errorString = 'Failed to update location of specialist';
        }

        return errorString;
    }

    global void finish(Database.BatchableContext BC){
        Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
        msg.setToAddresses(new List<String> { 'admin@enrite.com.au' } );
        msg.setSubject('Update Location of Specialist Completed');

        String body = '<h1>Update Location of Specialist Completed</h1>';
        body += '<ul>';
        if (Errors.isEmpty()) {
            body += '<li>All records successfully updated</li>';
        } else {
            for (String err : Errors.keySet()) {
                body += '<li>' + err + ': ' +  Errors.get(err) + '</li>';
            }
        }

        body += '</ul>';

        msg.setHtmlBody(body);

        Messaging.sendEmail(new List<Messaging.SingleEmailMessage> { msg });
    }

}