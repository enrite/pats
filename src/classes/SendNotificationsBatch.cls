/**
 * Package Name: classes
 * Project Name: PATS
 * Class Name: SendNotificationsBatch
 *
 */

global class SendNotificationsBatch implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {

    private static final String EMAIL_TEMPLATE_LIKE_STRING = 'Remittance_%_Notification';

    private static final String WILDCARD = '%';

    private static final String SMS = 'SMS';

    private static final String EMAIL = 'Email';

    private static final String ORG_WIDE_EMAIL = 'PATS';

    private Id paymentBatchId;

    private String paymentBatchName;

    public Map<String, EmailTemplate> EmailTemplates {
        get {
            if (EmailTemplates == null) {
                EmailTemplates = new Map<String, EmailTemplate>();
                for (EmailTemplate et : [SELECT ID, DeveloperName, Body, HtmlValue FROM EmailTemplate WHERE DeveloperName LIKE :EMAIL_TEMPLATE_LIKE_STRING AND IsActive = true]) {
                    EmailTemplates.put(et.DeveloperName, et);
                }
            }
            return EmailTemplates;
        }
        set;
    }

    public Id OrgWideEmailAddressId {
        get {
            if (OrgWideEmailAddressId == null) {
                OrgWideEmailAddressId = null;
                for (OrgWideEmailAddress oa : [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = :ORG_WIDE_EMAIL LIMIT 1]) {
                    OrgWideEmailAddressId = oa.Id;
                }
            }
            return OrgWideEmailAddressId;
        }
        set;
    }

    global SendNotificationsBatch(Id paymentBatchId) {
        this(paymentBatchId, '');
    }

    global SendNotificationsBatch(Id paymentBatchId, String paymentBatchName) {
        this.paymentBatchId = paymentBatchId;
        this.paymentBatchName = paymentBatchName;
    }

    global List<SObject> start(Database.BatchableContext param1) {
        return [SELECT ID,
                        Email__c,
                        Mobile__c,
                        Notification_Sent__c,
                        Notification_Sent_Datetime__c,
                        Notification_Sent_Error__c,
                        Mobile_As_Email__c,
                        (SELECT ID,
                                RecordType.Name,
                                Claim__r.Name,
                                Claim__r.Client_Name__r.PATS_Client_Number__c,
                                Claim__r.Client_Name__r.FirstName,
                                Claim__r.Client_Name__r.LastName,
                                Claim__r.Client_Name__c,
                                Claim__r.Method_to_receive_remittance_advise_via__c,
                                Claim__r.Specialist_Appointment_Date__c,
                                Approved_to_Pay_Account__c,
                                Approved_to_Pay_Account__r.Bank_Account_Email__c,
                                Payment_Batch_Item__r.Email__c,
                                Payment_Batch_Item__r.Mobile__c,
                                Payee_Invoice_Number__c
                        FROM    Claim_Items__r LIMIT 1)
                FROM Payment_Batch_Item__c
                WHERE Payment_Batch__c = :this.paymentBatchId
                AND Notification_Sent__c = false
                AND (Email__c != '' OR Mobile__c != '')];
    }

    global void execute(Database.BatchableContext context, List<SObject> records) {
        sendNotifications((List<Payment_Batch_Item__c>)records);
    }

    global void finish(Database.BatchableContext context) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {UserInfo.getUserEmail()});

        String errors = getErrorString();

        String msg = (String.isBlank(errors)) ? 'Notifications have been sent for batch ' + this.paymentBatchName : 'Notifications have been delivered for batch ' + this.paymentBatchName + ' with some errors:\n\nErrors\n\n' + errors;
        mail.setSubject('Notifications process finished');
        mail.setPlainTextBody(msg);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }


    public void sendNotifications(List<Payment_Batch_Item__c> records) {
        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        List<Task> tasks = new List<Task>();

        Datetime now = Datetime.now();
        Date today = Date.today();

        for (Payment_Batch_Item__c pbi : records) {

            messages.add(createSingleEmailMessage(pbi));
        }

        List<Messaging.SendEmailResult> emailResults = Messaging.sendEmail(messages, false);
        Integer counter = 0;

        for (Messaging.SendEmailResult res : emailResults) {
            Payment_Batch_Item__c item = records.get(counter);
            boolean smsPreferredMethod = isSmsPreferredMethod(item);
            item.Notification_Sent_Datetime__c = now;

            Task t = new Task(WhatId = item.Id, WhoId = item.Claim_Items__r.get(0).Claim__r.Client_Name__c, ActivityDate = today, Status = 'Completed', Type = 'Other', Subject='Remittance Notification');
            if (res.success) {
                item.Notification_Sent__c = true;
                item.Notification_Sent_Error__c = '';
                t.Description = 'Notification successfully sent via ' + (smsPreferredMethod ? SMS : EMAIL);
            } else {
                item.Notification_Sent__c = false;
                item.Notification_Sent_Error__c = getErrors(res.getErrors());
                t.Description = 'Notification was not sent via ' + (smsPreferredMethod ? SMS : EMAIL) + '\n\nReason:\n\n' + item.Notification_Sent_Error__c;
            }
            tasks.add(t);
            counter++;
        }

        update records;
        insert tasks;
    }

    private String getErrorString() {
        String errors = '';

        for (Payment_Batch_Item__c item : [SELECT ID, Name, Notification_Sent_Error__c FROM Payment_Batch_Item__c WHERE Payment_Batch__c = :this.paymentBatchId AND Notification_Sent__c = false AND Notification_Sent_Datetime__c != null]) {
            errors += item.Name + ': ' + item.Notification_Sent_Error__c + '\n';
        }

        return errors;
    }

    private Messaging.SingleEmailMessage createSingleEmailMessage(Payment_Batch_Item__c item) {
        boolean sendSms = isSmsPreferredMethod(item);

        Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
        String emailTemplateName = '';
        String emailAddress = '';

        if (sendSms) {
            emailTemplateName = EMAIL_TEMPLATE_LIKE_STRING.replace(WILDCARD, SMS);
            emailAddress = item.Mobile_As_Email__c;
        } else {
            emailTemplateName = EMAIL_TEMPLATE_LIKE_STRING.replace(WILDCARD, EMAIL);
            emailAddress = item.Email__c;
            msg.setHtmlBody(EmailTemplates.get(emailTemplateName).HtmlValue);
        }
        msg.setTreatBodiesAsTemplate(true);
        msg.setPlainTextBody(EmailTemplates.get(emailTemplateName).Body);
        msg.setWhatId(item.Id);
        msg.setOrgWideEmailAddressId(OrgWideEmailAddressId);
        msg.setToAddresses(new List<String> {emailAddress});
        msg.setSaveAsActivity(false);

        return msg;
    }

    private String getErrors(List<Messaging.SendEmailError> errors) {
        String errorString = '';

        for (Messaging.SendEmailError error : errors) {
            errorString += error.message + '\n';
        }

        return errorString;
    }

    private boolean isSmsPreferredMethod(Payment_Batch_Item__c item) {
        boolean sendSms;
        boolean accToPay = false;

        if (item.Claim_Items__r.isEmpty())
        {
            sendSms = false;
        }
        else
        {
            for(Claim_Item__c ci : item.Claim_Items__r)
            {
                System.debug('ci.Approved_to_Pay_Account__c: ' + ci.Approved_to_Pay_Account__c);
                if(!String.IsEmpty(ci.Approved_to_Pay_Account__c))
                {
                    accToPay = true;
                }
            }
            if(accToPay)
            {
                sendSms = false;
            }
            else
            {
                sendSms = item.Claim_Items__r.get(0).Claim__r.Method_to_receive_remittance_advise_via__c  == SMS;
            }
        }

        return sendSms;
    }
}