/**
 * Created by petermoustrides on 3/2/17.
 */

public with sharing class DistanceCalculatorController
{
    public String InstanceUrl
    {
        get
        {
            String result = null;

            List<String> fragments = URL.getSalesforceBaseUrl().getHost().split('\\.');

            if (fragments.size() == 3)
            {
                result = fragments[0]; // Note: Apex URL: NA14.salesforce.com
            }
            else if (fragments.size() == 4)
            {
                return ''; // if community return relative path
            }
            else if (fragments.size() == 5)
            {
                result = fragments[1]; // Note: Visualforce URL: mydomain.NA14.visual.force.com
            }

            return 'https://'+result + '.force.com';
        }
        set;
    }

}