public without sharing class ClaimSearchNoSharing 
{ 
    public static Claim__c getClaimClone(ID claimId)
    {
        String qry = Utilities.getRecordQueryString(Claim__c.sObjectType, claimId);
        if (!Test.isRunningTest() && Utilities.CurrentUser.ContactId != null)
        {
            if (Utilities.IsApplicant())
            {
                qry += ' AND Client_Name__c =\'' +  Utilities.CurrentUser.ContactId + '\'';
                       // ' OR Applicant_Name__c =\'' +  contactId + '\'; 
            }  
        } 
        System.debug(qry);  
        for(Claim__c cl : Database.query(qry))
        {
            return cl.clone(false, true);
        }
        return new Claim__c();
    }
}