public with sharing class MyDetailsController
{
    private Contact record;
    private ApexPages.StandardController controller;

//    public List<Practice_Location__c> practiceLocations
//    {
//        get
//        {
//            return [Select Id, Street__c, City__c, Postcode__c, State__c from Practice_Location__c where Contact_Referring_Delegate_Specialist__c = :record.Id];
//        }
//    }

    public MyDetailsController(ApexPages.StandardController con)
    {
        if(!Test.isRunningTest())
        {
            con.addFields(new String[]{'RecordType.Name'});
        }
        controller = con;
        record = (Contact) controller.getRecord();
    }

    public List<Reference_Data__c> AdditionalInformation
    {
        get
        {
            if(AdditionalInformation== null)
            {
                AdditionalInformation = [SELECT ID,
                                                Name,
                                                URL__c
                                        FROM    Reference_Data__c
                                        WHERE   RecordType.DeveloperName = 'Additional_Information' AND
                                                Active__c = true];
            }
            return AdditionalInformation;
        }
        private set;
    }

    public Practice_Location__c LatestPracticeLocation
    {
        get
        {
            if (LatestPracticeLocation == null)
            {
                LatestPracticeLocation = new Practice_Location__c(Contact_Referring_Delegate_Specialist__c = record.ID);
                for(Practice_Location__c pc : getPracticeLocation())
                {
                    LatestPracticeLocation = pc;
                }
            }
            return LatestPracticeLocation;
        }
        set;
    }

    public Boolean HasMessages
    {
        get
        {
            return ApexPages.hasMessages();
        }
    }

    public PageReference newClaim()
    {
        try
        {
            Claim__c claim = null;
            for(Contact c : [SELECT Id,
                                    Name,
                                    Salutation,
                                    FirstName,
                                    Middle_Name__c,
                                    LastName,
                                    BirthDate,
                                    OtherStreet,
                                    OtherCity,
                                    OtherState,
                                    OtherPostalcode,
                                    OtherCountry,
                                    MailingStreet,
                                    MailingCity,
                                    MailingState,
                                    MailingPostalcode,
                                    MailingCountry,
                                    Medicare_Card_Number__c,
                                    Medicare_Sequence_Number__c,
                                    Concession_Health_Card_Card_Holder__c,
                                    Card_Number__c,
                                    Department_of_Veteran_Affairs_card__c,
                                    DVA_Card_Number__c,
                                    Display_Other_Address__c,
                                    Display_Mailing_Address__c,
                                    HomePhone,
                                    MobilePhone,
                                    Email,
                                    OtherPhone,
                                    Bank_Account_Name__c,
                                    Bank_Account_Number__c,
                                    BSB__c,
                                    Method_to_receive_remittance_advise_via__c
                            FROM    Contact
                            WHERE   ID = :record.ID])
            {
                claim = new Claim__c(Client_Name__c = c.ID, Applicant_Name__c = c.ID);
                claim.RecordTypeID = Utilities.getRecordTypeId(Claim__c.sObjectType, 'Created');
                claim.Salutation__c = c.Salutation;
                claim.Client_First_Name__c = c.FirstName;
                claim.Client_Middle_Name__c = c.Middle_Name__c;
                claim.Client_Surname__c = c.LastName;
                claim.Date_of_Birth__c = c.BirthDate;
                claim.Home_Phone__c = c.HomePhone;
                claim.Mobile_Phone__c = c.MobilePhone;
                claim.Email__c = c.Email;
                claim.Work_Phone__c = c.OtherPhone;
                claim.Residential_Street__c = c.OtherStreet;
                claim.Residential_Suburb__c = c.OtherCity;
                claim.Residential_State__c = c.OtherState;
                claim.Residential_Postcode__c = c.OtherPostalcode;
                claim.Residential_Country__c = c.OtherCountry;
                claim.Mailing_Street__c = c.MailingStreet;
                claim.Mailing_Suburb__c = c.MailingCity;
                claim.Mailing_State__c = c.MailingState;
                claim.Mailing_Postcode__c = c.MailingPostalcode;
                claim.Mailing_Country__c = c.MailingCountry;
                claim.Medicare_Card_Number_Claim__c = c.Medicare_Card_Number__c;
                claim.Medicare_Sequence_Number_Claim__c = c.Medicare_Sequence_Number__c;
                claim.Concession_Health_Care_Card_Holder__c = c.Concession_Health_Card_Card_Holder__c;
                claim.Pensioner_or_Health_Care_Card_Number__c = c.Card_Number__c;
                claim.DVA_Card__c = c.Department_of_Veteran_Affairs_card__c;
                claim.Department_of_Veteran_Affairs_Card_Numbe__c = c.DVA_Card_Number__c;
                claim.Bank_Account_Name__c = c.Bank_Account_Name__c;
                claim.Bank_Account_Number__c = c.Bank_Account_Number__c;
                claim.BSB__c = c.BSB__c;
                claim.Method_to_receive_remittance_advise_via__c = c.Method_to_receive_remittance_advise_via__c;

                insert claim;
            }
            if(claim != null)
            {
                PageReference pg = new PageReference('/' + claim.ID + '/e');
                pg.getParameters().put('retURL', claim.ID);
                return pg.setRedirect(true);
            }
            Utilities.addError('Client details could not be found');
            return null;
        }
        catch(Exception ex)
        {
            return Utilities.addError(ex);
        }
    }

    public PageReference Redirect()
    {
        if (ApexPages.currentPage().getParameters().containsKey('Id'))
        {
            return null;
        }

        PageReference ref = ApexPages.currentPage();
        for (User u :
        [
                SELECT Id,
                        ContactId,
                        Profile.Name
                FROM User
                WHERE Id = :UserInfo.getUserId()
        ])
        {
            Id contactId = u.ContactId;
            Contact c = null;

            if (contactId == null && u.Profile.Name == 'System Administrator')
            {
                c = [select id, RecordType.Name from Contact limit 1];
            }
            else
            {
                c = [select RecordType.Name from Contact where id = :contactId];
            }

            if (c.RecordType.Name != 'Applicant / Client')
            {
                ref =(c.RecordType.Name != 'Third Party Applicant')?Page.AuthoriserDetail:Page.ThirdPartyMyDetails;
                ref.getParameters().putAll(ApexPages.currentPage().getParameters());
            }
            ref.getParameters().put('Id', c.Id);
        }

        return ref;
    }

    public PageReference save()
    {
        try
        {
            String errMsg = '';
            Contact applicant = (Contact) controller.getRecord();

            if(applicant.RecordType.Name != 'Applicant / Client')
            {
                Boolean newLocation = false;
                if(applicant.RecordType.Name == 'Referring Delegate')
                {
                    List<Practice_Location__c> latestLocationFromDB = getPracticeLocation();
                    if(latestLocationFromDB.isEmpty())
                    {
                        newLocation = true;
                    }
                    else
                    {
                        for(Practice_Location__c locationFromDB : latestLocationFromDB)
                        {
                            if(locationFromDB.Street__c != LatestPracticeLocation.Street__c ||
                                    locationFromDB.City__c != LatestPracticeLocation.City__c ||
                                    locationFromDB.State__c != LatestPracticeLocation.State__c ||
                                    locationFromDB.Postcode__c != LatestPracticeLocation.Postcode__c)
                            {
                                newLocation = true;
                                LatestPracticeLocation = LatestPracticeLocation.clone(false, true);
                                if(LatestPracticeLocation.Contact_Referring_Delegate_Specialist__c == null)
                                {
                                    LatestPracticeLocation.Contact_Referring_Delegate_Specialist__c = record.ID;
                                }
                            }
                        }
                    }
                }
                PageReference pg = controller.save();
                if(newLocation)
                {
                    insert LatestPracticeLocation;
                }
                return pg;
            }

            Utilities.checkRequiredField(applicant, Contact.FirstName);
            Utilities.checkRequiredField(applicant, Contact.LastName);
            Utilities.checkRequiredField(applicant, Contact.Medicare_Card_Number__c);
            Utilities.checkRequiredField(applicant, Contact.Medicare_Sequence_Number__c);
            Utilities.checkRequiredField(applicant, Contact.Birthdate);
            Utilities.checkRequiredField(applicant, Contact.Email);
            Utilities.checkRequiredField(applicant, Contact.Aboriginal_Torres_Strait_Islander__c);
            Utilities.checkRequiredField(applicant, Contact.OtherStreet);
            Utilities.checkRequiredField(applicant, Contact.OtherCity);
            Utilities.checkRequiredField(applicant, Contact.OtherState);
            Utilities.checkRequiredField(applicant, Contact.OtherCountry);
            Utilities.checkRequiredField(applicant, Contact.MailingStreet);
            Utilities.checkRequiredField(applicant, Contact.MailingCity);
            Utilities.checkRequiredField(applicant, Contact.MailingState);
            Utilities.checkRequiredField(applicant, Contact.MailingCountry);
            Utilities.checkRequiredField(applicant, Contact.Department_of_Veteran_Affairs_card__c);
            Utilities.checkRequiredField(applicant, Contact.Method_to_receive_remittance_advise_via__c);
            if (applicant.HomePhone == null && applicant.MobilePhone == null)
            {
                Utilities.addError('At least one phone number is required');
            }
            if (applicant.Card_Number__c != null && applicant.Expiry_Date__c == null)
            {
                Utilities.addRequiredFieldError(Contact.Expiry_Date__c);
            }
            if (applicant.Expiry_Date__c != null && applicant.Expiry_Date__c < Date.today())
            {
                Utilities.addError('Concession card expiry date must be in the future');
            }
            if (applicant.Department_of_Veteran_Affairs_card__c != null &&
                    applicant.Department_of_Veteran_Affairs_card__c != 'No card' &&
                    applicant.DVA_Card_Number__c == null)
            {
                Utilities.addRequiredFieldError(Contact.DVA_Card_Number__c);
            }
           // if (HasMessages)
           // {
           //     return null;
           // }

            if (String.isNotBlank(applicant.BSB__c))
            {
                Reference_Data__c rd = Utilities.getBSB(applicant.BSB__c);
                if (rd != null)
                {
                    applicant.BSB__c = rd.Name;

                    Utilities.checkRequiredField(applicant, Contact.Bank_Account_Name__c);
                    Utilities.checkRequiredField(applicant, Contact.Bank_Account_Number__c);
                }
                else
                {
                    Utilities.addError(Label.BSB_Invalid);
                }
            }
            validateSuburb(Contact.OtherCity, applicant.OtherCity, applicant.OtherPostalcode);
            validateSuburb(Contact.MailingCity, applicant.MailingCity, applicant.MailingPostalcode);

            return (HasMessages)?null:controller.save();
        }
        catch (Exception ex)
        {
            Utilities.addError(ex.getMessage());
        }
        return null;
    }

    private void validateSuburb(Schema.SObjectField field, String locality, String postcode)
    {
        if(Utilities.getLocation(locality, postcode) == null)
        {
            Utilities.addRequiredFieldError(field, locality + ' and ' + postcode + 'is not a valid locality/postcode');
        }
    }

    public static void getClaimDetailsFromContact(Claim__c claim, ID contactId)
    {
        for(Contact c : [SELECT Id,
                Name,
                Salutation,
                FirstName,
                Middle_Name__c,
                LastName,
                BirthDate,
                OtherStreet,
                OtherCity,
                OtherState,
                OtherPostalcode,
                OtherCountry,
                MailingStreet,
                MailingCity,
                MailingState,
                MailingPostalcode,
                MailingCountry,
                Medicare_Card_Number__c,
                Medicare_Sequence_Number__c,
                Concession_Health_Card_Card_Holder__c,
                Card_Number__c,
                Department_of_Veteran_Affairs_card__c,
                DVA_Card_Number__c,
                Display_Other_Address__c,
                Display_Mailing_Address__c,
                HomePhone,
                MobilePhone,
                Email,
                OtherPhone,
                Bank_Account_Name__c,
                Bank_Account_Number__c,
                BSB__c,
                Method_to_receive_remittance_advise_via__c
        FROM    Contact
        WHERE   ID = :contactId])
        {
            if(claim == null)
            {
                claim = new Claim__c(Client_Name__c = c.ID, Applicant_Name__c = c.ID);
            }
            claim.RecordTypeID = Utilities.getRecordTypeId(Claim__c.sObjectType, 'Created');
            claim.Salutation__c = c.Salutation;
            claim.Client_First_Name__c = c.FirstName;
            claim.Client_Middle_Name__c = c.Middle_Name__c;
            claim.Client_Surname__c = c.LastName;
            claim.Date_of_Birth__c = c.BirthDate;
            claim.Home_Phone__c = c.HomePhone;
            claim.Mobile_Phone__c = c.MobilePhone;
            claim.Email__c = c.Email;
            claim.Work_Phone__c = c.OtherPhone;
            claim.Residential_Street__c = c.OtherStreet;
            claim.Residential_Suburb__c = c.OtherCity;
            claim.Residential_State__c = c.OtherState;
            claim.Residential_Postcode__c = c.OtherPostalcode;
            claim.Residential_Country__c = c.OtherCountry;
            claim.Mailing_Street__c = c.MailingStreet;
            claim.Mailing_Suburb__c = c.MailingCity;
            claim.Mailing_State__c = c.MailingState;
            claim.Mailing_Postcode__c = c.MailingPostalcode;
            claim.Mailing_Country__c = c.MailingCountry;
            claim.Medicare_Card_Number_Claim__c = c.Medicare_Card_Number__c;
            claim.Medicare_Sequence_Number_Claim__c = c.Medicare_Sequence_Number__c;
            claim.Concession_Health_Care_Card_Holder__c = c.Concession_Health_Card_Card_Holder__c;
            claim.Pensioner_or_Health_Care_Card_Number__c = c.Card_Number__c;
            claim.DVA_Card__c = c.Department_of_Veteran_Affairs_card__c;
            claim.Department_of_Veteran_Affairs_Card_Numbe__c = c.DVA_Card_Number__c;
            claim.Bank_Account_Name__c = c.Bank_Account_Name__c;
            claim.Bank_Account_Number__c = c.Bank_Account_Number__c;
            claim.BSB__c = c.BSB__c;
            claim.Method_to_receive_remittance_advise_via__c = c.Method_to_receive_remittance_advise_via__c;

            upsert claim;
        }
    }

    private List<Practice_Location__c> getPracticeLocation()
    {
        return [ SELECT Id,
                        Name,
                        City__c,
                        Postcode__c,
                        State__c,
                        Street__c
                FROM Practice_Location__c
                WHERE Contact_Referring_Delegate_Specialist__c = :record.Id
                ORDER BY CreatedDate DESC
                LIMIT 1];
    }
}