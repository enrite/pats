public with sharing class ClaimDistanceUtil {

    private static final String GOOGLE_MAP_API_URL;
    private static final String GOOGLE_MAP_API_KEY;
    private static final String GOOGLE_DISTANCE_MATRIXA_API_URL;
    private static final Decimal MINIMUM_DISTANCE;

    static {
        // initialise the settings static variables
        PATS_Settings__c settings = PATS_Settings__c.getOrgDefaults();
        GOOGLE_MAP_API_URL = settings.Google_Map_API_URL__c;
        GOOGLE_MAP_API_KEY = settings.Google_Maps_API_Key__c;
        GOOGLE_DISTANCE_MATRIXA_API_URL = settings.Google_Distance_Matrix_API_URL__c;
        MINIMUM_DISTANCE = (settings.Minimum_Subsidy_Distance__c == null) ? 100 : settings.Minimum_Subsidy_Distance__c.setScale(0);
    }


    public static void setEligibilityStatus(Claim__c claim) {
        if (performCalculation(claim)) {
            claim.Eligible__c = 'Calculating Eligibility';
        }
    }
    public static void calculateClaimableDistance(Claim__c claim) {
        if (!performCalculation(claim)) {
            return;
        }

        calculateClaimableAmount(claim.Id);
    }

    private static boolean performCalculation(Claim__c claim) {
        Map<Id, SObject> oldMap = Trigger.isUpdate ? Trigger.oldMap : new Map<Id, SObject>();

        boolean performRecalculation = false;

        if (Trigger.isUpdate) {
            Claim__c oldClaim = (Claim__c)oldMap.get(claim.Id);
            performRecalculation =
                    (claim.Location_of_Specialist_appointment_lk__c != oldClaim.Location_of_Specialist_appointment_lk__c
                    || claim.Residential_Suburb__c != oldClaim.Residential_Suburb__c
                    || claim.Residential_Street__c != oldClaim.Residential_Street__c
                    || claim.Residential_Postcode__c != oldClaim.Residential_Postcode__c
                    || claim.Residential_State__c != oldClaim.Residential_State__c
                    || claim.Mode_of_Travel_Return__c != oldClaim.Mode_of_Travel_Return__c
                    || claim.Mode_of_Travel_Forward__c != oldClaim.Mode_of_Travel_Forward__c
                    || claim.Additional_Distance_Kms__c != oldClaim.Additional_Distance_Kms__c
                    );
        }

        return Trigger.isInsert || performRecalculation;
    }

    @future(callout=true)
    public static void calculateClaimableAmount(Id claimId) {
        Claim__c claim = getClaimById(claimId);

        if (String.isBlank(claim.Location_of_Specialist_appointment_lk__c) || claim.Location_of_Specialist_appointment_lk__r.Suburb_Location__Latitude__s == null || claim.Location_of_Specialist_appointment_lk__r.Suburb_Location__Longitude__s == null) {
            claim.Claimable_Distance__c = 0;
            claim.Eligible__c = 'Ineligible';
            update claim;
            return;
        }

        // if the we don't have a mode of travel then set the claimable distnace to 0 and the elibility to Ineligible
//        if ((String.isBlank(claim.Mode_of_Travel_Forward__c) || !claim.Mode_of_Travel_Forward__c.contains('Private Car'))
//                && (String.isBlank(claim.Mode_of_Travel_Return__c) || !claim.Mode_of_Travel_Return__c.contains('Private Car'))) {
//            claim.Claimable_Distance__c = 0;
//            claim.Eligible__c = 'Ineligible';
//            update claim;
//            return;
//        }

        GoogleAddressResponse addr = getResidentialAddressLocation(getAddressString(claim));

        if (addr.status != 'OK' || addr.results == null ||  addr.results.isEmpty() || addr.results.get(0).geometry == null || addr.results.get(0).geometry.location == null) {
            System.debug('Failed to get the location of the residential address');
            claim.Eligible__c = 'Address Not Found';
            update claim;
            return;
        }

        claim.Residental_Address_Location__Latitude__s = addr.results.get(0).geometry.location.lat;
        claim.Residental_Address_Location__Longitude__s = addr.results.get(0).geometry.location.lng;

        GoogleAddressResponse distAddressResponse = calculateDistinace(
                claim.Residental_Address_Location__Latitude__s,
                claim.Residental_Address_Location__Longitude__s,
                claim.Location_of_Specialist_appointment_lk__r.Suburb_Location__Latitude__s,
                claim.Location_of_Specialist_appointment_lk__r.Suburb_Location__Longitude__s
        );

        if (distAddressResponse.status != 'OK') { //||  distAddressResponse.rows == null || distAddressResponse.rows.isEmpty() || distAddressResponse.rows.get(0).elements == null || distAddressResponse.rows.get(0).elements.isEmpty() || distAddressResponse.rows.get(0).elements.get(0).distance == null || distAddressResponse.rows.get(0).elements.get(0).distance.value == null) {
            System.debug('Failed to get the distance');

            return;
        }

        Decimal additionalKms = claim.Additional_Distance_Kms__c;
        claim.Claimable_Distance__c = getNearestClaimableDistance(distAddressResponse);//(distAddressResponse.rows.get(0).elements.get(0).distance.value / 1000).setScale(2);
        if (additionalKms != null && additionalKms > 0) {
            claim.Claimable_Distance__c += additionalKms;
        }
        System.debug('Claimable Distance: ' + claim.Claimable_Distance__c);

        claim.Eligible__c = (claim.Claimable_Distance__c >= MINIMUM_DISTANCE) ? 'Eligible' : 'Ineligible';

        update claim;
    }

    private static Claim__c getClaimById(Id claimId) {
        Claim__c claim = null;

        for (Claim__c cl : [SELECT ID,
                Location_of_Specialist_appointment_lk__c,
                Location_of_Specialist_appointment_lk__r.Suburb_Location__Latitude__s,
                Location_of_Specialist_appointment_lk__r.Suburb_Location__Longitude__s,
                Residential_Suburb__c,
                Residential_Street__c,
                Residential_Postcode__c,
                Residential_State__c,
                Residential_Country__c,
                Residental_Address_Location__Latitude__s,
                Residental_Address_Location__Longitude__s,
                Claimable_Distance__c,
                Additional_Distance_Kms__c,
                Mode_of_Travel_Forward__c,
                Mode_of_Travel_Return__c
        FROM Claim__c WHERE Id = :claimId]) {
            claim = cl;
        }
        return claim;
    }
    private static String getAddressString(Claim__c claim) {
        String addressString = '';
        if (String.isNotBlank(claim.Residential_Street__c)) {
            addressString += claim.Residential_Street__c.toUpperCase();
        }

        if (String.isNotBlank(claim.Residential_Suburb__c)) {
            if (String.isNotBlank(addressString)) addressString += ', ';
            addressString += claim.Residential_Suburb__c.toUpperCase();
        }

        if (String.isNotBlank(claim.Residential_State__c)) {
            if (String.isNotBlank(addressString)) addressString += ' ';
            addressString += claim.Residential_State__c.toUpperCase();
        }

        if (String.isNotBlank(claim.Residential_Postcode__c)) {
            if (String.isNotBlank(addressString)) addressString += ' ';
            addressString += claim.Residential_Postcode__c.toUpperCase();
        }

        if (String.isNotBlank(claim.Residential_Country__c)) {
            if (String.isNotBlank(addressString)) addressString += ', ';
            addressString += claim.Residential_Country__c.toUpperCase();
        }


        return addressString;
    }

    public static GoogleAddressResponse getResidentialAddressLocation(String address) {
        Map<String, String> params = new Map<String, String>{'key' => GOOGLE_MAP_API_KEY, 'address' => address};
        return parseGoogleResponse(callGoogleMaps(GOOGLE_MAP_API_URL, params));
    }

    public static GoogleAddressResponse calculateDistinace(Decimal origLat, Decimal origLng, Decimal destLat, Decimal destLng) {
        boolean usingDirectionsApi = (GOOGLE_DISTANCE_MATRIXA_API_URL.contains('/directions/'));
        String originsKey = 'origins';
        String destinationKey = 'destinations';
        if (usingDirectionsApi) {
            originsKey = 'origin';
            destinationKey = 'destination';
        }

        Map<String, String> params = new Map<String, String>{
                'key' => GOOGLE_MAP_API_KEY,
                'units' => 'metric',
                originsKey => String.valueOf(origLat) + ',' + String.valueOf(origLng),
                destinationKey => String.valueOf(destLat) + ',' + String.valueOf(destLng)
        };

        if (usingDirectionsApi) {
            params.put('alternatives', 'true');
        }

        return parseGoogleResponse(callGoogleMaps(GOOGLE_DISTANCE_MATRIXA_API_URL, params));
    }

    public static String callGoogleMaps(String url, Map<String, String> params) {
        String urlParams = (url.indexOf('?') == -1) ? '?' : '&';

        // build the query string parameters.
        for (String paramKey : params.keySet()) {
            urlParams += paramKey + '=' + EncodingUtil.urlEncode(params.get(paramKey), 'UTF-8') + '&';
        }
        urlParams = urlParams.removeEnd('&');
        url = url + urlParams;

        System.debug('url: '  + url);
        Http http = new Http();

        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setHeader('Accept', 'application/json');
        req.setHeader('Content-Type', 'application/json');

        HttpResponse res = http.send(req);
        
        System.debug(res.getBody());
        return res.getBody();
    }

    public static Decimal getNearestClaimableDistance(GoogleAddressResponse resp) {
        List<GoogleAddressRoute> routes = resp.routes;
        Decimal claimableDistance = 0;
        Decimal kmDivisor = 1000;

        if (routes == null) {
            return claimableDistance;
        }
        boolean firstLeg = true;

        for (GoogleAddressRoute route : routes) {
            if (route.legs == null) continue;
            for (GoogleAddressLeg leg : route.legs) {
                if (leg.distance == null || leg.distance.value == null) continue;

                if (firstLeg) {
                    claimableDistance = leg.distance.value;
                    firstLeg = false;
                }

                if (claimableDistance > leg.distance.value) {
                    claimableDistance = leg.distance.value;
                }
            }
        }

        claimableDistance = (claimableDistance / kmDivisor).setScale(2);

        return claimableDistance;
    }

    public static GoogleAddressResponse parseGoogleResponse(String jsonString) {
        return (GoogleAddressResponse)JSON.deserialize(jsonString, GoogleAddressResponse.class);
    }

    public class GoogleAddressResponse {
        public String status {get;set;}
        public List<GoogleAddressResult> results {get;set;}
        public List<GoogleAddressRow> rows {get;set;}
        public List<GoogleAddressRoute> routes {get;set;}
    }

    public class GoogleAddressResult {
        public String formatted_address {get;set;}
        public GoogleAddressGeometry geometry {get;set;}
    }

    public class GoogleAddressGeometry {
        public String location_type {get;set;}
        public GoogleAddressLocation location {get;set;}
    }

    public class GoogleAddressLocation {
        public Decimal lat {get;set;}
        public Decimal lng {get;set;}
    }

    public class GoogleAddressRow {
        List<GoogleAddressElement> elements {get;set;}
    }

    public class GoogleAddressElement {
        public String status {get;set;}
        public GoogleAddressDistance distance {get;set;}
    }

    public class GoogleAddressDistance {
        public Decimal value {get;set;}
    }

    public class GoogleAddressRoute {
        public List<GoogleAddressLeg> legs {get;set;}
    }

    public class GoogleAddressLeg {
        public GoogleAddressDistance distance {get;set;}
    }

}