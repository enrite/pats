public without sharing class ClientSearchNoSharing 
{ 
    public static Contact findClient(Claim__c claim)
    {
        for(Contact c : [SELECT ID 
                    FROM    Contact 
                    WHERE   RecordType.DeveloperName = 'Applicant_Client' AND 
                            FirstName = :claim.Client_First_Name__c AND 
                            LastName = :claim.Client_Surname__c AND 
                            Medicare_Number_Trimmed__c LIKE :(Utilities.firstNineMedicareNumber(claim.Medicare_Card_Number_Claim__c)+'%')
                            ])
            //AND Medicare_Sequence_Number__c = :claim.Medicare_Sequence_Number_Claim__c
        {
            return c;
        }
        return null;
    }
}