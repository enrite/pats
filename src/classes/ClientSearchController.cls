public without sharing class ClientSearchController
{
    private Contact userContact;

    public ClientSearch Parameters
    {
        get
        {
            if (Parameters == null)
            {
                Parameters = new ClientSearch();
            }
            return Parameters;
        }
        set;
    }

    public List<Claim__c> SearchResults { get; set; }

    public boolean SpecialistUser
    {
        get
        {
            if (SpecialistUser == null)
            {
                User u =
                [
                        SELECT Id,
                                ContactId,
                                Profile.Name
                        FROM User
                        WHERE Id = :UserInfo.getUserId()
                ];

                userContact = [select RecordType.Name, Type_of_Service__c, Specialist_Service__c from Contact where id = :u.ContactId];

                SpecialistUser = u.Profile.Name.contains('Specialist');
            }
            return SpecialistUser;
        }
        set;
    }

    public PageReference Search()
    {
        Map<Id, Contact> contactMap = new Map<Id, Contact>([
                SELECT Id
                FROM Contact
                WHERE FirstName = :Parameters.FirstName
                AND LastName = :Parameters.LastName
                AND ((PATS_Client_Number__c = :Parameters.PATSClientNumber and PATS_Client_Number__c != null) OR
                (Medicare_Card_Number__c = :Parameters.MedicareCardNumber and Medicare_Card_Number__c != null
                AND Medicare_Sequence_Number__c = :Parameters.MedicareSequenceNumber and Medicare_Sequence_Number__c != null))
        ]);

        system.debug(contactMap);
        system.debug(SpecialistUser);
        SearchResults = (!SpecialistUser) ?
                [
                        SELECT Id,
                                Name,
                                Specialist_Appointment_Date__c,
                                Specialist_Appointment_Time__c
                        FROM Claim__c
                        WHERE Client_Name__c in :contactMap.keySet()
                        AND Doc_Authorised__c = null
                        AND Doc_Authorising_Delegate_Name__c = null
                        AND Spec_Date_Authorised_by_Specialist_S__c = null
                        AND Spec_Authorising_treating_Specialist__c = null
                        AND Status__c = 'Created'
                ] :
                [
                        SELECT Id,
                                Name,
                                Specialist_Appointment_Date__c,
                                Specialist_Appointment_Time__c
                        FROM Claim__c
                        WHERE Client_Name__c in :contactMap.keySet()
                        AND
                        (
                                (Doc_Type_of_Specialist__c IN :(userContact.Type_of_Service__c==null?new List<string>():userContact.Type_of_Service__c.split(';')) AND Doc_Type_of_Specialist__c != null) OR
                                (Doc_Specialist_Service__c IN :(userContact.Specialist_Service__c==null?new List<string>():userContact.Specialist_Service__c.split(';')) AND Doc_Specialist_Service__c != null) OR
                                (Doc_Authorised__c = false AND Doc_Type_of_Specialist__c = null AND Doc_Specialist_Service__c = null)
                        )
                        AND Spec_Date_Authorised_by_Specialist_S__c = null
                        AND Spec_Authorising_treating_Specialist__c = null
                        AND Status__c != 'Approved'
                ];

        system.debug(SearchResults);

        if (SearchResults.size() < 1)
        {
            PageReference ref = SpecialistUser ? Page.ClaimSpecialist : Page.ClaimDelegate;
            ref.getParameters().put('retURL', Page.ClientSearch.getUrl());
            if (SearchResults.size() == 1)
            {
                ref.getParameters().put('id', SearchResults[0].Id);

            }
            else
            {
                Map<string, string> params = new Map<string, string>
                {
                        'FirstName' => Parameters.FirstName,
                        'LastName' => Parameters.LastName,
                        'MedicareCard' => Parameters.MedicareCardNumber,
                        'MedicareSequenceNumber' => Parameters.MedicareSequenceNumber,
                        'PATSClientNumber' => Parameters.PATSClientNumber
                };
                ref.getParameters().putAll(params);
            }


            return ref.setRedirect(true);
        }


        return null;
    }


    private class ClientSearch
    {
        public string MedicareCardNumber { get; set; }
        public string MedicareSequenceNumber { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string PATSClientNumber { get; set; }
    }
}