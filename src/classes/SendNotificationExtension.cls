/**
 * Package Name: classes
 * Project Name: PATS
 * Class Name: SendNotificationExtension
 *
 */

public with sharing class SendNotificationExtension {

    public Id PaymentBatchItemId {get;set;}

    public Payment_Batch_Item__c PaymentBatchItem {
        get {
            if (PaymentBatchItem == null) {
                PaymentBatchItem = new Payment_Batch_Item__c();
                for (Payment_Batch_Item__c item : [SELECT   ID,
                                                            Email__c,
                                                            Mobile__c,
                                                            Notification_Sent__c,
                                                            Notification_Sent_Datetime__c,
                                                            Notification_Sent_Error__c,
                                                            Mobile_As_Email__c,
                                                            Payment_Batch__c,
                                                            Payment_Batch__r.Name,
                                                            (SELECT ID,
                                                                    RecordType.Name,
                                                                    Claim__r.Name,
                                                                    Claim__r.Client_Name__r.PATS_Client_Number__c,
                                                                    Claim__r.Client_Name__r.FirstName,
                                                                    Claim__r.Client_Name__r.LastName,
                                                                    Claim__r.Client_Name__c,
                                                                    Claim__r.Method_to_receive_remittance_advise_via__c,
                                                                    Claim__r.Specialist_Appointment_Date__c,
                                                                    Approved_to_Pay_Account__c,
                                                                    Approved_to_Pay_Account__r.Bank_Account_Email__c,
                                                                    Payment_Batch_Item__r.Email__c,
                                                                    Payment_Batch_Item__r.Mobile__c,
                                                                    Payee_Invoice_Number__c
                                                            FROM    Claim_Items__r LIMIT 1)
                                                    FROM Payment_Batch_Item__c WHERE ID=:PaymentBatchItemId]) {
                    PaymentBatchItem = item;
                }
            }
            return PaymentBatchItem;
        }
        set;}

    public SendNotificationExtension(ApexPages.StandardController ctr) {
        PaymentBatchItemId = ctr.getId();
    }

    public PageReference send() {
        SendNotificationsBatch batch = new SendNotificationsBatch(PaymentBatchItem.Payment_Batch__c, PaymentBatchItem.Payment_Batch__r.Name);
        batch.sendNotifications(new List<Payment_Batch_Item__c> { PaymentBatchItem });

        return new PageReference('/' + PaymentBatchItem.Id);
    }
}