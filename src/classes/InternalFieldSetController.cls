/**
 * Created by Enrite Solutions on 29/03/2019.
 */

public with sharing class InternalFieldSetController {

    public SObjectType typeOfObject {set;get{
        if (this.typeOfObject == null){
             this.typeOfObject = Record.getSObjectType();
        }
        return this.typeOfObject;
    }}

    public SObject Record {set;get;}


}