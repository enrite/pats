public with sharing class ClaimItemController
{
    private Claim_Item__c record;
    public ClaimItemController(ApexPages.StandardController ctrlr)
    {
        record = (Claim_Item__c)ctrlr.getRecord();
    }

    public PageReference newClaimItem()
    {
        try
        {
            Claim_Item__c claimItem = null;
            for(Claim__c c : [SELECT Id,
                    Forward_Travel_Date__c,
                    Return_Travel_Date__c
            FROM    Claim__c
            WHERE   ID = :record.Claim__c])
            {
                claimItem = new Claim_Item__c(RecordTypeId=record.RecordTypeId, Claim__c = c.ID, Date_from__c= c.Forward_Travel_Date__c, Date_to__c=c.Return_Travel_Date__c);

                insert claimItem;
            }

            if(claimItem != null)
            {
                PageReference pg = new PageReference('/' + claimItem.ID + '/e');
                pg.getParameters().put('retURL', claimItem.ID);
                return pg.setRedirect(true);
            }
            Utilities.addError('Claim details could not be found');
            return null;
        }
        catch(Exception ex)
        {
            return Utilities.addError(ex);
        }
    }
}