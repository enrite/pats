public class Notifications
{
    public static void sendConfirmationEmail(Claim__c claim)
    {
        List<Messaging.singleemailMessage> messages = new List<Messaging.singleemailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        mail.setTargetObjectId(Utilities.CurrentUser.ContactId);
        mail.setWhatId(claim.Id);
        mail.setTemplateId(DMLNoSharing.SubmitttedConfirmationEmailTemplate.Id);
        mail.saveAsActivity = false;
        messages.add(mail);

        Messaging.sendEmail(messages);
    }
}