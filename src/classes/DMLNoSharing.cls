public without sharing class DMLNoSharing 
{ 
    public static void insertContact(Contact c)
    {
        insert c;
    }

    public static void updateContact(Contact c)
    {
        update c;
    }

    public static void upsertClaim(Claim__c c)
    {
        upsert c;
    }

    public static List<Approval.ProcessResult> submitApprovalRequests(List<Approval.ProcessSubmitRequest> approvalRequests)
    {
        return Approval.process(approvalRequests);
    }

    public static void submitWorkItemRequests(List<Approval.ProcessWorkitemRequest> pwiRequests)
    {
        for(Approval.ProcessResult r : Approval.process(pwiRequests))
        {
            if(!r.isSuccess())
            {
                for(Database.Error err : r.getErrors())
                {
                    throw new PATSException(err.getMessage());
                }
            }
        }
    }

    public static List<Contact> matchingContacts(Contact applicant)
    {
        String appMedicareNumber = Utilities.getMedicareNumber(applicant.Medicare_Card_Number__c);
        
        return
        [
                SELECT ID, FirstName, LastName
                FROM Contact
                WHERE RecordType.DeveloperName = 'Applicant_Client' AND
                Medicare_Card_Number__c LIKE :appMedicareNumber AND
                Medicare_Sequence_Number__c = :applicant.Medicare_Sequence_Number__c
                limit 1
        ];
    }

    public static User getUser(Id contactId)
    {
        List<User> users = [Select id from user where contactid = :contactId];

        return users.isEmpty()?null:users[0];
    }


    public static List<EmailTemplate> WelcomeEmailTemplates
    {
        get{
            if(WelcomeEmailTemplates == null)
            {
                WelcomeEmailTemplates = [SELECT Id, DeveloperName, HtmlValue, Body, Subject from EmailTemplate where DeveloperName in : new List<String>{
                        PATSConstants.Email_DelegateWelcome,
                        PATSConstants.Email_ClientApplicantWelcome,
                        PATSConstants.Email_SpecialisDelegateWelcome,
                        PATSConstants.Email_SpecialistWelcome
                }];
            }
            return WelcomeEmailTemplates;
        }
        set;
    }


    public static EmailTemplate SubmitttedConfirmationEmailTemplate
    {
        get
        {
            if(SubmitttedConfirmationEmailTemplate==null)
            {
                SubmitttedConfirmationEmailTemplate = [SELECT Id, HtmlValue, Body, Subject FROM EmailTemplate where DeveloperName = :PATSConstants.Email_SubmittedConfirmation];
            }
            return SubmitttedConfirmationEmailTemplate;
        }
        set;
    }
    
    public static Network PatsNetwork
    {
        get{
             if(PatsNetwork == null)
             {
                 PatsNetwork = [Select id from Network where name = 'PATS'];
             }
            return PatsNetwork;
        }set;
    }
}