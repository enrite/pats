public with sharing class NotesController
{

    private static final String REF_DATA_CONTACT_DETAILS = 'Contact Details';
    private static final String REF_DATA_PATS_GUIDLINES = 'PATS Guidlines';
    private static final String REF_DATA_COPIES_OF_FORMS = 'Copies of Forms';
    private static final String REF_DATA_CLAIM_STATUS = 'Claim Status Explained';
    private static final String REF_DATA_REC_TYPE_COMMUNITY_NOTES = 'Community_Notes';

    private Contact record;
    private ApexPages.StandardController controller;


    public NotesController(ApexPages.StandardController con)
    {
        if(!Test.isRunningTest())
        {
            con.addFields(new String[]{'RecordType.Name'});
        }
        controller = con;
        record = (Contact) controller.getRecord();
    }

    public String ContactDetails
    {
        get
        {
            if (ContactDetails == null)
            {
                ContactDetails = Utilities.getReferenceData(REF_DATA_CONTACT_DETAILS, REF_DATA_REC_TYPE_COMMUNITY_NOTES);
            }
            return ContactDetails;
        }
        set;
    }

    public String PatsGuidelines
    {
        get
        {
            if (PatsGuidelines == null)
            {
                PatsGuidelines = Utilities.getReferenceData(REF_DATA_PATS_GUIDLINES, REF_DATA_REC_TYPE_COMMUNITY_NOTES);

            }
            return PatsGuidelines;
        }
        set;
    }

    public String CopiesOfForms
    {
        get
        {
            if (CopiesOfForms == null)
            {
                CopiesOfForms = Utilities.getReferenceData(REF_DATA_COPIES_OF_FORMS, REF_DATA_REC_TYPE_COMMUNITY_NOTES);
            }
            return CopiesOfForms;
        }
        set;
    }

    public String ClaimStatusExplained
    {
        get
        {
            if (ClaimStatusExplained == null)
            {
                ClaimStatusExplained = Utilities.getReferenceData(REF_DATA_CLAIM_STATUS, REF_DATA_REC_TYPE_COMMUNITY_NOTES);
            }
            return ClaimStatusExplained;
        }
        set;
    }
}