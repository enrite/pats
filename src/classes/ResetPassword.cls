global without sharing class ResetPassword
{
    webservice static String resetPassword(ID contactId)
    {
        for(User u : [SELECT ID
                        FROM    User
                        WHERE   ContactId = :contactId AND
                                IsActive = true])
        {
            System.resetPassword(u.ID, true);
            return 'Password successfully reset';
        }
        return 'No active user found for this contact';
    }
}