/**
 * Created by Cain on 20/08/2018.
 */
@IsTest
public class DeleteClaimItemControllerTest
{
    public static testMethod void test1()
    {
        Account a = new Account(Name = 'PATS');
        insert a;

        Contact con = new Contact();
        con.LastName = 'Test';
        con.FirstName = 'Test';
        con.Medicare_Card_Number__c ='123445';
        con.Medicare_Sequence_Number__c ='1';
        INSERT con;

        Claim__c claim = new Claim__c();
        claim.Applicant_Name__c = con.Id;
        claim.Paper_Copy_of_Section_2_Specialist__c = 'Paper to be posted';
        INSERT claim;

        Claim_Item__c claimItem = new Claim_Item__c();
        claimItem.Claim__c = claim.Id;
        INSERT claimItem;

        Test.setCurrentPage(Page.DeleteClaimItem);
        ApexPages.currentPage().getParameters().put('id', claimItem.Id);

        DeleteClaimItemController ext = new DeleteClaimItemController(new ApexPages.StandardController(claimItem));
        ext.deleteClaimItem();
    }
}