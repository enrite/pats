public without sharing class ClaimExtension
{
    public  ClaimExtension(ApexPages.StandardController ctrlr)
    {
        if(!Test.isRunningTest())
        {
            ctrlr.addFields(new String[]{'Date_submitted__c','Specialist_Appointment_Date__c','Specialist_Appointment_Time__c','Client_Name__c','Applicant_Name__c','Doc_Date_Authorised_by_Delegate__c','Spec_Date_Authorised_by_Specialist_S__c', 'Location_of_Specialist_appointment_lk__r.Suburb_Location__Longitude__s', 'Location_of_Specialist_appointment_lk__r.Suburb_Location__Latitude__s'});
        }
        controller = ctrlr;

        getCurrentContact();
        String salutation = Utilities.getParameter('Salutation');
        String fName = Utilities.getParameter('FirstName');
        String lName = Utilities.getParameter('LastName');
        String medicareNum = Utilities.getParameter('MedicareCard');
        String medicareSeq = Utilities.getParameter('MedicareSequenceNumber');
        if (String.isNotBlank(fName) || String.isNotBlank(lName) || String.isNotBlank(medicareNum) || String.isNotBlank(medicareSeq)) {
            Claim__c claim = (Claim__c) controller.getRecord();
            claim.Salutation__c = salutation;
            claim.Client_First_Name__c = fName;
            claim.Client_Surname__c = lName;
            claim.Medicare_Card_Number_Claim__c = medicareNum;
            claim.Medicare_Sequence_Number_Claim__c = medicareSeq;
        }

        if (Utilities.CurrentUser.Profile.Name == PATSConstants.Profile_ThirdParty) {
            Claim__c claim = (Claim__c) controller.getRecord();
            claim.Bank_Account_Name__c = CurrentContact.Account.Bank_Account_Name__c;
            claim.Bank_Account_Number__c = CurrentContact.Account.Bank_Account_Number__c;
            claim.BSB__c = CurrentContact.Account.BSB__c;
            claim.Method_to_receive_remittance_advise_via__c = 'Email';
            claim.Email__c = CurrentContact.Account.Bank_Account_Email__c;
        }

    }


    private transient List<SelectOption> tForwardTravelOptions;
    private transient List<SelectOption> tReturnTravelOptions;

    private ApexPages.StandardController controller;

    public boolean IsThirdPartyApplicant{
        get{
            if(IsThirdPartyApplicant == null)
            {
                IsThirdPartyApplicant = Utilities.IsThirdPartyApplicant();
            }
            return IsThirdPartyApplicant;
        }set;
    }

    public boolean IsDelegate{
        get{
            if(IsDelegate == null)
            {
                IsDelegate = Utilities.IsDelegate();
            }
            return IsDelegate;
        }set;
    }

    public List<Attachment> Attachments{
        get{

            if(Attachments==null || Attachments.size()==0)
            {
                Attachments = (controller.getId()== null)?new List<Attachment>():[Select id, Name, CreatedDate from Attachment where CreatedById =: UserInfo.getUserId() and ParentId =: controller.getId()];
            }
            return Attachments;
        }
        set;
    }

    public Boolean IsIE9OrLess
    {
        get
        {
            if(ApexPages.currentPage() == null)
            {
                return false;
            }
            String userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');
            for(String userAgentPart : userAgent.split(';'))
            {
                if(userAgentPart.contains('MSIE'))
                {
                    String s = userAgentPart.replace('MSIE', '').deleteWhitespace().substringBefore('.');
                    return s.isNumeric() && Integer.valueOf(s) <= 9;
                }
            }
            return false;
        }
    }

    public Boolean HasMessages
    {
        get
        {
            return ApexPages.hasMessages();
        }
    }

    public String ApplicantProfile
    {
        get
        {
            return PATSConstants.Profile_ClientApplicant;
        }
    }

    public Contact CurrentContact
    {
        get
        {
            if(CurrentContact == null)
            {
                getCurrentContact();
            }
            return CurrentContact;
        }
        private set;
    }
    public String ContentType
    {
        get;
        set;
    }
    public Blob FileBody
    {
        get;
        set;
    }

    public String FileName
    {
        get;
        set;
    }

    public String FocusToId
    {
        get;
        set;
    }

    public String InstanceUrl
    {
        get
        {
            String result = null;

            List<String> fragments = URL.getSalesforceBaseUrl().getHost().split('\\.');

            if (fragments.size() == 3)
            {
                result = fragments[0]; // Note: Apex URL: NA14.salesforce.com
            }
            else if (fragments.size() == 4)
            {
                return ''; // if community return relative path
            }
            else if (fragments.size() == 5)
            {
                result = fragments[1]; // Note: Visualforce URL: mydomain.NA14.visual.force.com
            }

            return 'https://'+result + '.force.com';
        }
        set;
    }

    public List<SelectOption> ForwardTravelOptions
    {
        get
        {
            if(tForwardTravelOptions == null)
            {
                tForwardTravelOptions = Utilities.getOptionForPicklist(Claim__c.Mode_of_Travel_Forward__c, false);
            }
            return tForwardTravelOptions;
        }
    }

    public List<SelectOption> ReturnTravelOptions
    {
        get
        {
            if(tReturnTravelOptions == null)
            {
                tReturnTravelOptions = Utilities.getOptionForPicklist(Claim__c.Mode_of_Travel_Return__c, false);
            }
            return tReturnTravelOptions;
        }
    }

    public Decimal FuelSubsidyPerKm {
        get {
            return PATS_Settings__c.getOrgDefaults().Fuel_Subsidy__c == null ? 0 : PATS_Settings__c.getOrgDefaults().Fuel_Subsidy__c;
        }
    }

    public String ReferringDelegateAuthorsationInfo {
        get {
            if (ReferringDelegateAuthorsationInfo == null)
            {
                ReferringDelegateAuthorsationInfo = Utilities.getReferenceData(PATSConstants.REF_DATA_ReferringDelegateAuthorisationInfo, PATSConstants.REF_DATA_RecTypeClaim);
            }
            return ReferringDelegateAuthorsationInfo;
        }
        set;
    }

    public PageReference redir()
    {
        Claim__c claim = (Claim__c)controller.getRecord();
        if(claim.Date_submitted__c != null)
        {
            PageReference pg = Page.ClaimView;
            pg.getParameters().put('id', claim.ID);
            return pg.setRedirect(true);
        }
        return null;
    }

    public PageReference redirDelegate()
    {
        Claim__c claim = (Claim__c)controller.getRecord();
        if(claim.Doc_Date_Authorised_by_Delegate__c != null)
        {
            PageReference pg = Page.ClientSearch;
            return pg.setRedirect(true);
        }
        return null;
    }

    public PageReference redirSpecialist()
    {
        Claim__c claim = (Claim__c)controller.getRecord();
        if(claim.Spec_Date_Authorised_by_Specialist_S__c != null)
        {
            PageReference pg = Page.ClientSearch;
            return pg.setRedirect(true);
        }
        return null;
    }

    public PageReference populateClaim()
    {
        try
        {
            Claim__c claim = (Claim__c) controller.getRecord();
            claim.Client_Authorised__c = true;
            MyDetailsController.getClaimDetailsFromContact(claim, claim.Client_Name__c);

            PageReference pg = new PageReference('/' + claim.ID);
            return pg.setRedirect(true);
        }
        catch(Exception ex)
        {
            return Utilities.addError(ex);
        }
    }

    public PageReference directToUploadInvoice()
    {
        Claim__c claim = (Claim__c)controller.getRecord();
        PageReference pg = Page.ThirdPartyUploadInvoice;
        pg.getParameters().put('id', claim.ID);
        return pg.setRedirect(true);
    }

    public PageReference uploadInvoice()
    {
        Claim__c claim = (Claim__c)controller.getRecord();
        try {
            saveInvoiceAttachment(claim);
            PageReference pg = Page.Claim;
            pg.getParameters().put('id', claim.ID);
            return pg.setRedirect(true);
        }
        catch (Exception e)
        {
            return Utilities.addError(e);
        }
    }

    public PageReference saveOverride()
    {
        try
        {
            PATS_Settings__c settings = PATS_Settings__c.getOrgDefaults();
            Decimal minimumDistance = (settings.Minimum_Subsidy_Distance__c == null || settings.Minimum_Subsidy_Distance__c < 0) ? 100 : settings.Minimum_Subsidy_Distance__c.setScale(0);

            Claim__c claim = (Claim__c)controller.getRecord();
            Boolean newRecord = claim.ID == null;

            if (!validateClaimableDistance()) {
                return null;
            }

            if(string.isBlank(claim.Doc_Specialist_Service__c) && string.isBlank(claim.Doc_Type_of_Specialist__c))
            {
                Utilities.addRequiredFieldError(Claim__c.Doc_Type_of_Specialist__c, 'or ' + Claim__c.Doc_Specialist_Service__c.getDescribe().getLabel() + ' are required.');
                return null;
            }

            if (claim.Claimable_Distance__c == null)
            {
                claim.Eligible__c = '';
            } else {
                // TODO add this to configuration
                claim.Eligible__c = (claim.Claimable_Distance__c >= minimumDistance) ? 'Eligible' : 'Ineligible';
            }

            setClaimClient(claim, true);
            DMLNoSharing.upsertClaim(claim);
            saveAttachment(claim);
            FocusToId = Utilities.getParameter('focusTo');
            PageReference pg = Page.Claim;
            pg.getParameters().put('id', claim.ID);
            return pg.setRedirect(true);
        }
        catch(Exception ex)
        {
            return Utilities.addError(ex);
        }
    }

    public PageReference saveDelegate()
    {
        try
        {
            if (!validateClaimableDistance()) {
                return null;
            }

            Claim__c claim = (Claim__c)controller.getRecord();
            if(string.isBlank(claim.Doc_Specialist_Service__c) && string.isBlank(claim.Doc_Type_of_Specialist__c))
            {
                Utilities.addRequiredFieldError(Claim__c.Doc_Type_of_Specialist__c, 'or ' + Claim__c.Doc_Specialist_Service__c.getDescribe().getLabel() + ' are required.');
                return null;
            }
            if(claim.Spec_Is_an_escort_required_during_travel__c == 'Yes')
            {
                Utilities.checkRequiredField(claim, Claim__c.Spec_Why_is_an_escort_required_to_travel__c);
            }
            system.debug(claim.Delegate_Consent_Confirmed__c);
            if(!claim.Delegate_Consent_Confirmed__c)
            {
                Utilities.addError(Label.Delegate_Authorisation + ' is required');
                return null;
            }
            setClaimClient(claim, false);
            claim.Doc_Authorising_Delegate_Name__c = Utilities.CurrentUser.ContactId;
            claim.Doc_Date_Authorised_by_Delegate__c = Datetime.now();
            claim.Doc_Authorised__c = true;
            DMLNoSharing.upsertClaim(claim);
            PageReference pg = Page.ClaimSubmitted;
            pg.getParameters().put('Id',claim.Id);
            Notifications.sendConfirmationEmail(claim);
            return pg.setRedirect(true);
        }
        catch(Exception ex)
        {
            return Utilities.addError(ex);
        }
    }

    public PageReference saveSpecialist()
    {
        try
        {
            if (!validateClaimableDistance()) {
                return null;
            }
            Claim__c claim = (Claim__c)controller.getRecord();
            if(claim.Specialist_Treatment_Form__c == 'S2')
            {
                if(claim.Spec_Start_Date_treatment_consultation__c > claim.Spec_End_Date_of_treatment_consultation__c)
                {
                    Utilities.addRequiredFieldError(Claim__c.Spec_Start_Date_treatment_consultation__c, Claim__c.Spec_End_Date_of_treatment_consultation__c, Label.All_MustBeBefore);
                }
                if(claim.Spec_Hospital_Admission_Date__c > claim.Spec_Hospital_Discharge_Date__c)
                {
                    Utilities.addRequiredFieldError(Claim__c.Spec_Hospital_Admission_Date__c,  Claim__c.Spec_Hospital_Discharge_Date__c, Label.All_MustBeBefore);
                }
                if(claim.Spec_Does_the_client_warrant_air_travel__c == 'Yes' && claim.Spec_Why_do_they_warrant_air_travel__c == null)
                {
                    Utilities.addRequiredFieldError(Claim__c.Spec_Why_do_they_warrant_air_travel__c);
                }
                if(claim.Client_Name__r.BirthDate != null && claim.Client_Name__r.BirthDate.addYears(18) < Date.today() && claim.Spec_Is_an_escort_required_during_travel__c == null)
                {
                    Utilities.addError(Label.Claim_Under18Escort);
                }
                if(claim.Spec_Is_an_escort_required_during_travel__c == 'Yes')
                {
                    Utilities.checkRequiredField(claim, Claim__c.Spec_Why_is_an_escort_required_to_travel__c);
                    Utilities.checkRequiredField(claim, Claim__c.Spec_Accommodation_required_for_escort__c);
                }
                if(claim.Spec_Accommodation_required_for_escort__c == 'Yes')
                {
                    Utilities.checkRequiredField(claim, Claim__c.Spec_Reason_for_escort_accommodation__c);
                    Utilities.checkRequiredField(claim, Claim__c.Spec_Nights_accommodation_for_the_escort__c);
                    if(claim.Spec_Nights_accommodation_for_the_escort__c > claim.Spec_How_many_extra_nights_required__c)
                    {
                        Utilities.addRequiredFieldError(Claim__c.Spec_Nights_accommodation_for_the_escort__c, Claim__c.Spec_How_many_extra_nights_required__c, 'must not exceed');
                    }
                }
            }
            else if(claim.Specialist_Treatment_Form__c == 'S4 - Block Treatment')
            {
                if(claim.Does_the_client_warrant_air_travel_S4__c == 'Yes' && claim.Why_do_they_warrant_air_travel_S4__c == null)
                {
                    Utilities.addRequiredFieldError(Claim__c.Why_do_they_warrant_air_travel_S4__c);
                }

                if(claim.Does_the_client_warrant_air_travel_S4__c == 'Yes' && claim.Forward_air_travel_required_S4__c == null)
                {
                    Utilities.addRequiredFieldError(Claim__c.Forward_air_travel_required_S4__c);
                }
                if(claim.Does_the_client_warrant_air_travel_S4__c == 'Yes' && claim.Return_air_travel_required_S4__c == null)
                {
                    Utilities.addRequiredFieldError(Claim__c.Return_air_travel_required_S4__c);
                }

                if(claim.Client_Name__r.BirthDate != null && claim.Client_Name__r.BirthDate.addYears(18) < Date.today() && claim.Is_escort_required_during_travel_S4__c == null)
                {
                    Utilities.addError(Label.Claim_Under18Escort);
                }
                if(claim.Is_escort_required_during_travel_S4__c == 'Yes')
                {
                    Utilities.checkRequiredField(claim, Claim__c.Why_is_escort_required_to_travel_S4__c);
                    Utilities.checkRequiredField(claim, Claim__c.Accommodation_required_for_escort_S4__c);
                }
                if(claim.Accommodation_required_for_escort_S4__c == 'Yes' && claim.Reason_for_escort_accommodation_S4__c == null)
                {
                    Utilities.addRequiredFieldError(Claim__c.Reason_for_escort_accommodation_S4__c);
                }

                /*if(claim.Accommodation_required_for_escort_S4__c == 'Yes')
                {
                    Utilities.checkRequiredField(claim, Claim__c.Reason_for_escort_accommodation_S4__c);
                    Utilities.checkRequiredField(claim, Claim__c.Spec_Nights_accommodation_for_the_escort__c);
                    if(claim.Spec_Nights_accommodation_for_the_escort__c > claim.Spec_How_many_extra_nights_required__c)
                    {
                        Utilities.addRequiredFieldError(Claim__c.Spec_Nights_accommodation_for_the_escort__c, Claim__c.Spec_How_many_extra_nights_required__c, 'must not exceed');
                    }
                }*/

                if(claim.Treatment_Start_Date_1__c > claim.Treatment_End_Date_1__c)
                {
                    Utilities.addRequiredFieldError(Claim__c.Treatment_Start_Date_1__c, Claim__c.Treatment_End_Date_1__c, Label.All_MustBeBefore);
                }
                if(claim.Treatment_Start_Date_2__c > claim.Treatment_End_Date_2__c)
                {
                    Utilities.addRequiredFieldError(Claim__c.Treatment_Start_Date_2__c, Claim__c.Treatment_End_Date_2__c, Label.All_MustBeBefore);
                }
                if(claim.Treatment_Start_Date_3__c > claim.Treatment_End_Date_3__c)
                {
                    Utilities.addRequiredFieldError(Claim__c.Treatment_Start_Date_3__c, Claim__c.Treatment_End_Date_3__c, Label.All_MustBeBefore);
                }
                if(claim.Treatment_Start_Date_4__c > claim.Treatment_End_Date_4__c)
                {
                    Utilities.addRequiredFieldError(Claim__c.Treatment_Start_Date_4__c, Claim__c.Treatment_End_Date_4__c, Label.All_MustBeBefore);
                }
                if(claim.Treatment_Start_Date_5__c > claim.Treatment_End_Date_5__c)
                {
                    Utilities.addRequiredFieldError(Claim__c.Treatment_Start_Date_5__c, Claim__c.Treatment_End_Date_5__c, Label.All_MustBeBefore);
                }

                if(claim.Journey_Start_Date_1__c > claim.Journey_End_Date_1__c)
                {
                    Utilities.addRequiredFieldError(Claim__c.Journey_Start_Date_1__c, Claim__c.Journey_End_Date_1__c, Label.All_MustBeBefore);
                }
                if(claim.Journey_Start_Date_2__c > claim.Journey_End_Date_2__c)
                {
                    Utilities.addRequiredFieldError(Claim__c.Journey_Start_Date_2__c, Claim__c.Journey_End_Date_2__c, Label.All_MustBeBefore);
                }
                if(claim.Journey_Start_Date_3__c > claim.Journey_End_Date_3__c)
                {
                    Utilities.addRequiredFieldError(Claim__c.Journey_Start_Date_3__c, Claim__c.Journey_End_Date_3__c, Label.All_MustBeBefore);
                }
                if(claim.Journey_Start_Date_4__c > claim.Journey_End_Date_4__c)
                {
                    Utilities.addRequiredFieldError(Claim__c.Journey_Start_Date_4__c, Claim__c.Journey_End_Date_4__c, Label.All_MustBeBefore);
                }
                if(claim.Journey_Start_Date_5__c > claim.Journey_End_Date_5__c)
                {
                    Utilities.addRequiredFieldError(Claim__c.Journey_Start_Date_5__c, Claim__c.Journey_End_Date_5__c, Label.All_MustBeBefore);
                }
            }

            if(HasMessages)
            {
                return null;
            }
            setClaimClient(claim, false);
            if(claim.Specialist_Treatment_Form__c == 'S2')
            {
                claim.Spec_Authorising_treating_Specialist__c = Utilities.CurrentUser.ContactId;
                claim.Spec_Date_Authorised_by_Specialist_S__c = Datetime.now();
                claim.Spec_Authorised__c = true;
            } else if(claim.Specialist_Treatment_Form__c == 'S4 - Block Treatment')
            {
                if(claim.Treatment_Start_Date_1__c != null)
                {
                    claim.Authorised_1__c = true;
                }
                if(claim.Treatment_Start_Date_2__c != null)
                {
                    claim.Authorised_2__c = true;
                }
                if(claim.Treatment_Start_Date_3__c != null)
                {
                    claim.Authorised_3__c = true;
                }
                if(claim.Treatment_Start_Date_4__c != null)
                {
                    claim.Authorised_4__c = true;
                }
                if(claim.Treatment_Start_Date_5__c != null)
                {
                    claim.Authorised_5__c = true;
                }
                claim.Certified_by_treating_Specialist_S4__c = true;
                claim.Authorising_treating_Specialist_S4__c = Utilities.CurrentUser.ContactId;
                claim.Date_Certified_by_Specialist_S4__c = Datetime.now();
            }

            DMLNoSharing.upsertClaim(claim);

            return Page.ClientSearch.setRedirect(true);
        }
        catch(Exception ex)
        {
            return Utilities.addError(ex);
        }
    }

    public PageReference submitClaim()
    {
        try
        {
            Claim__c claim = (Claim__c)controller.getRecord();
            validateClaim(claim);

            if(HasMessages || !validateClaimableDistance())
            {
                return null;
            }
            if(claim.ID == null)
            {
                setClaimClient(claim, true);
            }
            claim.Status__c = 'Submitted';
            claim.Date_Submitted__c = Datetime.now();
            claim.RecordTypeId = Utilities.getRecordTypeId(Claim__c.sObjectType, 'Submitted');
            claim.Client_Certification_Date__c = Datetime.now();
            Notification__c notification = Notification__c.getOrgDefaults();
            notification.Client_Authorisation_Part_1__c = notification.Client_Authorisation_Part_1__c == null ? '' : notification.Client_Authorisation_Part_1__c;
            notification.Client_Authorisation_Part_2__c = notification.Client_Authorisation_Part_2__c == null ? '' : notification.Client_Authorisation_Part_2__c;
            notification.Client_Authorisation_Part_3__c = notification.Client_Authorisation_Part_3__c == null ? '' : notification.Client_Authorisation_Part_3__c;
            claim.Client_Certification_Text__c = notification.Client_Authorisation_Part_1__c + notification.Client_Authorisation_Part_2__c + notification.Client_Authorisation_Part_3__c;
            DMLNoSharing.upsertClaim(claim);
            saveAttachment(claim);

            return Page.MyApplications.setRedirect(true);
        }
        catch(Exception ex)
        {
            return Utilities.addError(ex);
        }
    }

    private String getExtension (String fileName)
    {
        if (String.IsBlank(fileName) || !fileName.contains('.'))
        {
            return '';
        }
        return '.' + fileName.substringAfterLast('.');
    }

    private void saveInvoiceAttachment(Claim__c claim)
    {
        if(FileBody == null)
        {
            return;
        }
        Attachment att = new Attachment();
        att.ParentID = claim.Id;
        att.Name ='Claim  ' + claim.Name + '-Invoice' + getExtension(FileName);
        att.Body = FileBody;
        att.ContentType = ContentType;
        claim.Invoice_Uploaded__c = true;
        update claim;
        insert att;
    }

    private void saveAttachment(Claim__c claim)
    {
        if(FileBody == null)
        {
            return;
        }
        Attachment att = new Attachment();
        att.ParentID = claim.Id;
        att.Name = FileName;
        att.Body = FileBody;
        insert att;
    }

    private void validateClaim(Claim__c claim)
    {
        Utilities.checkRequiredField(claim, Claim__c.Client_First_Name__c);
        Utilities.checkRequiredField(claim, Claim__c.Client_Surname__c);
        Utilities.checkRequiredField(claim, Claim__c.Date_of_Birth__c);
        Utilities.checkRequiredField(claim, Claim__c.Medicare_Card_Number_Claim__c);
        Utilities.checkRequiredField(claim, Claim__c.Medicare_Sequence_Number_Claim__c);
        if (!IsDelegate && !IsThirdPartyApplicant) {
            Utilities.checkRequiredField(claim, Claim__c.Concession_Health_Care_Card_Holder__c);
        }
        if (!IsDelegate && !IsThirdPartyApplicant)
        {
            Utilities.checkRequiredField(claim, Claim__c.DVA_Card__c);
        }
        if (!IsThirdPartyApplicant) {
            Utilities.checkRequiredField(claim, Claim__c.Any_Insurance_claim__c);
            Utilities.checkRequiredField(claim, Claim__c.Private_Health_Fund_Claim_A__c);
            Utilities.checkRequiredField(claim, Claim__c.Forward_Travel_Date__c);
            Utilities.checkRequiredField(claim, Claim__c.Return_Travel_Date__c);
        }

        Utilities.checkRequiredField(claim, Claim__c.Method_to_receive_remittance_advise_via__c);
        if(claim.Method_to_receive_remittance_advise_via__c == 'Email' && claim.Email__c == null)
        {
            Utilities.addError(Claim__c.Email__c.getDescribe().getLabel() + ' is required when ' + Claim__c.Method_to_receive_remittance_advise_via__c.getDescribe().getLabel() + ' is ' + claim.Method_to_receive_remittance_advise_via__c);
        }
        if(claim.Method_to_receive_remittance_advise_via__c == 'SMS' && claim.Mobile_Phone__c == null)
        {
            Utilities.addError(Claim__c.Mobile_Phone__c.getDescribe().getLabel() + ' is required when ' + Claim__c.Method_to_receive_remittance_advise_via__c.getDescribe().getLabel() + ' is ' + claim.Method_to_receive_remittance_advise_via__c);
        }
        Utilities.checkRequiredField(claim, Claim__c.Bank_Account_Number__c);
        system.debug(claim.Bank_Account_Number__c);
        system.debug(claim.Bank_Account_Number__c.contains('\''));
        if(claim.Bank_Account_Name__c != null && claim.Bank_Account_Name__c.contains('\''))
        {
            Utilities.addError(Claim__c.Bank_Account_Name__c.getDescribe().getLabel() + ' cannot contain apostrophes');
        }
        Utilities.checkRequiredField(claim, Claim__c.BSB__c);
        if(!IsThirdPartyApplicant && claim.Trip_started_from_home_town_address__c != 'Yes')
        {
            Utilities.addError(Label.Registration_TripStarted);
        }
        if(claim.Eligible_from_any_other_scheme__c != null && claim.Eligible_from_any_other_scheme__c != 'Not eligible')
        {
            Utilities.addError(Label.Claim_OtherScheme);
        }
        if(claim.Concession_Health_Care_Card_Holder__c == 'Yes' && claim.Pensioner_or_Health_Care_Card_Number__c == null)
        {
            Utilities.addRequiredFieldError(Claim__c.Pensioner_or_Health_Care_Card_Number__c, 'is required when ' + Claim__c.Concession_Health_Care_Card_Holder__c.getDescribe().getLabel() + ' is Yes');
        }
        if(claim.DVA_Card__c == 'Gold')
        {
            if (!IsDelegate)
            {
                Utilities.addError(Label.Claim_GoldDVACard);
            }
        }
        else if(claim.DVA_Card__c != null && claim.DVA_Card__c != 'No Card' && claim.Department_of_Veteran_Affairs_Card_Numbe__c == null)
        {
            if(!IsDelegate)
            {
                Utilities.addRequiredFieldError(Claim__c.Department_of_Veteran_Affairs_Card_Numbe__c);
            }
        }
        if (!IsThirdPartyApplicant) {
            Utilities.checkRequiredField(claim, Claim__c.Eligible_from_any_other_scheme__c);
            Utilities.checkRequiredField(claim, Claim__c.Workers_compensation_claim__c);
            Utilities.checkRequiredField(claim, Claim__c.Any_Insurance_claim__c);
            Utilities.checkRequiredField(claim, Claim__c.Private_Health_Fund_Claim_A__c);
        }
        if(claim.Specialist_Appointment_Date__c != null && claim.Specialist_Appointment_Date__c < Date.today().addMonths(-6))
        {
            Utilities.addError(Label.Claim_6MonthsAfterAppointment);
        }
        if(!claim.Client_Authorised__c)
        {
            Utilities.addError(Label.Claim_ClientAuthorised);
        }
        if(claim.Date_of_Birth__c >= Date.today())
        {
            Utilities.addRequiredFieldError(Claim__c.Date_of_Birth__c, Label.All_PastDate);
        }
        if(claim.Date_of_Birth__c > Date.today().addYears(-18) && claim.Name_of_escort__c == null)
        {
            Utilities.addError(Label.Claim_Under18Escort);
        }
        if(claim.Spec_Is_an_escort_required_during_travel__c == 'Yes' && claim.Name_of_escort__c == null)
        {
            Utilities.addRequiredFieldError(Claim__c.Name_of_escort__c, Label.Claim_EscortRequired);
        }

        if (!IsThirdPartyApplicant) {
            if (String.isBlank(claim.Mode_of_Travel_Forward__c)) {
                Utilities.addRequiredFieldError(Claim__c.Mode_of_Travel_Forward__c);
            } else {
                validateAmountPaidForTravel(claim, Claim__c.Mode_of_Travel_Forward__c);
            }

            if (String.isBlank(claim.Mode_of_Travel_Return__c)) {
                Utilities.addRequiredFieldError(Claim__c.Mode_of_Travel_Return__c);
            } else {
                validateAmountPaidForTravel(claim, Claim__c.Mode_of_Travel_Return__c);
            }

            // Travel dates
            if (claim.Forward_Travel_Date__c > claim.Return_Travel_Date__c) {
                Utilities.addRequiredFieldError(Claim__c.Return_Travel_Date__c, Claim__c.Forward_Travel_Date__c, Label.All_EqualOrGreater);
            }
        }
        if(claim.Client_Check_in__c != null && claim.Client_Check_out__c == null)
        {
            Utilities.addRequiredFieldError(Claim__c.Client_Check_out__c);
        }
        if (!IsThirdPartyApplicant) {
            if(claim.Is_this_the_nearest_specialist__c == null)
            {
                Utilities.addRequiredFieldError(Claim__c.Is_this_the_nearest_specialist__c);
            }
        }
        if(claim.Is_this_the_nearest_specialist__c == 'No' && claim.The_reason_for_visiting_this_specialist__c == null)
        {
            Utilities.addRequiredFieldError(Claim__c.The_reason_for_visiting_this_specialist__c);
        }
        if(claim.Client_Check_in__c == null && claim.Client_Check_out__c != null)
        {
            Utilities.addRequiredFieldError(Claim__c.Client_Check_in__c);
        }
        if(claim.Client_Check_in__c >= claim.Client_Check_out__c)
        {
            Utilities.addRequiredFieldError(Claim__c.Client_Check_out__c, Claim__c.Client_Check_in__c, Label.All_MustBeAfter);
        }
        if (!IsThirdPartyApplicant) {
            if (claim.Client_Check_in__c < claim.Forward_Travel_Date__c || claim.Client_Check_in__c > claim.Return_Travel_Date__c) {
                Utilities.addRequiredFieldError(Claim__c.Client_Check_in__c,
                        Label.All_Between + ' ' +
                                Claim__c.Forward_Travel_Date__c.getDescribe().getLabel() +
                                ' and ' +
                                Claim__c.Return_Travel_Date__c.getDescribe().getLabel());
            }
            if (claim.Client_Check_out__c < claim.Forward_Travel_Date__c || claim.Client_Check_in__c > claim.Return_Travel_Date__c) {
                Utilities.addRequiredFieldError(Claim__c.Client_Check_out__c,
                        Label.All_Between + ' ' +
                                Claim__c.Forward_Travel_Date__c.getDescribe().getLabel() +
                                ' and ' +
                                Claim__c.Return_Travel_Date__c.getDescribe().getLabel());
            }
        }
        if(claim.Escort_Check_in__c != null && claim.Client_Check_out__c == null)
        {
            Utilities.addRequiredFieldError(Claim__c.Client_Check_out__c);
        }
        if(claim.Escort_Check_in__c == null && claim.Escort_Check_out__c != null)
        {
            Utilities.addRequiredFieldError(Claim__c.Escort_Check_in__c);
        }
        if(claim.Escort_Check_in__c >= claim.Escort_Check_out__c)
        {
            Utilities.addRequiredFieldError(Claim__c.Escort_Check_out__c, Claim__c.Escort_Check_in__c, Label.All_MustBeAfter);
        }
        if (!IsThirdPartyApplicant) {
            if (claim.Escort_Check_in__c < claim.Forward_Travel_Date__c || claim.Escort_Check_in__c > claim.Return_Travel_Date__c) {
                Utilities.addRequiredFieldError(Claim__c.Escort_Check_in__c,
                        Label.All_Between + ' ' +
                                Claim__c.Forward_Travel_Date__c.getDescribe().getLabel() +
                                ' and ' +
                                Claim__c.Return_Travel_Date__c.getDescribe().getLabel());
            }
            if (claim.Escort_Check_out__c < claim.Forward_Travel_Date__c || claim.Escort_Check_out__c > claim.Return_Travel_Date__c) {
                Utilities.addRequiredFieldError(Claim__c.Escort_Check_out__c,
                        Label.All_Between + ' ' +
                                Claim__c.Forward_Travel_Date__c.getDescribe().getLabel() +
                                ' and ' +
                                Claim__c.Return_Travel_Date__c.getDescribe().getLabel());
            }
        }
        if(Utilities.getBSB(claim.BSB__c) == null)
        {
            Utilities.addError(Label.BSB_Invalid);
        }

        if(!claim.Spec_Authorised__c && claim.Paper_Copy_of_Section_2_Specialist__c == null)
        {
            Utilities.addError(Notification__c.getOrgDefaults().Claim_Not_Authorised__c);
        }
        if(claim.Paper_Supporting_Documentation__c == 'Scanned copy attached')
        {
            if([SELECT COUNT()
            FROM    Attachment
            WHERE   ParentID = :claim.ID] == 0)
            {
                Utilities.addError(Label.Label_Claim_SupportingDocumentation);
            }
        }
    }

    private void validateAmountPaidForTravel(Claim__c claim, Schema.SObjectField travelField)
    {
        String s = (String)claim.get(travelField);
        if(claim.Total_amount_paid_for_Travel__c == null &&
                (s.contains('Bus/coach/rail;') || s.contains('Ferry;') || s.contains('Community Bus;') || s.contains('Air;')))
        {
            Utilities.addRequiredFieldError(Claim__c.Total_amount_paid_for_Travel__c, 'is required when ' + travelField.getDescribe().getLabel() + ' is not by road');
        }
    }

    private void setClaimClient(Claim__c claim, Boolean setApplicant)
    {
        if(claim.Client_Name__c != null && claim.Applicant_Name__c != null)
        {
            return;
        }
        Contact matchedClient = ClientSearchNoSharing.findClient(claim);
        if(matchedClient == null)
        {
            matchedClient = new Contact();
            matchedClient.FirstName = claim.Client_First_Name__c;
            matchedClient.LastName = claim.Client_Surname__c;
            matchedClient.Medicare_Card_Number__c = claim.Medicare_Card_Number_Claim__c;
            matchedClient.Medicare_Sequence_Number__c = claim.Medicare_Sequence_Number_Claim__c;
            matchedClient.AccountId = Utilities.DefaultAccount.Id;
            DMLNoSharing.insertContact(matchedClient);
        }
        claim.Client_Name__c = matchedClient.Id;

        if(setApplicant && claim.Applicant_Name__c == null)
        {
            claim.Applicant_Name__c = Utilities.CurrentUser.ContactId;
        }
    }

    private void getCurrentContact()
    {
        CurrentContact = new Contact();
        // should only show contact details if the claim is for the current user
        for (Contact c : [SELECT    Id,
                Name,
                Salutation,
                FirstName,
                Middle_Name__c,
                LastName,
                BirthDate,
                OtherStreet,
                OtherCity,
                OtherState,
                OtherPostalcode,
                OtherCountry,
                MailingStreet,
                MailingCity,
                MailingState,
                MailingPostalcode,
                MailingCountry,
                Medicare_Card_Number__c,
                Medicare_Sequence_Number__c,
                Concession_Health_Card_Card_Holder__c,
                Card_Number__c,
                Department_of_Veteran_Affairs_card__c,
                DVA_Card_Number__c,
                Display_Other_Address__c,
                Display_Mailing_Address__c,
                HomePhone,
                MobilePhone,
                Email,
                OtherPhone,
                Bank_Account_Name__c,
                Bank_Account_Number__c,
                BSB__c,
                Method_to_receive_remittance_advise_via__c,
                Account.Bank_Account_Name__c,
                Account.BSB__c,
                Account.Bank_Account_Number__c,
                Account.Bank_Account_Email__c
        FROM     Contact
        WHERE    ID IN (SELECT   ContactId
        FROM        User
        WHERE       ID = :UserInfo.getUserId())])
        {
            CurrentContact = c;
        }
    }

    private boolean validateClaimableDistance() {
        boolean isValid = true;
        Claim__c claim = (Claim__c)controller.getRecord();

        if(claim.Location_of_Specialist_appointment_lk__c == null
                || (String.isBlank(claim.Residential_Suburb__c) && String.isBlank(claim.Residential_Street__c) && String.isBlank(claim.Residential_Postcode__c) && String.isBlank(claim.Residential_State__c))
                ) {
            return isValid;
        }
        PATS_Settings__c settings = PATS_Settings__c.getOrgDefaults();
        Decimal minimumDistance = (settings.Minimum_Subsidy_Distance__c == null || settings.Minimum_Subsidy_Distance__c < 0) ? 100 : settings.Minimum_Subsidy_Distance__c.setScale(0);


        if (String.isNotBlank(claim.Location_of_Specialist_appointment_lk__c)) {
            // reget the location of the appointment, in case we have a validation error which can loose it on a new record.
            for (Reference_Data__c rf : [SELECT ID, Suburb_Location__Longitude__s, Suburb_Location__Latitude__s FROM Reference_Data__c WHERE Id = :claim.Location_of_Specialist_appointment_lk__c LIMIT 1]) {
                claim.Location_of_Specialist_appointment_lk__r = rf;
            }
        }

        if (claim.Claimable_Distance__c == null || claim.Claimable_Distance__c < minimumDistance) {
            claim.addError(settings.Minimum_Distance_Error__c);
            isValid = false;
        }

        return isValid;
    }
}