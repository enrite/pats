/**
 * Created by petermoustrides on 25/1/17.
 */
@RestResource(urlMapping='/DistanceCalculator/*')
global class DistanceCalculator
{

    @HttpGet
    global static MDSRouteService.Route doGet()
    {
        try
        {
            RestRequest req = RestContext.request;
            RestResponse res = RestContext.response;

            Decimal startLat = Decimal.valueOf(req.params.get('startLat'));
            Decimal startLng = Decimal.valueOf(req.params.get('startLng'));
            Decimal endLat = Decimal.valueOf(req.params.get('endLat'));
            Decimal endLng = Decimal.valueOf(req.params.get('endLng'));

            MDSRouteService.AuthHeader authHeader = new MDSRouteService.AuthHeader();
            authHeader.Username = DistanceCalculator__c.getOrgDefaults().MapDS_UserName__c;
            authHeader.Password = DistanceCalculator__c.getOrgDefaults().MapDS_Password__c;

            MDSRouteService.LatLong fromAddress = new MDSRouteService.LatLong();
            fromAddress.Latitude = startLat;
            fromAddress.Longitude = startLng;

            MDSRouteService.LatLong toAddress = new MDSRouteService.LatLong();
            toAddress.Latitude = endLat;
            toAddress.Longitude = endLng;

            MDSRouteService.ArrayOfLatLong latLongs = new MDSRouteService.ArrayOfLatLong();
            latLongs.LatLong = new List<MDSRouteService.LatLong>();
            latLongs.LatLong.add(fromAddress);
            latLongs.LatLong.add(toAddress);

            MDSRouteService.SegmentPreference preference = new MDSRouteService.SegmentPreference();
            preference.avoidPreference = 'NoAvoid';
            preference.buildPreference = 'Quickest';
            preference.buildDetailLevel = 'mainRoads';
            preference.outputDetail = 'DistanceTimeOnly';
            preference.retentionPeriod = 'Session';
            preference.BreakDown = 'Single';

            MDSRouteService.MDSRouteServiceSoapSoap msRoute = new MDSRouteService.MDSRouteServiceSoapSoap();
            msRoute.AuthHeader = authHeader;
            return msRoute.CalculateSimpleRoute(latLongs, 'mapds.au', preference);
        }
        catch (Exception ex){
            system.debug(ex);
        }
        return null;
    }
}