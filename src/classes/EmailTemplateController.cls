global class EmailTemplateController 
{ 
    global String UserId
    {
        get;
        set
        {
            UserId = value;
            getRelatedUser(value);
        }
    }

    global User RelatedToUser
    {
        get
        {
            if(RelatedToUser.ID == null && UserId != null)
            {
                getRelatedUser(UserId);
            }
            return RelatedToUser;
        }
        set;
    }

    private void getRelatedUser(ID uId)
    {
        RelatedToUser = new User();
        {
            for(User u : [SELECT Id,
                                Name,
                                Profile.Name,
                                Contact.Salutation
                        FROM    User 
                        WHERE   ID = :uId])
            {
                RelatedToUser = u;
            }
        }
    }
}