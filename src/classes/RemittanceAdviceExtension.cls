public without sharing class RemittanceAdviceExtension
{ 
    public RemittanceAdviceExtension(ApexPages.StandardController con)
    {
        if(!Test.isRunningTest())
        {
            con.addFields(new List<String>{Payment_Batch__c.Total_Payment_Batch_Items_Count__c.getDescribe().getName()});
        }
        controller = con;
    }

    private ApexPages.StandardController controller;
    private transient List<RemittanceItem> tRemittanceItems;

    public String NewLine
    {
        get 
        {
             return '\n';
        }
    }

    public String FileName
    {
        get 
        {
            if(FileName == null)
            {
                Integer filesToday = [SELECT    COUNT() 
                                        FROM    Attachment 
                                        WHERE   Name LIKE '%.REM' AND
                                                DAY_ONLY(convertTimezone(CreatedDate)) = :Date.today() AND
                                                ParentId IN (SELECT ID 
                                                            FROM    Payment_Batch__c)];
                FileName = Datetime.now().format('YYMMdd') + String.valueOf(filesToday).leftPad(2, '0') + '.REM';
            }
            return FileName;
        }
        private set;
    }

    public List<RemittanceItem> RemittanceItems 
    {
        get 
        {
            if(tRemittanceItems == null)
            {
                tRemittanceItems = new List<RemittanceItem>();
                for(Payment_Batch_Item__c pbi : [SELECT  Id,
                                                        Accommodation_Total__c,
                                                        Amount__c,
                                                        Payment_Date__c,
                                                        Email__c,
                                                        Mobile__c,
                                                        (SELECT ID,
                                                                RecordType.Name,
                                                                Claim__r.Name,
                                                                Claim__r.Client_Name__r.PATS_Client_Number__c,
                                                                Claim__r.Client_Name__r.FirstName,
                                                                Claim__r.Client_Name__r.LastName,
                                                                Claim__r.Method_to_receive_remittance_advise_via__c,
                                                                Claim__r.Specialist_Appointment_Date__c,
                                                                Approved_to_Pay_Account__c,
                                                                Approved_to_Pay_Account__r.Bank_Account_Email__c,
                                                                Payment_Batch_Item__r.Email__c,
                                                                Payment_Batch_Item__r.Mobile__c,
                                                                Payee_Invoice_Number__c
                                                        FROM    Claim_Items__r)
                                                FROM    Payment_Batch_Item__c 
                                                WHERE   Failed__c = false AND 
                                                        Payment_Batch__c = :controller.getID() AND 
                                                        // must be linked to claim items
                                                        ID IN (SELECT   Payment_Batch_Item__c 
                                                                FROM    Claim_Item__c)])
                {
                    if(String.isEmpty(pbi.Email__c) && String.isEmpty(pbi.Mobile__c))
                    {
                        continue;
                    }
                    if(pbi.Claim_Items__r.size() > 0) 
                    {
                        Claim_Item__c ci = pbi.Claim_Items__r[0];
                        if(ci.Approved_to_Pay_Account__c!=null)
                        {
                            if(String.isBlank(ci.Approved_to_Pay_Account__r.Bank_Account_Email__c))
                            {
                                continue;
                            }
                        }
                        else
                        {
                            if (String.isBlank(ci.Claim__r.Method_to_receive_remittance_advise_via__c))
                            {
                                continue;
                            }
                            if (ci.Claim__r.Method_to_receive_remittance_advise_via__c == 'Email' && String.isEmpty(pbi.Email__c))
                            {
                                continue;
                            }
                            if (ci.Claim__r.Method_to_receive_remittance_advise_via__c == 'SMS' && String.isEmpty(pbi.Mobile__c))
                            {
                                continue;
                            }
                        }
                    }
                    tRemittanceItems.add(new RemittanceItem(pbi));
                }
            }
            return tRemittanceItems;
        }
    }

    public PageReference generateFile()
    {
        Savepoint sp = Database.setSavepoint();
        try 
        {
            Payment_Batch__c paymentBatch = (Payment_Batch__c)controller.getRecord();
            if(paymentBatch.Total_Payment_Batch_Items_Count__c == 0)
            {
                return Utilities.addError('There are no Payment Batch Items for this Payment Batch.');
            }
            if([SELECT COUNT() 
                        FROM    Attachment 
                        WHERE   Name LIKE '%.eft' AND
                                ParentId = :paymentBatch.ID] == 0)
            {
                 return Utilities.addError('A Payment Batch extract file must be created before a Remittance Advice file can be created.');
            }
             if([SELECT COUNT() 
                        FROM    Attachment 
                        WHERE   Name LIKE '%.REM' AND
                                ParentId = :paymentBatch.ID] > 0)
            {
                 return Utilities.addError('A Remittance Advice file has already been created for this Payment Batch.');
            }
            
            PageReference pg = Page.RemittanceAdviceFile;
            pg.getParameters().put('id', paymentBatch.ID);

            String remittanceString = pg.getContent().toString().replace('\n', '\r\n');
            Attachment att = new Attachment();
            att.ParentId = paymentBatch.ID;
            att.Name = FileName;
            att.Body = Blob.valueOf(remittanceString);
            insert att;

            paymentBatch.Date_Remittance_Advice_Created__c = Datetime.now();
            update paymentBatch;

            pg = new PageReference('/' + paymentBatch.ID);
            return pg;
        }
        catch(Exception ex)
        {
            return Utilities.addError(ex, sp);
        }
    }

    public class RemittanceItem 
    {
        public RemittanceItem(Payment_Batch_Item__c paymentBatchItem)
        {
            BatchItem = paymentBatchItem;
            TravelTypes = '';
            ClaimItem = paymentBatchItem.Claim_Items__r[0];
            BatchItem.Mobile__c = null;
            BatchItem.Email__c = null;
            if(ClaimItem.Approved_to_Pay_Account__c!=null || ClaimItem.Claim__r.Method_to_receive_remittance_advise_via__c == 'Email')
            {
                BatchItem.Email__c = ClaimItem.Payment_Batch_Item__r.Email__c;
            }
            else if(ClaimItem.Claim__r.Method_to_receive_remittance_advise_via__c == 'SMS')
            {
                BatchItem.Mobile__c = ClaimItem.Payment_Batch_Item__r.Mobile__c;
            }
            Set<String> trTypes = new Set<String>();
            for(Claim_Item__c ci : paymentBatchItem.Claim_Items__r)
            {
                trTypes.add(ci.RecordType.Name);
            }
            TravelTypes = String.join(new List<String>(trTypes), '+');    

            String fName = ClaimItem.Claim__r.Client_Name__r.FirstName == null ? '' : ClaimItem.Claim__r.Client_Name__r.FirstName;
            String lName = ClaimItem.Claim__r.Client_Name__r.LastName;
            ClientFullName = fName + ' ' + lName;
            ClientFormattedName = lName + ', ' + fName;
        }
        public Payment_Batch_Item__c BatchItem {get;set;}
        public Claim_Item__c ClaimItem {get;set;}
        public String TravelTypes {get;set;}
        public String ClientFullName {get;set;}
        public String ClientFormattedName {get;set;}
        public String TravelTotal
        {
            get
            {
                return getFormattedDecimal(BatchItem.Amount__c - BatchItem.Accommodation_Total__c);
            }
        }
        public String AccomTotal 
        {
            get
            {
                return getFormattedDecimal(BatchItem.Accommodation_Total__c);
            }
        }
        public String Amount 
        {
            get
            {
                return getFormattedDecimal(BatchItem.Amount__c);
            }
        }

        private String getFormattedDecimal(Decimal d)
        {
            if(d == null)
            {
                return '';
            }
            return d.setScale(2).toPlainString();
        }
    }
}