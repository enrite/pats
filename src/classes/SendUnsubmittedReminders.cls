global class SendUnsubmittedReminders implements Schedulable {
	/**
	 * @description Executes the scheduled Apex job. 
	 * @param sc contains the job ID
	 */ 
	global void execute(SchedulableContext sc) 
    {
        Integer interval = PATS_Settings__c.getOrgDefaults().Unsubmitted_Reminder__c.intValue();
        Map<Id, Claim__c> claims = new Map<Id, Claim__c>( [SELECT   ID,
                                                                    Applicant_Name__c
                                                            FROM    Claim__c
                                                            WHERE   Unsubmitted_Reminder_Email_Sent__c = null AND 
                                                                    Status__c = 'Created' AND 
                                                                    Date_submitted__c <= :Date.today().addDays(interval * -1)]);
        EmailTemplate template = [SELECT ID 
                                FROM    EmailTemplate 
                                WHERE   DeveloperName = 'Unsubmitted_Reminder'];
        OrgWideEmailAddress fromAddress = [SELECT ID 
                                            FROM    OrgWideEmailAddress
                                            WHERE   DisplayName = 'PATS'];
        List<Messaging.SingleEmailMessage> msgs = new List<Messaging.SingleEmailMessage>();
        for (Claim__c cl : claims.values())
        {
            Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
            msg.setTemplateID(template.ID);
            msg.setSaveAsActivity(false);
            msg.setTargetObjectId(cl.Applicant_Name__c);
            msg.setOrgWideEmailAddressId(fromAddress.ID);
            msgs.add(msg);
        }

        Messaging.SendEmailResult[] emResults = Messaging.sendEmail(msgs, false);
        for(Messaging.SendEmailResult emResult : emResults)
        {
            if(!emResult.isSuccess())
            {
                for(Messaging.SendEmailError emError : emResult.getErrors())
                {
                    claims.remove(emError.getTargetObjectId());
                }
            }
        }
        for (Claim__c cl : claims.values())
        {
            cl.Unsubmitted_Reminder_Email_Sent__c = Date.today();
        }
        update claims.values();
	}
}