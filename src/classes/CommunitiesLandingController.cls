/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
public with sharing class CommunitiesLandingController {
    
    // Code we will invoke on page load.
    public PageReference forwardToStartPage() 
    {
        String profileName = Utilities.CurrentUser.Profile.Name;
        if(profileName == PATSConstants.Profile_Delegate ||
                profileName == PATSConstants.Profile_Specialist)
        {
             return Page.ClientSearch.setRedirect(true);
        }
        else if(profileName == PATSConstants.Profile_ClientApplicant || profileName == PATSConstants.Profile_ThirdParty)
        {
            return Page.MyApplications.setRedirect(true);
        }
        return new PageReference(Network.getLoginUrl(Network.getNetworkId()));
    }
    
    public CommunitiesLandingController() {}
}