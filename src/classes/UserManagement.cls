public class UserManagement
{
    public static void createCommunityUser(String contactId)
    {
        createCommunityUsers(new Set<Id>
        {
                contactId
        });
    }

    public static PageReference logoutUser()
    {
        User u = [SELECT id, email, username, usertype, communitynickname, timezonesidkey, languagelocalekey, firstname, lastname, phone, title,
                street, city, country, postalcode, state, localesidkey, mobilephone, extension, fax, contact.email
        FROM User
        WHERE id = :UserInfo.getUserId()];
        
        if (u.usertype != 'GUEST')
        {
            return new PageReference('/secur/logout.jsp');
        }
        return null;
    }

    @future
    public static void createCommunityUsers(Set<Id> contactIds)
    {
        String errorMessage = '';
        try
        {
            Organization org = [SELECT Id,
                                        Name,
                                        IsSandbox
                                FROM Organization
                                LIMIT 1];

            Account defaultAccount = Utilities.DefaultAccount;
            Map<String, Profile> profilesByName = new Map<String, Profile>();
            for(Profile p :[SELECT ID,
                                    Name
                            FROM Profile])
            {
                profilesByName.put(p.Name, p);
            }
            List<User> users = new List<User>();
            Set<String> contactEmails = new Set<String>();
            Set<String> userEmails = new Set<String>();
            List<Contact> contacts = [SELECT ID,
                                            FirstName,
                                            LastName,
                                            Email,
                                            PATS_Client_Number__c,
                                            RecordType.DeveloperName,
                                            Application_For_Access_As__c
                                    FROM    Contact
                                    WHERE   ID IN :contactIds AND
                                            Email != null AND
                                            ID NOT IN(SELECT ContactId
                                                    FROM User)];
//            for(Contact c : contacts)
//            {
//                contactEmails.add(c.Email);
//            }
//            for(User u : [SELECT    Email
//                            FROM    User
//                            WHERE   Email IN :contactEmails AND
//                                    Profile.Name IN :(new Set<String>{PATSConstants.Profile_Delegate,
//                                                                    PATSConstants.Profile_Specialist,
//                                                                    PATSConstants.Profile_ClientApplicant,
//                                                                    PATSConstants.Profile_ThirdParty})])
//            {
//                userEmails.add(u.Email);
//            }
            for(Contact c : contacts)
            {
//                if(userEmails.contains(c.Email))
//                {
//                    errorMessage += 'A user already exists with the email address ' + c.Email + '\n';
//                    continue;
//                }

                User u = new User();
                u.ContactId = c.Id;
                String clientId = c.PATS_Client_Number__c.deleteWhiteSpace();
                u.Username = clientId + '@pats.sa' + (org.IsSandbox ? ('.' + org.Name.replaceAll('[ -]', '')) : '');
                u.CommunityNickname = clientId;
                u.Email = c.Email;
                u.FirstName = c.FirstName;
                u.LastName = c.LastName;
                if(c.RecordType.DeveloperName == 'Referring_Delegate')
                {
                    u.ProfileId = profilesByName.get(PATSConstants.Profile_Delegate).ID;
                }
                else if(c.RecordType.DeveloperName == 'Specialist')
                {
                    u.ProfileId = profilesByName.get(PATSConstants.Profile_Specialist).ID;
                }
                else if(c.RecordType.DeveloperName == 'Applicant_Client')
                {
                    u.ProfileId = profilesByName.get(PATSConstants.Profile_ClientApplicant).ID;
                }
                else if(c.RecordType.DeveloperName == 'Third_Party_Applicant')
                {
                    u.ProfileId = profilesByName.get(PATSConstants.Profile_ThirdParty).ID;
                }
                u.TimeZoneSidKey = defaultAccount.Owner.TimeZoneSidKey;
                u.LocaleSidKey = defaultAccount.Owner.LocaleSidKey;
                u.LanguageLocaleKey = defaultAccount.Owner.LanguageLocaleKey;
                u.EmailEncodingKey = defaultAccount.Owner.EmailEncodingKey;
                u.Alias = u.CommunityNickname;
                users.add(u);
            }
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.EmailHeader.triggerUserEmail = true;
            Database.SaveResult[] srs = Database.insert(users, dmo);

            for(Database.SaveResult sr : srs)
            {
                System.debug(sr.isSuccess());
                if(!sr.isSuccess())
                {
                    for(Database.Error e : sr.getErrors())
                    {
                        System.debug(e.getMessage());
                        errorMessage += e.getMessage() + '\n';
                    }
                }
            }
        }
        catch(Exception ex)
        {
            errorMessage += ex.getMessage();
        }
        if(String.isNotBlank(errorMessage))
        {
            EmailTemplate et;
            String templateName = 'Automatic_user_creation_failed';
            try
            {
                 et = [SELECT   ID
                        FROM    EmailTemplate
                        WHERE   DeveloperName = :templateName AND
                                IsActive = true];
            }
            catch(Exception ex)
            {
                throw new PATSException(templateName + ' email template not available');
            }
            Messaging.SingleEmailMessage msg = Messaging.renderStoredEmailTemplate(et.ID, UserInfo.getUserId(), null);
            msg.setPlainTextBody('The following errors were encountered:\n\n' + errorMessage);
            msg.setSaveAsActivity(false);
            Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{msg});
        }
    }
}