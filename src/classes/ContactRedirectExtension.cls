public with sharing class ContactRedirectExtension
{
    public ContactRedirectExtension(ApexPages.StandardController c)
    {
        controller = c;
    }

    private ApexPages.StandardController controller;

    public PageReference redirect()
    {
        PageReference pg = new PageReference('/' + Contact.SObjectType.getDescribe().getKeyPrefix() + '/e');
        Map<String, String> params = ApexPages.currentPage().getParameters();
        for(String s : params.keySet())
        {
            if(s != null && s != 'save_new')
            {
                pg.getParameters().put(s, params.get(s));
            }
        }
        pg.getParameters().put('con19state', 'SA');
        pg.getParameters().put('con19country', 'Australia');
       // pg.getParameters().put('con4', 'PATS');
        //pg.getParameters().put('con4_lkid', '001O000000xXFOF');
        pg.getParameters().put('nooverride', '1');

        return pg.setRedirect(true);
    }
}