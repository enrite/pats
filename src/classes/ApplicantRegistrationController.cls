public with sharing class ApplicantRegistrationController
{
    private final string USER_EXISTS = 'A user account already exists. Please <a href="/login">login</a> using your current login details or if you have forgotten your password please use the "Forgot Your Password" <a href="/secur/forgotpassword.jsp?locale=us&lqs=">link</a> to reset it';

    public ApplicantRegistrationController(ApexPages.StandardController ctrlr)
    {
        controller = ctrlr;
    }

    public PageReference logout()
    {
        return UserManagement.logoutUser();
    }

    private ApexPages.StandardController controller;

    public Boolean HasMessages
    {
        get
        {
            return ApexPages.hasMessages();
        }
    }

    public ID applicantRecordTypeId
    {
        get
        {
            return Schema.sObjectType.Contact.getRecordTypeInfosByName().get('Applicant / Client').getRecordTypeId();
        }
    }

    public void savePractitioner()
    {
        try
        {
            Contact practitioner = (Contact) controller.getRecord();
            Map<String, RecordTypeInfo> recTypesByName = Contact.sObjectType.getDescribe().getRecordTypeInfosByName();

            if (practitioner.Application_For_Access_As__c == 'Referring Delegate')
            {
                practitioner.RecordTypeId = recTypesByName.get('Referring Delegate').getRecordTypeId();
            }
            else if (practitioner.Application_For_Access_As__c == 'Specialist')
            {
                Utilities.checkRequiredField(practitioner, Contact.My_AHPRA_Number__c);
                practitioner.RecordTypeId = recTypesByName.get('Specialist').getRecordTypeId();
            }
            else if (practitioner.Application_For_Access_As__c == 'Specialist Delegate')
            {
                Utilities.checkRequiredField(practitioner, Contact.Practitioners_Name__c);
                Utilities.checkRequiredField(practitioner, Contact.Practitioners_AHPRA_number__c);
                practitioner.RecordTypeId = recTypesByName.get('Specialist').getRecordTypeId();
            }
            if(practitioner.Application_For_Access_As__c != 'Specialist Delegate' && practitioner.My_AHPRA_Number__c != null)
            {
                for (Contact c :
                [
                        SELECT ID
                        FROM Contact
                        WHERE My_AHPRA_Number__c = :practitioner.My_AHPRA_Number__c

                ])
                {
                    Utilities.addError('A user account already exists with this AHPRA number. Please login using your current login details or if you have forgotten your password please use the "Forgot Your Password" link to reset it');
                }
            }
            if (HasMessages)
            {
                return;
            }
            insert practitioner;
        }
        catch (Exception ex)
        {
            Utilities.addError(ex);
        }
    }

    public void saveThirdParty()
    {
        try
        {
            Contact thirdPartyContact = (Contact) controller.getRecord();
            Map<String, RecordTypeInfo> recTypesByName = Contact.sObjectType.getDescribe().getRecordTypeInfosByName();
            thirdPartyContact.RecordTypeId = recTypesByName.get('Third Party Applicant').getRecordTypeId();

            Utilities.checkRequiredField(thirdPartyContact, Contact.FirstName);
            Utilities.checkRequiredField(thirdPartyContact, Contact.LastName);
            Utilities.checkRequiredField(thirdPartyContact, Contact.Email);
            Utilities.checkRequiredField(thirdPartyContact, Contact.OtherStreet);
            Utilities.checkRequiredField(thirdPartyContact, Contact.OtherCity);
            Utilities.checkRequiredField(thirdPartyContact, Contact.OtherState);
            Utilities.checkRequiredField(thirdPartyContact, Contact.OtherCountry);
            if (HasMessages)
            {
                return;
            }

            insert thirdPartyContact;
        }
        catch (Exception ex)
        {
            String msg = '';
            if(ex instanceof DmlException)
            {
                DmlException dex = (DmlException)ex;
                for(Integer i = 0; i < dex.getNumDml(); i++)
                {
                    if(dex.getDmlType(i) == StatusCode.DUPLICATES_DETECTED)
                    {
                        msg = Label.Duplicate_Contact;
                        break;
                    }
                }
            }
            if(String.isBlank(msg))
            {
                msg = ex.getMessage();
            }
            Utilities.addError(msg);
        }
    }

    public void saveOverride()
    {
        try
        {
            String errMsg = '';
            Contact applicant = (Contact) controller.getRecord();
            Utilities.checkRequiredField(applicant, Contact.FirstName);
            Utilities.checkRequiredField(applicant, Contact.LastName);
            Utilities.checkRequiredField(applicant, Contact.Medicare_Card_Number__c);
            Utilities.checkRequiredField(applicant, Contact.Medicare_Sequence_Number__c);
            Utilities.checkRequiredField(applicant, Contact.Birthdate);
            Utilities.checkRequiredField(applicant, Contact.Email);
            Utilities.checkRequiredField(applicant, Contact.Aboriginal_Torres_Strait_Islander__c);
            Utilities.checkRequiredField(applicant, Contact.OtherStreet);
            Utilities.checkRequiredField(applicant, Contact.OtherCity);
            Utilities.checkRequiredField(applicant, Contact.OtherState);
            Utilities.checkRequiredField(applicant, Contact.OtherCountry);
            Utilities.checkRequiredField(applicant, Contact.MailingStreet);
            Utilities.checkRequiredField(applicant, Contact.MailingCity);
            Utilities.checkRequiredField(applicant, Contact.MailingState);
            Utilities.checkRequiredField(applicant, Contact.MailingCountry);
            Utilities.checkRequiredField(applicant, Contact.Department_of_Veteran_Affairs_card__c);
            Utilities.checkRequiredField(applicant, Contact.Method_to_receive_remittance_advise_via__c);
            if (applicant.HomePhone == null && applicant.MobilePhone == null)
            {
                Utilities.addError('At least one phone number is required');
            }
            validateSuburb(Contact.OtherCity, applicant.OtherCity, applicant.OtherPostalcode);
            validateSuburb(Contact.MailingCity, applicant.MailingCity, applicant.MailingPostalcode);
            if(applicant.Method_to_receive_remittance_advise_via__c == 'SMS' && applicant.MobilePhone == null)
            {
                Utilities.addRequiredFieldError(Contact.MobilePhone, 'is required when ' + Contact.Method_to_receive_remittance_advise_via__c.getDescribe().getLabel() + ' is SMS');
            }
            if(applicant.Bank_Account_Number__c != null && !applicant.Bank_Account_Number__c.replaceAll('[_ -]', '').isNumeric())
            {
                Utilities.addRequiredFieldError(Contact.Bank_Account_Number__c, 'must be numeric');
            }
            if(applicant.BSB__c != null)
            {
                if(!applicant.BSB__c.replaceAll('[ -]', '').isNumeric())
                {
                    Utilities.addRequiredFieldError(Contact.BSB__c, 'must be numeric');
                }
                else
                {
                    Reference_Data__c rd = Utilities.getBSB(applicant.BSB__c);
                    if (rd != null)
                    {
                        applicant.BSB__c = rd.Name;
                    }
                    else
                    {
                        Utilities.addError('BSB number is not valid');
                    }
                }
            }
            if(applicant.Bank_Account_Name__c != null || applicant.BSB__c != null || applicant.Bank_Account_Number__c != null)
            {
                if(applicant.Bank_Account_Name__c == null)
                {
                    Utilities.addRequiredFieldError(Contact.Bank_Account_Name__c);
                }
                if(applicant.BSB__c == null)
                {
                    Utilities.addRequiredFieldError(Contact.BSB__c);
                }
                if(applicant.Bank_Account_Number__c == null)
                {
                    Utilities.addRequiredFieldError(Contact.Bank_Account_Number__c);
                }
            }
            if (applicant.Card_Number__c != null && applicant.Expiry_Date__c == null)
            {
                Utilities.addRequiredFieldError(Contact.Expiry_Date__c);
            }
            if (applicant.Expiry_Date__c != null && applicant.Expiry_Date__c < Date.today())
            {
                Utilities.addRequiredFieldError(Contact.Expiry_Date__c, 'must be in the future');
            }
            if (applicant.Department_of_Veteran_Affairs_card__c != null &&
                    applicant.Department_of_Veteran_Affairs_card__c != 'No card' &&
                    applicant.DVA_Card_Number__c == null)
            {
                Utilities.addRequiredFieldError(Contact.DVA_Card_Number__c);
            }
            if (HasMessages)
            {
                return;
            }

            List<Contact> contacts  = DMLNoSharing.matchingContacts(applicant);

            system.debug('ContactsMatched'+contacts);
            if(contacts != null && contacts.size()> 0)
            {
                Contact contact = contacts[0];
                Boolean contactMatches = contact.FirstName == applicant.FirstName && contact.LastName == applicant.LastName;
                User user = DMLNoSharing.getUser(contact.Id);

                if(user == null && contactMatches)
                {
                    applicant.Id = contact.Id;
                }
                else
                {
                    if(contactMatches) Utilities.addError(USER_EXISTS);
                }
            }

            if (HasMessages)
            {
                return;
            }
            applicant.AccountID = Utilities.DefaultAccount.ID;
            applicant.RecordTypeId = applicantRecordTypeId;

            system.debug('*** ApplicantId'+applicant.Id);
            if(applicant.Id == null)
            {
                insert applicant;
            }
            else
                DMLNoSharing.updateContact(applicant);

            UserManagement.createCommunityUser(applicant.ID);
        }
        catch (Exception ex)
        {
            Utilities.addError(ex);
        }
    }

    private void validateSuburb(Schema.SObjectField field, String locality, String postcode)
    {
        if (Utilities.getLocation(locality, postcode) == null)
        {
            Utilities.addRequiredFieldError(field, locality + ' and ' + postcode + ' is not a valid locality/postcode');
        }
    }
}