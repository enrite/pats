public with sharing class PracticeLocationController
{
    private Practice_Location__c record;

    public PracticeLocationController(ApexPages.StandardController con)
    {
        record = (Practice_Location__c)con.getRecord();
        if(ApexPages.currentPage().getParameters().containsKey('ContactId'))
        {
            record.Contact_Referring_Delegate_Specialist__c = ApexPages.currentPage().getParameters().get('ContactId');
        }
    }
}