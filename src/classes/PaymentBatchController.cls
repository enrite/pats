public without sharing class PaymentBatchController
{
    private final string PAYMENT_HAS_BEEN_PROCESSED = 'The Payment Batch has already been processed. No further changes can be made to it.';
    private final string PAYMENT_ITEMS_NOT_CREATED = 'The Payment Batch has no Payment Items to process. You must Generate Payment Items and review items before you can generate the extract.';
    private final string DELETE_GENERATED_BATCH_ITEMS = 'ERROR: Unable to delete existing payment batch items.';
    private final string CAN_NOT_RETRIEVE_CLAIM_ITEMS = 'ERROR: Unable to retireve Claim Items.';
    private final string ACCOMMODATION_RECORDTYPE_NAME = 'Accommodation';
    private final string TOTALS_MISMATCH = 'Sum of payment batch items does not match the sum of claim items approved to pay, payment items will need to be generated again';

    private Payment_Batch__c record;

    public PaymentBatchController(ApexPages.StandardController con)
    {
        record = (Payment_Batch__c) con.getRecord();
    }

    public Payment_Batch__c PaymentBatch
    {
        get
        {
            if(PaymentBatch == null)
            {
                PaymentBatch = [SELECT ID,
                                        Name,
                                        Total_Payment_Batch_Items_Amount__c,
                                        Total_Payment_Batch_Items_Count__c
                                FROM    Payment_Batch__c
                                WHERE   ID = :record.ID];
            }
            return PaymentBatch;
        }
        private set;
    }

    public List<Payment_Batch_Item__c> getPaymentItems()
    {
        return [SELECT ID,
                        Name,
                        Failed__c,
                        Amount__c,
                        BSB__c,
                        Account_Number__c,
                        Title_of_Account__c
                FROM    Payment_Batch_Item__c
                WHERE   Payment_Batch__c = :record.ID];
    }

    public PageReference GenerateExtractFile()
    {
        try
        {
system.debug('GenerateExtractFile');
            checkBatchTotal(record);

            record = [
                    SELECT Name, Date_Generated__c, (
                            select Account_Number__c,
                                    BSB__c,
                                    Title_of_Account__c,
                                    Email__c,
                                    Mobile__c,
                                    Amount__c,
                                    Accommodation_Total__c
                            from Payment_Batch_Items__r
                    )
                    from Payment_Batch__c
                    where id = :record.Id
            ];

            if(record.Payment_Batch_Items__r.size() == 0) {
                Utilities.addError(PAYMENT_ITEMS_NOT_CREATED);
                return null;
            }

            PageReference extractPage = Page.PaymentBatchExtract;
            extractPage.getParameters().put('id', record.id);

            String extractBlob = extractPage.getContent().toString().replace('\n', '\r\n');

            Attachment a = new Attachment();
            a.ParentId = record.Id;
            a.Name = record.Name + '.eft';
            a.Body = Blob.valueOf(extractBlob);
            insert a;

            record.Date_Generated__c = DateTime.now();
            update record;

            Approval.lock(record, false);

            return new PageReference('/' + record.Id);
        }
        catch(Exception ex)
        {
            Utilities.addError(ex);
            return null;
        }
    }

    public PageReference GeneratePaymentItems()
    {
        record = [SELECT Date_Generated__c from Payment_Batch__c where id = :record.Id];

        if (record.Date_Generated__c != null)
        {
            Utilities.addError(PAYMENT_HAS_BEEN_PROCESSED);
            return null;
        }
        Savepoint sp = Database.setSavepoint();
        try
        {
            deleteExistingClaimItems(record);

            Map<String, Payment_Batch_Item__c> paymentBatchItemMap = new Map<String, Payment_Batch_Item__c>();
            List<Batch_Item_Claim_Item__c> batchItemClaimItems = new List<Batch_Item_Claim_Item__c>();
            Relationships claimToPaymentItemRelationship = new Relationships();
            Relationships batchItemClaimItemRelationship = new Relationships();

            List<Claim_Item__c> approvedClaimItems = getApprovedClaimItems();
            for (Claim_Item__c claimItem : approvedClaimItems)
            {
                String paymentBatchItemKey = claimItem.Claim__c + claimItem.Payee_Invoice_Number__c;
                if (!paymentBatchItemMap.containsKey(paymentBatchItemKey))
                {
                    paymentBatchItemMap.put(paymentBatchItemKey, paymentBatchItemFromClaimItem(claimItem));
                }

                Payment_Batch_Item__c paymentBatchItem = paymentBatchItemMap.get(paymentBatchItemKey);
                if (claimItem.RecordType.Name == ACCOMMODATION_RECORDTYPE_NAME)
                {
                    paymentBatchItem.Accommodation_Total__c += claimItem.Total_Approved_to_Pay__c;
                }
                paymentBatchItem.Amount__c += claimItem.Total_Approved_to_Pay__c;


                claimToPaymentItemRelationship.add(claimItem, Claim_Item__c.Payment_Batch_Item__c.getDescribe().getSObjectField(), paymentBatchItem);

                Batch_Item_Claim_Item__c batchItemClaimItem = new Batch_Item_Claim_Item__c(Claim_Item__c=claimItem.Id);
                batchItemClaimItems.add(batchItemClaimItem);
                batchItemClaimItemRelationship.add(batchItemClaimItem, Batch_Item_Claim_Item__c.Payment_Batch_Item__c.getDescribe().getSObjectField(), paymentBatchItem);
            }

            insert paymentBatchItemMap.values();
            claimToPaymentItemRelationship.resolve();
            batchItemClaimItemRelationship.resolve();
            Approval.unlock(approvedClaimItems);

            insert batchItemClaimItems;
            Database.SaveResult [] updateResults = Database.update(approvedClaimItems, false);
            boolean hasErrors = false;
            for(Integer i=0; i < updateResults.size(); i++)
            {
                Database.SaveResult ur = updateResults[i];
                Claim_Item__c claimItem =  approvedClaimItems[i];
                if (!ur.isSuccess())
                {
                    for (Database.Error e : ur.getErrors())
                    {
                        hasErrors = true;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '<a href="/'+claimItem.id+'">'+claimItem.Name+'</a>&nbsp;'+ e.getMessage()));
                    }
                }
            }
            if(hasErrors)
            {
                if(sp != null)
                {
                    Database.rollback(sp);
                }
                return null;
            }
            Approval.lock(approvedClaimItems);

            checkBatchTotal(record);

            return (new PageReference('/' + record.Id)).setRedirect(true);
        }
        catch(Exception ex)
        {
            //system.debug(ex.getMessage());
            Utilities.addError(ex, sp);
            return null;
        }
    }

    private void checkBatchTotal(Payment_Batch__c record)
    {
        AggregateResult ar = [SELECT SUM(Total_Approved_to_Pay__c) sum
                            FROM    Claim_Item__c
                            WHERE   Payment_Batch_Item__r.Payment_Batch__c = :record.ID];
        Decimal claimTotal = (Decimal)ar.get('sum');

         ar = [SELECT SUM(Amount__c) sum
                FROM    Payment_Batch_Item__c
                WHERE   Payment_Batch__c = :record.ID];
        Decimal batchTotal = (Decimal)ar.get('sum');
        if(claimTotal != batchTotal)
        {
            throw new PATSException(TOTALS_MISMATCH);
        }
    }

    private void deleteExistingClaimItems(Payment_Batch__c record)
    {
        try
        {
            delete [select id from Payment_Batch_Item__c where Payment_Batch__c = :record.id];
        }
        catch (Exception ex)
        {
            system.debug(ex);
            Utilities.addError(DELETE_GENERATED_BATCH_ITEMS);
        }
    }


    private Payment_Batch_Item__c paymentBatchItemFromClaimItem(Claim_Item__c claimItem)
    {
        Boolean accountPayment = claimItem.Approved_to_Pay_Account__c != null;
        Payment_Batch_Item__c paymentBatchItem = new Payment_Batch_Item__c();
        paymentBatchItem.Payment_Batch__c = record.id;
        paymentBatchItem.Account_Number__c = accountPayment ? claimItem.Approved_to_Pay_Account__r.Bank_Account_Number__c : claimItem.Claim__r.Bank_Account_Number__c;
        paymentBatchItem.BSB__c = accountPayment ? claimItem.Approved_to_Pay_Account__r.BSB__c : claimItem.Claim__r.BSB__c;
        paymentBatchItem.Title_of_Account__c = accountPayment ? claimItem.Approved_to_Pay_Account__r.Bank_Account_Name__c : claimItem.Claim__r.Bank_Account_Name__c;
        paymentBatchItem.Email__c = accountPayment ? claimItem.Approved_to_Pay_Account__r.Bank_Account_Email__c : claimItem.Claim__r.Email__c;
        paymentBatchItem.Mobile__c = accountPayment ? claimItem.Approved_to_Pay_Account__r.Bank_Account_Mobile__c : claimItem.Claim__r.Mobile_Phone__c;
        paymentBatchItem.Amount__c = 0;
        paymentBatchItem.Accommodation_Total__c = 0;

        return paymentBatchItem;
    }

    private List<Claim_Item__c> getApprovedClaimItems()
    {
        List<Claim_Item__c> result = new List<Claim_Item__c>();
        try
        {
            result =
            [
                    SELECT Id,
                            Name,
                            Total_Approved_to_Pay__c,
                            Payee_Invoice_Number__c,
                            Claim__c,
                            Claim_Item_Status__c,
                            Payment_Batch_Item__c,
                            Approved_to_Pay_Account__c,
                            Approved_to_Pay_Account__r.Bank_Account_Number__c,
                            Approved_to_Pay_Account__r.BSB__c,
                            Approved_to_Pay_Account__r.Bank_Account_Name__c,
                            Approved_to_Pay_Account__r.Bank_Account_Email__c,
                            Approved_to_Pay_Account__r.Bank_Account_Mobile__c,
                            Claim__r.Bank_Account_Name__c,
                            Claim__r.Bank_Account_Number__c,
                            Claim__r.Email__c,
                            Claim__r.Mobile_Phone__c,
                            Claim__r.BSB__c,
                            RecordType.Name
                    FROM Claim_Item__c
                    where Claim_Item_Status__c = 'Approved' and
                    Payment_Batch_Item__c = null and Total_Approved_to_Pay__c > 0
                    order by Claim__c
            ];
        }
        catch (Exception ex)
        {
            system.debug(ex);
            Utilities.addError(CAN_NOT_RETRIEVE_CLAIM_ITEMS);
        }

        return result;
    }
}