public without sharing class MyApplicationsController
{
    public string CurrentContactAccountName
    {
        get
        {
            if (CurrentContactAccountName == null)
            {
                setCurrentContact();
            }
            return CurrentContactAccountName;
        }
        set;
    }

    private Id CurrentContactId
    {
        get
        {
            if (CurrentContactId == null)
            {
                setCurrentContact();
            }
            return CurrentContactId;
        }
        set;
    }

    private void setCurrentContact()
    {
        for (User u : [SELECT Id,
                ContactId,
                Account.Name
        FROM User
        WHERE Id = :UserInfo.getUserId()])
        {
            CurrentContactId = u.ContactId;
            CurrentContactAccountName = u.Account.Name;
        }
    }

    public List<Claim__c> MyOpenClaims
    {
        get
        {
            if (MyOpenClaims == null)
            {
                MyOpenClaims = CurrentContactAccountName != 'PATS' ?
                        [
                                SELECT Id,
                                        Name,
                                        Specialist_Appointment_Date__c,
                                        Specialist_Appointment_Time__c,
                                        Status__c,
                                        Applicant_Name__c,
                                        Applicant_Name__r.Name,
                                        Client__c,
                                        Client_Name__c,
                                        Client_Name__r.Name
                                FROM Claim__c
                                WHERE (Applicant_Name__c = :CurrentContactId
                                OR Applicant_Name__r.Account.Name = :CurrentContactAccountName)
                                AND Status__c = 'Created'
                                ORDER BY CreatedDate DESC
                        ] :
                        [
                                SELECT Id,
                                        Name,
                                        Specialist_Appointment_Date__c,
                                        Specialist_Appointment_Time__c,
                                        Status__c,
                                        Applicant_Name__c,
                                        Applicant_Name__r.Name,
                                        Client__c,
                                        Client_Name__c,
                                        Client_Name__r.Name
                                FROM Claim__c
                                WHERE Applicant_Name__c = :CurrentContactId
                                AND Status__c = 'Created'
                                ORDER BY CreatedDate DESC
                        ];
            }

            system.debug(MyOpenClaims);
            return MyOpenClaims;
        }
        set;
    }

    public List<Claim_View__c> ClaimsOnBehalf
    {
        get
        {
            if (ClaimsOnBehalf == null)
            {
                ClaimsOnBehalf = [SELECT Id,
                                        Client_Name__c,
                                        Claim_Number__c,
                                        Claim_Status__c,
                                        Applicant_Name__c,
                                        Claim_View__r.Specialist_Appointment_Date__c,
                                        Claim_View__r.Specialist_Appointment_Time__c
                                FROM    Claim_View__c
                                WHERE   Client_View__c = :CurrentContactId AND 
                                        Claim_View__r.Applicant_Name__c != :CurrentContactId
                                ORDER BY CreatedDate DESC];
            }
            return ClaimsOnBehalf;
        }
        set;
    }
    
    public List<Claim__c> ClosedClaims
    {
        get
        {
            if (ClosedClaims == null)
            {
                ClosedClaims = CurrentContactAccountName != 'PATS' ?
                        [SELECT Id,
                                Name,
                                Specialist_Appointment_Date__c,
                                Specialist_Appointment_Time__c,
                                Status__c,
                                Applicant_Name__c,
                                Applicant_Name__r.Name,
                                Client_Name__c,
                                Client_Name__r.Name
                        FROM Claim__c
                        WHERE (Applicant_Name__c = :CurrentContactId
                        OR Applicant_Name__r.Account.Name = :CurrentContactAccountName)
                        AND Status__c != 'Created'
                        ORDER BY CreatedDate DESC]
                        :[SELECT Id,
                                       Name,
                                       Specialist_Appointment_Date__c,
                                       Specialist_Appointment_Time__c,
                                       Status__c,
                                       Applicant_Name__c,
                                       Applicant_Name__r.Name,
                                       Client_Name__c,
                                       Client_Name__r.Name
                                FROM Claim__c
                                WHERE (Applicant_Name__c = :CurrentContactId
                                       OR Client_Name__c = :CurrentContactId)
                                AND Status__c != 'Created'
                                ORDER BY CreatedDate DESC];
            }
            return ClosedClaims;
        }
        set;
    }
}