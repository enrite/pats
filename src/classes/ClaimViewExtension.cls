public class ClaimViewExtension 
{
    public ClaimViewExtension(ApexPages.StandardController ctrlr)
    {
        if(!Test.isRunningTest())
        {
            ctrlr.addFields(new String[]{'Claim_View__c','Client_View__c'});
        }
        controller = ctrlr;
    }

    private ApexPages.StandardController controller;

    public Claim__c ClaimView
    {
        get 
        {
            if (ClaimView == null)
            {
                Claim_View__c clView = (Claim_View__c)controller.getRecord();
                ClaimView = ClaimSearchNoSharing.getClaimClone(clView.Claim_View__c);
            }
            return ClaimView;
        }
        private set;
    }
}