public without sharing class ThirdPartySearchController
{
    private Contact userContact;

    public ClaimSearch Parameters
    {
        get
        {
            if (Parameters == null)
            {
                Parameters = new ClaimSearch();
            }
            return Parameters;
        }
        set;
    }

    public Id SelectedClaimId;

    public List<Claim__c> SearchResults { get; set; }

    public boolean SpecialistUser
    {
        get
        {
            if (SpecialistUser == null)
            {
                User u =
                [
                        SELECT Id,
                                ContactId,
                                Profile.Name
                        FROM User
                        WHERE Id = :UserInfo.getUserId()
                ];

                userContact = [select RecordType.Name, Type_of_Service__c, Specialist_Service__c from Contact where id = :u.ContactId];

                SpecialistUser = u.Profile.Name.contains('Specialist');
            }
            return SpecialistUser;
        }
        set;
    }

    public PageReference Search()
    {
        Parameters.SearchVia = string.isBlank(Parameters.PATSClaimNumber)?(string.isBlank(Parameters.PATSClientNumber)?'medicare':'pats'):'claim';
system.debug(Parameters);
        List<Contact> contacts = new List<Contact>([
                SELECT Id, Medicare_Card_Number__c, Medicare_Sequence_Number__c, Salutation
                FROM Contact
                WHERE FirstName = :Parameters.FirstName
                AND LastName = :Parameters.LastName
                AND ((PATS_Client_Number__c = :Parameters.PATSClientNumber and PATS_Client_Number__c != null) OR
                (Medicare_Card_Number__c = :Parameters.MedicareCardNumber and Medicare_Card_Number__c != null
                AND Medicare_Sequence_Number__c = :Parameters.MedicareSequenceNumber and Medicare_Sequence_Number__c != null))
        ]);

        SearchResults =
        [
                SELECT Id,
                        Name,
                        Specialist_Appointment_Date__c,
                        Specialist_Appointment_Time__c,
                        Client_Name__r.Salutation,
                        Client_Name__r.Medicare_Card_Number__c,
                        Client_Name__r.Medicare_Sequence_Number__c,
                        Applicant_Name__c
                FROM Claim__c
                WHERE Status__c != 'Approved'
                AND Client_First_Name__c = :Parameters.FirstName
                AND Client_Surname__c = :Parameters.LastName
                AND (
                        (Name = :Parameters.PATSClaimNumber or (Name like :(String.ISBLANK(Parameters.PATSClaimNumber) ? '%' : '~'))) AND
                        (Client_Name__r.PATS_Client_Number__c = :Parameters.PATSClientNumber or (Name like :(String.ISBLANK(Parameters.PATSClientNumber) ? '%' : '~'))) AND
                        (
                                (Client_Name__r.Medicare_Card_Number__c = :Parameters.MedicareCardNumber AND Client_Name__r.Medicare_Sequence_Number__c = :Parameters.MedicareSequenceNumber)
                                or (Name like :(String.ISBLANK(Parameters.MedicareCardNumber) ? '%' : '~'))
                        )
                )
                AND Client_is_Applicant__c = true
                ORDER BY CreatedDate DESC
        ];


        if (SearchResults.size() > 0)
        {
            SelectedClaimId = SearchResults[0].Id;
            Parameters.Salutation = SearchResults[0].Client_Name__r.Salutation;
            Parameters.MedicareCardNumber = SearchResults[0].Client_Name__r.Medicare_Card_Number__c;
            Parameters.MedicareSequenceNumber = SearchResults[0].Client_Name__r.Medicare_Sequence_Number__c;
        }

        if(contacts.size() > 0)
        {
            Parameters.Salutation = contacts[0].Salutation;
            Parameters.MedicareCardNumber = contacts[0].Medicare_Card_Number__c;
            Parameters.MedicareSequenceNumber = contacts[0].Medicare_Sequence_Number__c;
        }

        return null;
    }

    public PageReference Claim()
    {
        User u =
        [
                SELECT Id,
                        ContactId,
                        Profile.Name
                FROM User
                WHERE Id = :UserInfo.getUserId()
        ];

        SearchResults[0].Applicant_Name__c = u.ContactId;
        SearchResults[0].BSB__c = '';
        SearchResults[0].Bank_Account_Name__c = '';
        SearchResults[0].Bank_Account_Number__c = '';

        update SearchResults[0];

        PageReference ref = Page.Claim;
        ref.getParameters().put('ID', SelectedClaimId);
        ref.getParameters().put('retURL', Page.MyApplications.getUrl());
        ref.getParameters().put('saveURL', Page.MyApplications.getUrl());

        return ref.setRedirect(true);
    }


    private class ClaimSearch
    {
        public string Salutation {get; set;}
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string PATSClaimNumber { get; set; }
        public string PATSClientNumber { get; set; }
        public string MedicareCardNumber { get; set; }
        public string MedicareSequenceNumber { get; set; }
        public string SearchVia{get;set;}
    }
}