/**
 * Package Name: classes
 * Project Name: PATS
 * Class Name: SendNotificationsTest
 *
 */
@IsTest
private class SendNotificationsTest {

    @IsTest
    static void testSendEmailNotifications() {
        Payment_Batch__c pb = new Payment_Batch__c(Date_for_Processing__c=Date.today());
        insert pb;

        Payment_Batch_Item__c pbi = new Payment_Batch_Item__c(Payment_Batch__c=pb.id, Payee__c ='Test', Client_Name__c = 'Test', Email__c='test@test.com', Mobile__c='123132123');
        insert pbi;

        update pbi;

        Claim__c c = new Claim__c(Method_to_receive_remittance_advise_via__c = 'Email', Email__c='test@test.com', Mobile_Phone__c = '123132123');
        c.Paper_Copy_of_Section_2_Specialist__c = 'Paper to be posted';
        insert c;

        Claim_Item__c  ci = new Claim_Item__c();
        ci.Claim__c = c.Id;
        ci.Payment_Batch_Item__c = pbi.Id;
        insert ci;

        Test.startTest();

        SendNotificationsBatch batch = new SendNotificationsBatch(pb.Id);
        Database.executeBatch(batch);

        Test.stopTest();
    }

    @IsTest
    static void testSendSmsNotifications() {
        Payment_Batch__c pb = new Payment_Batch__c(Date_for_Processing__c=Date.today());
        insert pb;

        Payment_Batch_Item__c pbi = new Payment_Batch_Item__c(Payment_Batch__c=pb.id, Payee__c ='Test', Client_Name__c = 'Test', Email__c='test@test.com', Mobile__c='123132123');
        insert pbi;

        update pbi;

        Claim__c c = new Claim__c(Method_to_receive_remittance_advise_via__c = 'SMS', Email__c='test@test.com', Mobile_Phone__c = '123132123');
        c.Paper_Copy_of_Section_2_Specialist__c = 'Paper to be posted';
        insert c;

        Claim_Item__c  ci = new Claim_Item__c();
        ci.Claim__c = c.Id;
        ci.Payment_Batch_Item__c = pbi.Id;
        insert ci;

        Test.startTest();

        SendNotificationsBatch batch = new SendNotificationsBatch(pb.Id);
        Database.executeBatch(batch);

        Test.stopTest();
    }
}