/**
 * Created by petermoustrides on 14/06/2016.
 */
@isTest
public with sharing class TestTriggers
{
    public static testMethod void testPaymentBatchItemTrigger()
    {
        Payment_Batch__c pb = new Payment_Batch__c(Date_for_Processing__c=Date.today());
        insert pb;

        Payment_Batch_Item__c pbi = new Payment_Batch_Item__c(Payment_Batch__c=pb.id);
        insert pbi;

        update pbi;

        Attachment a = new Attachment();
        a.Name = 'test';
        a.Body = Blob.valueOf('test');
        a.ContentType = 'text/plain';
        a.ParentId = pbi.ID;
        insert a;
    }

    public static testMethod void test1()
    {
        ccr x = new ccr();
    }
}