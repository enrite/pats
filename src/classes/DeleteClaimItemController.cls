/**
 * Created by Cain on 20/08/2018.
 */

public class DeleteClaimItemController
{
    private ApexPages.StandardController standardController;

    public DeleteClaimItemController(ApexPages.StandardController standardController)
    {
        this.standardController = standardController;
    }

    public PageReference deleteClaimItem()
    {
        Id parentId;
        PageReference returnPage;

        parentId = ApexPages.currentPage().getParameters().get('id');
        Claim_Item__c  record = [SELECT Id, Claim__c FROM Claim_Item__c WHERE Id = :parentId];
        Id ClaimId = record.Claim__c;
        Approval.unlock(record);
        try
        {
            DELETE record;

            String retURL =  ApexPages.currentPage().getParameters().get('retURL');
            if(retURL.contains('id'))
            {
                //From list so return
                returnPage = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
            }
            else
            {
                //from object to return to parent claim
                returnPage = new PageReference('/apex/ClaimViewInternal?id=' + ClaimId);
            }
        }
        catch(Exception e)
        {
            Utilities.addError(e);
            returnPage = null;
        }

        return returnPage;
    }
}