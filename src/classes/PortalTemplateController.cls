/**
 * Created by petermoustrides on 26/04/2016.
 */

public with sharing class PortalTemplateController
{
    transient List<Schema.DescribeTabSetResult> tabSetDesc = Schema.describeTabs();

    public static Map<String, Boolean> getTabs()
    {
        Map<String, Boolean> appTabs  = new Map<String, Boolean> ();

        List<string> pages = new List<string>{'ClientSearch','MyApplications', 'MyDetails', 'Notes'};

        Map<Id, ApexPage> pagesMap = new Map<Id, ApexPage>([SELECT Id, Name, MasterLabel FROM ApexPage WHERE Name in :pages ]);

        for(ApexPage page : pagesMap.values())
        {
            appTabs.put(page.Name, false);
        }

        List<SetupEntityAccess> SEAccesses =
        [
                SELECT Id, SetupEntityId
                FROM SetupEntityAccess
                WHERE Parent.Profile.Id = :UserInfo.getProfileId() AND SetupEntityId in :pagesMap.keySet()
        ];

        for(SetupEntityAccess sea: SEAccesses)
        {
            appTabs.put(pagesMap.get(sea.SetupEntityId).Name, true);
        }


        return appTabs;
    }

    public String userThumbPhoto;
    private static String theUserId = UserInfo.getUserId();

    //getter - the acutal thumbnail photo url from the db
    public String getUserThumbPhoto() {
        //query with the user id
        String thePhoto = [SELECT SmallPhotoUrl FROM User WHERE User.ID = :theUserId LIMIT 1].SmallPhotoUrl;
        //System.debug('The photo url: '+thePhoto);
        return thePhoto;
    }

    private Contact CurrentContact;

    public String CurrentContactAccountName
    {
        get{
            if(CurrentContact==null)
            {
                getCurrentContact();
            }
            return CurrentContact.Account.Name;
        }set;
    }

    private void getCurrentContact()
    {
        CurrentContact = new Contact();
        // should only show contact details if the claim is for the current user
        for (Contact c : [SELECT    Id,
                Name,
                Salutation,
                FirstName,
                Middle_Name__c,
                LastName,
                Account.Name
        FROM     Contact
        WHERE    ID IN (SELECT   ContactId
        FROM        User
        WHERE       ID = :UserInfo.getUserId())])
        {
            CurrentContact = c;
        }
    }
}