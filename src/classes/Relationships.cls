public class Relationships
{
    private List<Relationship> relationships = new List<Relationship>();

    public void resolve()
    {
        // Resolve relationships
        for(Relationship relationship : relationships)
            relationship.Record.put(relationship.RelatedToField, relationship.RelatedTo.Id);
    }

    public void add(SObject record, Schema.sObjectField relatedToField, SObject relatedTo)
    {
        // Relationship to resolve
        Relationship relationship = new Relationship();
        relationship.Record = record;
        relationship.RelatedToField = relatedToField;
        relationship.RelatedTo = relatedTo;
        relationships.add(relationship);
    }

    private class Relationship
    {
        public SObject Record;
        public Schema.sObjectField RelatedToField;
        public SObject RelatedTo;
    }
}