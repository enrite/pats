/**
 * Created by petermoustrides on 2/2/17.
 */
@RestResource(urlMapping='/TreatmentLocalities/*')
global with sharing class TreatmentLocalities
{
    @HttpGet
    global static void doGet()
    {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        String[] searchTerms = req.params.get('term').split(',');

        String searchTerm = searchTerms[0];//req.params.get('term').split(',')[0];
        String postCode = (searchTerms.size() > 1) ? searchTerms[1].trim().toLowerCase() : '';
        String state = (searchTerms.size() > 2) ? searchTerms[2].trim().toLowerCase() : '';

        res.addHeader('Content-Type', 'application/json');
        if(searchTerm.length()>0)
        {
            searchTerm = '%'+searchTerm+'%';
            List<Reference_Data__c> treatmentLocalities = new List<Reference_Data__c>();

            for (Reference_Data__c loc : [SELECT Id,
                                                Name,
                                                State__c,
                                                Postcode__c,
                                                Suburb_Location__c
                                            from Reference_Data__c
                                            where
                                            RecordType.Name = 'Treatment Localities'
                                            and Name like :searchTerm
                                            LIMIT 10]) {

                boolean addToList = true;
                if (String.isNotBlank(state)) {
                    addToList = (loc.State__c.toLowerCase() == state && loc.Postcode__c.toLowerCase() == postCode);
                } else if (String.isNotBlank(postCode) && String.isBlank(state)) {
                    addToList = (loc.Postcode__c.toLowerCase() == postCode);
                }

                if (addToList) {
                    treatmentLocalities.add(loc);
                }
            }


            res.responseBody = Blob.valueOf(JSON.serialize(new SuburbResults(treatmentLocalities)));

        }
        else
        {
            res.responseBody = Blob.valueOf(JSON.serialize(new SuburbResults(new List<Reference_Data__c>())));
        }
    }

    global class SuburbResults
    {
        public List<SuburbResult> Results{get;set;}
        public Integer Total {get;set;}
        public String Status{get;set;}

        public SuburbResults(List<Reference_Data__c> treatmentLocalities)
        {
            Results = new List<SuburbResult>();
            for(Reference_Data__c tl : treatmentLocalities)
            {
                Results.add(new SuburbResult(tl));
            }
            Total = treatmentLocalities.size();
            Status = (Total==0)?'ZERO_RESULTS':'OK';
        }
    }

    global class SuburbResult
    {
        string Value {get;set;}
        string Coordinate {get;set;}
        string Id {get;set;}
        public SuburbResult(Reference_Data__c treatmentLocality)
        {
            Location loc = treatmentLocality.Suburb_Location__c;
            this.Value = treatmentLocality.Name.toUpperCase()+', '+ treatmentLocality.Postcode__c+ ', '+treatmentLocality.State__c.toUpperCase();
            this.Coordinate = loc.longitude+', '+ loc.latitude;
            this.Id = treatmentLocality.Id;
        }
    }
}