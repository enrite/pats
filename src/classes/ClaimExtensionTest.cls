@IsTest
public class ClaimExtensionTest 
{ 
    public static testMethod void test1()
    {
        Account a = new Account(Name = 'PATS');
        insert a;

        Claim__c claim = new Claim__c();

        Test.setCurrentPage(Page.Claim);
        ApexPages.currentPage().getParameters().put('FirstName', 'Test');
        ApexPages.currentPage().getParameters().put('LastName', 'Test');
        ApexPages.currentPage().getParameters().put('MedicareCard', '1234567898');
        ApexPages.currentPage().getParameters().put('MedicareSequenceNumber', '1');

        ClaimExtension ext = new ClaimExtension(new ApexPages.StandardController(claim));

        System.assert(ext.ApplicantProfile == PATSConstants.Profile_ClientApplicant);
        System.assert(ext.ForwardTravelOptions != null);
        System.assert(ext.ReturnTravelOptions != null);
        System.assert(ext.redir() == null);

        claim.Date_submitted__c = Date.today();
        ext = new ClaimExtension(new ApexPages.StandardController(claim));
        System.assert(ext.redir() != null);

        ext.saveOverride();


        System.assert(ext.redirDelegate() == null);
        claim.Doc_Date_Authorised_by_Delegate__c = Date.today();
        ext = new ClaimExtension(new ApexPages.StandardController(claim));
        System.assert(ext.redirDelegate() != null);

        ext.saveDelegate();

        claim.Doc_Authorised__c = true;
        ext.saveDelegate();

        System.assert(ext.redirSpecialist() == null);
        claim.Spec_Date_Authorised_by_Specialist_S__c = Date.today();
        ext = new ClaimExtension(new ApexPages.StandardController(claim));
        System.assert(ext.redirSpecialist() != null);


    }

    public static testMethod void test2()
    {
        Account a = new Account(Name = 'PATS');
        insert a;

        Contact c = new Contact(AccountID = a.ID);
        c.FirstName = 'Test';
        c.LastName = 'Test';
        c.Medicare_Card_Number__c = '1234567898';
        c.Medicare_Sequence_Number__c = '1';
        c.Email = 'test@test.com';
        insert c;
        Claim__c claim = new Claim__c();
        claim.Applicant_Name__c = c.ID;
        claim.Paper_Copy_of_Section_2_Specialist__c = 'Paper to be posted';
        insert claim;

        Claim_Item__c item = new Claim_Item__c(Claim__c = claim.ID);
        insert item;

        ClaimExtension ext = new ClaimExtension(new ApexPages.StandardController(claim));
        claim.Specialist_Treatment_Form__c = 'S2';
        UPDATE claim;
        ext.submitClaim();
        ext.saveSpecialist();

        claim.Specialist_Treatment_Form__c = 'S4 - Block Treatment';
        UPDATE claim;
        ext.saveSpecialist();

        ext.InstanceUrl ='www.salesforce.com';
        String x = ext.InstanceUrl;

        ext.directToUploadInvoice();
        ext.uploadInvoice();
    }
}