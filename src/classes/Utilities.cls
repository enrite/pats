public class Utilities
{
    public static Account DefaultAccount
    {
        get
        {
            if (DefaultAccount == null)
            {
                DefaultAccount =
                [
                        SELECT ID,
                                Name,
                                Owner.TimeZoneSidKey,
                                Owner.LocaleSidKey,
                                Owner.LanguageLocaleKey,
                                Owner.EmailEncodingKey
                        FROM Account
                        WHERE Name = 'PATS'
                ];
            }
            return DefaultAccount;
        }
        set;
    }

    public static User CurrentUser
    {
        get
        {
            if (CurrentUser == null)
            {
                CurrentUser =
                [
                        SELECT ID,
                                Name,
                                ContactId,
                                Contact.Id,
                                Contact.Name,
                                ProfileId,
                                Profile.Name
                        FROM User
                        WHERE ID = :UserInfo.getUserId()
                ];
            }
            return CurrentUser;
        }
        set;
    }

    public static boolean IsApplicant()
    {
        return (CurrentUser.Profile.Name == 'Customer Community - Client / Applicant');
    }

    public static boolean IsThirdPartyApplicant()
    {
        return (CurrentUser.Profile.Name == PATSConstants.Profile_ThirdParty);
    }

    public static boolean IsDelegate()
    {
        return (CurrentUser.Profile.Name == 'Customer Community - Delegate');
    }

    public static boolean IsSpecialist()
    {
        return (CurrentUser.Profile.Name == 'Customer Community - Specialist');
    }

    public static boolean IsPractitioner()
    {
        return IsSpecialist() || IsDelegate();
    }

    public static void validateBSBs(List<Contact> contacts)
    {
        validateBSBs(contacts, Contact.BSB__c);
    }

    public static void validateBSBs(List<Claim__c> claims)
    {
        validateBSBs(claims, Claim__c.BSB__c);
    }

    public static void updateAccountDetails(List <Claim__c> newClaims, Map<Id, Claim__c> oldMap)
    {
        Map<Id, Claim__c> claimsByContactId = new Map<Id, Claim__c>();

        for (Claim__c claim : newClaims)
        {
            if (oldMap.get(claim.Id).Applicant_Name__c != claim.Applicant_Name__c)
            {
                claimsByContactId.put(claim.Applicant_Name__c, claim);
            }
        }

        for (Contact c :
        [
                SELECT Id,
                        Bank_Account_Name__c,
                        BSB__c,
                        Bank_Account_Number__c,
                        Account.Bank_Account_Name__c,
                        Account.BSB__c,
                        Account.Bank_Account_Number__c,
                        RecordType.Name
                from Contact
                where id in :claimsByContactId.keySet()
        ])
        {
            Boolean IsThirdParty = (c.RecordType.Name == PATSConstants.Contact_ThirdParty);

            if (IsThirdParty) {
                claimsByContactId.get(c.Id).Bank_Account_Name__c = c.Account.Bank_Account_Name__c;
                claimsByContactId.get(c.Id).BSB__c = c.Account.BSB__c;
                claimsByContactId.get(c.Id).Bank_Account_Number__c = c.Account.Bank_Account_Number__c;
            }
        }
    }

    private static void validateBSBs(List<sObject> records, Schema.SObjectField BSBField)
    {
        Map<String, List<sObject>> bsbNumbers = new Map<String, List<sObject>>();
        for (sObject c : records)
        {
            if (String.isBlank((String) c.get(BSBField))) continue;
            String bsb = ((String) c.get(BSBField)).replaceAll('[ -]', '');
            bsb = bsb.left(3) + '-' + bsb.right(3);

            c.put(BSBField, bsb);
            if (!bsbNumbers.containsKey(bsb))
            {
                bsbNumbers.put(bsb, new List<sObject>());
            }
            bsbNumbers.get(bsb).add(c);
        }


        Set<String> validBSBs = new Set<String>();
        for (Reference_Data__c r :
        [
                SELECT Id, Name
                from Reference_Data__c
                WHERE RecordType.DeveloperName = 'BSB' AND
                Name in :bsbNumbers.keySet() AND
                Active__c = true
        ])
        {
            validBSBs.add(r.Name);
        }

        for (String bsbNumber : bsbNumbers.keySet())
        {
            if (!validBSBs.contains(bsbNumber))
            {
                for (sObject c : bsbNumbers.get(bsbNumber))
                {
                    String objectName = c.getSobjectType().getDescribe().getName();
                    if (objectName == 'Contact')
                    {
                        ((Contact) c).BSB__c.addError(Label.BSB_Invalid);
                    }
                    else if (objectName == Schema.Claim__c.getSObjectType().getDescribe().getName())
                    {
                        ((Claim__c) c).BSB__c.addError(Label.BSB_Invalid);
                    }
                }
            }
        }
    }


    public static String formatPhoneNumber(String s)
    {

        if (String.isBlank(s))

        {

            return null;

        }

        return s.replaceAll('[() -]', '');

    }

    public static void validateLocations(List<Contact> contacts)
    {
        validateLocations(contacts, Contact.OtherCity, Contact.OtherPostalCode, Contact.MailingCity, Contact.MailingPostalCode);
    }

    public static void validateLocations(List<Claim__c> claims)
    {
        validateLocations(claims, Claim__c.Residential_Suburb__c, Claim__c.Residential_Postcode__c, Claim__c.Mailing_Suburb__c, Claim__c.Mailing_Postcode__c);
    }

    public static void validateClaimItems(Map<Id, Claim__c> newMap, Map<Id, Claim__c> oldMap)
    {
        Map<Id, Claim__c> claimMap = new Map<Id, Claim__c>();
        for (Claim__c c : newMap.values())
        {
            if (c.Forward_Travel_Date__c == oldMap.get(c.id).Forward_Travel_Date__c
                    && c.Return_Travel_Date__c == oldMap.get(c.Id).Return_Travel_Date__c)
                continue;

            claimMap.put(c.id, c);

        }

        List<Claim_Item__c> approvedClaimItems =
        [
                select id, Name, Claim__c, Date_to__c, Date_from__c
                from Claim_Item__c
                where Claim_Item_Status__c != 'Rejected' and RecordType.DeveloperName != 'Accomodation'
                and Claim__c in:claimMap.keySet()
        ];

        for (Claim_Item__c claimItem : approvedClaimItems)
        {
            if (claimMap.get(claimItem.Claim__c).Forward_Travel_Date__c == null)
            {
                claimMap.get(claimItem.Claim__c).Forward_Travel_Date__c.addError('Must not be blank as Claim Item:' + claimItem.Name + ' has been approved.');
            }
            else if (claimItem.Date_from__c < claimMap.get(claimItem.Claim__c).Forward_Travel_Date__c)
            {
                claimMap.get(claimItem.Claim__c).Forward_Travel_Date__c.addError('Claim Item:' + claimItem.Name + ' has a forward travel date before this date.');
            }


            if (claimMap.get(claimItem.Claim__c).Return_Travel_Date__c == null)
            {
                claimMap.get(claimItem.Claim__c).Return_Travel_Date__c.addError('Must not be blank as Claim Item:' + claimItem.Name + ' has been approved.');
            }
            else if (claimItem.Date_to__c > claimMap.get(claimItem.Claim__c).Return_Travel_Date__c)
            {
                claimMap.get(claimItem.Claim__c).Return_Travel_Date__c.addError('Claim Item:' + claimItem.Name + ' has a return travel date after this date.');
            }
        }
    }

    public static void setClaimClients(List<Claim__c> claims) {
        setClaimClients(claims, new Map<Id, Claim__c>());
    }

    public static String getMedicareNumber (String appMedicareNumber)
    {
        String cutMedicareNumber = '';
        if (appMedicareNumber != null && appMedicareNumber.length() > 8)
        {
            cutMedicareNumber = appMedicareNumber.substring(0, 9) + '%';
        }
        return cutMedicareNumber;
    }

    public static void setClaimClients(List<Claim__c> claims, Map<Id, Claim__c> previousValues)
    {
        boolean isInsert = previousValues.isEmpty();
        Map<String, Claim__c> claimMap = new Map<String, Claim__c>();
        Set<Claim__c> existingClient = new Set<Claim__c>();

        for (Claim__c claim : claims)
        {
            String firstName = claim.Client_First_Name__c;
            String lastName = claim.Client_Surname__c;
            String medicareCardNumber = Utilities.formatMedicareNumber(claim.Medicare_Card_Number_Claim__c);
            String medicareSequenceNumber = claim.Medicare_Sequence_Number_Claim__c;
            boolean identifyingDetailsSupplied = !String.isBlank(firstName) && !String.isBlank(lastName) && !String.isBlank(medicareCardNumber) && !String.isBlank(medicareSequenceNumber);

            if (identifyingDetailsSupplied && String.isBlank(claim.Client_Name__c))
            {
                string key = firstName.toLowerCase() + ';' + lastName.toLowerCase() + ';' + medicareCardNumber + ';' + medicareSequenceNumber;
                if (!claimMap.containsKey(key))
                {
                    claimMap.put(key, claim);
                }
            }

            if (identifyingDetailsSupplied && String.isNotBlank(claim.Client_Name__c)) {
                existingClient.add(claim);
            }
        }

        // process the existing records with a client already set.
        Utilities.processExistingClientMedicareDetails(existingClient, previousValues);

        Map<String, Contact> matchedContacts = new Map<String, Contact>();
        Relationships clientNameRelationship = new Relationships();
        for (Contact r :
        [
                SELECT Id,
                        FirstName,
                        Middle_Name__c,
                        LastName,
                        Medicare_Card_Number__c,
                        Medicare_Sequence_Number__c,
                        Salutation,
                        Birthdate,
                        Email,
                        HomePhone,
                        MobilePhone,
                        BSB__c,
                        Bank_Account_Name__c,
                        Bank_Account_Number__c,
                        Concession_Health_Card_Card_Holder__c,
                        Card_Number__c,
                        DVA_Card_Number__c,
                        Department_of_Veteran_Affairs_card__c,
                        OtherStreet,
                        OtherCity,
                        OtherState,
                        OtherCountry,
                        OtherPostalCode,
                        MailingPostalCode,
                        MailingCity,
                        MailingState,
                        MailingCountry,
                        MailingStreet,
                        Method_to_receive_remittance_advise_via__c
                from Contact
                WHERE Concatenated_Key__c in :claimMap.keySet()
        ])
        {
            String firstName = r.FirstName;
            String lastName = r.LastName;
            String medicareCardNumber = Utilities.formatMedicareNumber(r.Medicare_Card_Number__c);
            String medicareSequenceNumber = r.Medicare_Sequence_Number__c;
            matchedContacts.put(firstName.toLowerCase() + ';' + lastName.toLowerCase() + ';' + medicareCardNumber + ';' + medicareSequenceNumber, r);
        }

        List<Contact> contactsToInsert = new List<Contact>();
        for (String key : claimMap.keySet())
        {
            Claim__c claim = claimMap.get(key);
            Contact matchedClient = matchedContacts.containsKey(key) ? matchedContacts.get(key) : null;
            if (matchedClient == null)
            {
                matchedClient = new Contact();
                matchedClient.Salutation = claim.Salutation__c;
                matchedClient.FirstName = claim.Client_First_Name__c;
                matchedClient.Middle_Name__c = claim.Client_Middle_Name__c;
                matchedClient.LastName = claim.Client_Surname__c;
                matchedClient.Medicare_Card_Number__c = Utilities.formatMedicareNumber(claim.Medicare_Card_Number_Claim__c);
                matchedClient.Medicare_Sequence_Number__c = claim.Medicare_Sequence_Number_Claim__c;
                matchedClient.AccountId = Utilities.DefaultAccount.Id;
                matchedClient.Birthdate = claim.Client_date_of_birth__c;
                matchedClient.Email = claim.Email__c;
                matchedClient.HomePhone = claim.Home_Phone__c;
                matchedClient.MobilePhone = claim.Mobile_Phone__c;
                matchedClient.BSB__c = claim.BSB__c;
                matchedClient.Bank_Account_Name__c = claim.Bank_Account_Name__c;
                matchedClient.Bank_Account_Number__c = claim.Bank_Account_Number__c;
                matchedClient.Concession_Health_Card_Card_Holder__c = claim.Concession_Health_Care_Card_Holder__c;
                matchedClient.Card_Number__c = claim.Pensioner_or_Health_Care_Card_Number__c;
                matchedClient.DVA_Card_Number__c = claim.DVA_Card__c;
                matchedClient.Department_of_Veteran_Affairs_card__c = claim.DVA_Card_Holder__c;
                matchedClient.OtherStreet = claim.Residential_Street__c;
                matchedClient.OtherCity = claim.Residential_Suburb__c;
                matchedClient.OtherState = claim.Residential_State__c;
                matchedClient.OtherPostalCode = claim.Residential_Postcode__c;
                matchedClient.OtherCountry = claim.Residential_Country__c;
                matchedClient.MailingStreet = claim.Mailing_Street__c;
                matchedClient.MailingCity = claim.Mailing_Suburb__c;
                matchedClient.MailingState = claim.Mailing_State__c;
                matchedClient.MailingPostalCode = claim.Mailing_Postcode__c;
                matchedClient.MailingCountry = claim.Mailing_Country__c;
                matchedClient.Method_to_receive_remittance_advise_via__c = claim.Method_to_receive_remittance_advise_via__c;
                contactsToInsert.add(matchedClient);
                clientNameRelationship.add(claim, Claim__c.Client_Name__c.getDescribe().getSObjectField(), matchedClient);
                if (claim.Applicant_Name__c == null)
                {
                    clientNameRelationship.add(claim, Claim__c.Applicant_Name__c.getDescribe().getSObjectField(), matchedClient);
                }
            }
            else
            {
                claim.Client_Name__c = matchedClient.Id;
                if (claim.Applicant_Name__c == null)
                {
                    claim.Applicant_Name__c = matchedClient.Id;
                }
            }
        }

        Database.SaveResult [] insertResults = Database.insert(contactsToInsert, false);
        for (Integer i = 0; i < insertResults.size(); i++)
        {
            Database.SaveResult ir = InsertResults[i];
            Contact c = contactsToInsert[i];
            String firstName = c.FirstName;
            String lastName = c.LastName;
            String medicareCardNumber = c.Medicare_Card_Number__c;
            String medicareSequenceNumber = c.Medicare_Sequence_Number__c;
            Claim__c claim = claimMap.get(firstName.toLowerCase() + ';' + lastName.toLowerCase() + ';' + medicareCardNumber + ';' + medicareSequenceNumber);

            if (!ir.isSuccess())
            {
                for (Database.Error e : ir.getErrors())
                {
                    claim.addError(e.getMessage());
                }
            }
        }
        clientNameRelationship.resolve();
    }

    private static void processExistingClientMedicareDetails(Set<Claim__c> claims, Map<Id, Claim__c> previousValues) {
        Set<Id> contactIds = new Set<Id>();

        for (Claim__c cl : claims) {
            contactIds.add(cl.Client_Name__c);
        }

        Map<Id, Contact> contacts = new Map<Id, Contact>([SELECT ID, Medicare_Card_Number__c, Medicare_Sequence_Number__c FROM Contact WHERE ID IN :contactIds]);
        List<Contact> contactsToUpdate = new List<Contact>();
        for (Claim__c cl : claims) {
            Contact c = contacts.containsKey(cl.Client_Name__c) ? contacts.get(cl.Client_Name__c) : null;
            if (c == null) continue;

            if (String.isBlank(c.Medicare_Sequence_Number__c) && String.isBlank(c.Medicare_Card_Number__c)) {
                c.Medicare_Card_Number__c = cl.Medicare_Card_Number_Claim__c;
                c.Medicare_Sequence_Number__c = cl.Medicare_Sequence_Number_Claim__c;
                contactsToUpdate.add(c);
            } else {
                if (Utilities.firstNineMedicareNumber(c.Medicare_Card_Number__c) != Utilities.firstNineMedicareNumber(cl.Medicare_Card_Number_Claim__c)){
                    // removed from the if: || c.Medicare_Sequence_Number__c != cl.Medicare_Sequence_Number_Claim__c)
                    cl.addError('The supplied medicare number differs from the one stored on the client, please update the client record first.');
                }
            }
        }

        if (contactsToUpdate.size() > 0)
            update contactsToUpdate;
    }

    public static void validateLocations(List<sObject> records, Schema.SObjectField residentialCityField, Schema.SObjectField residentialPostalCodeField, Schema.SObjectField mailingCityField, Schema.SObjectField mailingPostalCodeField)
    {
        Map<String, Set<SObject>> suburbPostcodes = new Map<String, Set<SObject>>();
        for (SObject c : records)
        {
            string otherCity = (String) c.get(residentialCityField);
            string otherPostalCode = (String) c.get(residentialPostalCodeField);
            string key;
            if (!String.isBlank(otherCity) && !String.isBlank(otherPostalcode))
            {
                key = otherCity.toLowerCase() + ';' + otherPostalcode;
                if (!suburbPostcodes.containsKey(key))
                {
                    suburbPostcodes.put(key, new Set<SObject>());
                }
                suburbPostcodes.get(key).add(c);
            }

            string mailingCity = (String) c.get(mailingCityField);
            string mailingPostalcode = (String) c.get(mailingPostalCodeField);
            if (String.isBlank(mailingCity) || String.isBlank(mailingPostalcode)) continue;
            key = mailingCity.toLowerCase() + ';' + mailingPostalcode;
            if (!suburbPostcodes.containsKey(key))
            {
                suburbPostcodes.put(key, new Set<SObject>());
            }

            if (!suburbPostcodes.get(key).contains(c))
            {
                suburbPostcodes.get(key).add(c);
            }
        }

        Set<String> validSuburbPostcodes = new Set<String>();
        for (Reference_Data__c r :
        [
                SELECT Id, Name, Postcode__c
                from Reference_Data__c
                WHERE RecordType.DeveloperName = 'SuburbPostcode' AND
                SuburbPostcodeKey__c in :suburbPostcodes.keySet() AND
                Active__c = true
        ])
        {
            validSuburbPostcodes.add(r.Name.toLowerCase() + ';' + r.Postcode__c);
        }

        for (String suburbPostcode : suburbPostcodes.keySet())
        {
            if (!validSuburbPostcodes.contains(suburbPostcode))
            {
                for (SObject c : suburbPostcodes.get(suburbPostcode))
                {
                    String[] s = suburbPostcode.split(';');
                    string addressError = Label.Address_Invalid.replace('{locality}', s[0].toUpperCase()).replace('{postcode}', s[1]);
                    if (c instanceof Contact)
                    {
                        Contact contact = (contact) c;
                        if (contact.MailingCity != null && contact.MailingCity.toLowerCase() == s[0] && contact.MailingPostalCode == s[1])
                        {
                            contact.MailingCity.addError(addressError);
                            contact.MailingPostalCode.addError(addressError);
                        }
                        else
                        {
                            contact.OtherCity.addError(addressError);
                            contact.OtherPostalcode.addError(addressError);
                        }
                    }
                    else if (c instanceof Claim__c)
                    {
                        Claim__c claim = (Claim__c) c;
                        if (claim.Residential_Suburb__c.toLowerCase() == s[0] && claim.Residential_Postcode__c == s[1])
                        {
                            claim.Residential_Suburb__c.addError(addressError);
                            claim.Residential_Postcode__c.addError(addressError);
                        }
                        else
                        {
                            claim.Mailing_Suburb__c.addError(addressError);
                            claim.Mailing_Postcode__c.addError(addressError);
                        }
                    }
                }
            }
        }
    }

    public static String getReferenceData(String name, String developerName)
    {
        List<Reference_Data__c> ref = [
                SELECT Name, Notes__c
                FROM Reference_Data__c
                WHERE Active__c = true
                AND RecordType.DeveloperName = :developerName
                AND Name = :name
                LIMIT 1
        ];
        return ref.isEmpty()? null : ref.get(0).Notes__c;
    }

    public static Reference_Data__c getBSB(String bsb)
    {
        if (String.isBlank(bsb))
        {
            return null;
        }
        bsb = bsb.replaceAll('[ -]', '');
        // if bsb is not 6 characters = no problem as should fail anyway
        bsb = bsb.left(3) + '-' + bsb.right(3);
        for (Reference_Data__c rd :
        [
                SELECT ID, Name
                FROM Reference_Data__c
                WHERE RecordType.DeveloperName = 'BSB' AND
                Name = :bsb AND
                Active__c = true
        ])
        {
            return rd;
        }
        return null;
    }

    public static Reference_Data__c getLocation(String loc, String postcode)
    {
        if (String.isBlank(loc) || String.isBlank(postcode))
        {
            return null;
        }
        for (Reference_Data__c rd :
        [
                SELECT ID,
                        Name
                FROM Reference_Data__c
                WHERE RecordType.DeveloperName = 'SuburbPostcode' AND
                Name = :loc AND
                Postcode__c = :postcode AND
                Active__c = true
        ])
        {
            return rd;
        }
        return null;
    }

    public static void syncClaimViews(List<Claim__c> claims, Boolean isDelete)
    {
        if (isDelete)
        {
            delete
            [
                    SELECT ID
                    FROM Claim_View__c
                    WHERE Claim_View__c IN :claims
            ];
        }
        else
        {
            List<Claim_View__c> newClaimViews = new List<Claim_View__c>();
            for (Claim__c c : claims)
            {
                newClaimViews.add(new Claim_View__c(Claim_View__c = c.Id, Client_View__c = c.Client_Name__c));
            }
            insert newClaimViews;
        }
    }

    public static List<SelectOption> getOptionForPicklist(Schema.SObjectField f, Boolean addNullOption)
    {
        List<SelectOption> opts = new List<SelectOption>();
        if (addNullOption)
        {
            opts.add(new SelectOption('', ' '));
        }
        for (Schema.PicklistEntry ple : f.getDescribe().getPickListValues())
        {
            opts.add(new SelectOption(ple.getValue(), ple.getLabel()));
        }
        return opts;
    }

    public static void checkRequiredField(sObject record, Schema.SObjectField field)
    {
        if (record.get(field) == null)
        {
            addRequiredFieldError(field);
        }
    }

    public static PageReference addError(Exception ex)
    {
        String msg = ex.getMessage();
        if (ex instanceof DMLException)
        {
            msg = ((DMLException) ex).getDMLMessage(0);
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
        return null;
    }

    public static PageReference addError(Exception ex, Savepoint sp)
    {
        if (sp != null)
        {
            Database.rollback(sp);
        }
        return addError(ex);
    }

    public static PageReference addError(String msg)
    {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
        return null;
    }

    public static PageReference addRequiredFieldError(Schema.SObjectField field)
    {
        return addRequiredFieldError(field, Label.All_IsRequired);
    }

    public static PageReference addRequiredFieldError(Schema.SObjectField field, String msg)
    {
        return addError(field.getDescribe().getLabel() + ' ' + msg);
    }

    public static PageReference addRequiredFieldError(Schema.SObjectField field1, Schema.SObjectField field2, String msg)
    {
        return addError(field1.getDescribe().getLabel() + ' ' + msg + ' ' + field2.getDescribe().getLabel());
    }

    public static PageReference addMessage(String msg)
    {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, msg));
        return null;
    }

    public static String getParameter(String parameter)
    {
        if (ApexPages.currentPage() == null)
        {
            return null;
        }
        return ApexPages.currentPage().getParameters().get(parameter);
    }

    public static String formatMedicareNumber(String s)
    {
        if (String.isBlank(s))
        {
            return null;
        }
        s = s.deleteWhitespace();
        if (s.length() != 10)
        {
            return s;
        }
        return s.mid(0, 4) + ' ' + s.mid(4, 5) + ' ' + s.mid(9, 1);
    }

    public static String firstNineMedicareNumber(String s)
    {
        if (String.isBlank(s))
        {
            return '';
        }
        s = s.deleteWhitespace();
        if (s.length() < 9)
        {
            return s;
        }
        return s.substring(0, 9);
    }

    public static String formatBankAccountNumber(String s)
    {
        if (String.isBlank(s))
        {
            return null;
        }
        s = s.replace('_', '');
        return s.leftPad(9, '0');
    }

    public static String getFieldQueryString(sObjectType objType)
    {
        String qry = 'SELECT ';
        Schema.DescribesObjectResult describeResult = objType.getDescribe();
        Map<String, Schema.SObjectField> fieldsByName = describeResult.fields.getMap();
        for (String fieldName : fieldsByName.keySet())
        {
            qry += fieldName + ',';
        }
        return qry.removeEnd(',');
    }

    public static String getRecordQueryString(sObjectType objType, ID recordId)
    {
        return getFieldQueryString(objType) + ' FROM ' + objType.getDescribe().getName() + ' WHERE ID=\'' + String.escapeSingleQuotes(recordId) + '\'';
    }

    public static Map<String, RecordTypeInfo> getRecordTypeInfos(sObjectType sobjType)
    {
        return sobjType.getDescribe().getRecordTypeInfosByName();
    }

    public static RecordTypeInfo getRecordTypeInfo(sObjectType sobjType, String recordTypeName)
    {
        return getRecordTypeInfos(sobjType).get(recordTypeName);
    }

    public static ID getRecordTypeId(sObjectType sobjType, String recordTypeName)
    {
        return getRecordTypeInfo(sobjType, recordTypeName).getRecordTypeID();
    }

    @future
    public static void updateUserEmail(Id contactId)
    {
        Contact c = [SELECT Id, Email from contact where id =: contactid];

        List<User> users = [SELECT Id, Email, ContactId from User
                          where ContactId =: contactId];

        if(users.size()>0)
        {
            users[0].Email = c.Email;
            update users[0];
        }
    }
}