/**
 * Package Name: classes
 * Project Name: PATS
 * Class Name: SendNotificationsExtension
 *
 */

public with sharing class SendNotificationsExtension {

    public Payment_Batch__c PaymentBatch {get;set;}

    public SendNotificationsExtension(ApexPages.StandardController ctr) {
        if (!Test.isRunningTest()) ctr.addFields(new List<String> { 'Name' });
        PaymentBatch = (Payment_Batch__c)ctr.getRecord();
    }

    public PageReference runBatch() {
        SendNotificationsBatch batch = new SendNotificationsBatch(PaymentBatch.Id);
        // limit the notifications to 250 per batch this will ensure that we do not exceed the email limits
        Database.executeBatch(batch, 250);
        return new PageReference('/' + PaymentBatch.Id);
    }
}