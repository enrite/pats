public class PATSConstants 
{ 
    public final static String Profile_ClientApplicant = 'Customer Community - Client / Applicant';
    public final static String Profile_Delegate = 'Customer Community - Delegate';
    public final static String Profile_Specialist = 'Customer Community - Specialist';
    public final static String Profile_ThirdParty = 'Customer Community - Third Party';

    public final static String Contact_ThirdParty = 'Third Party Applicant';

    public final static String Email_ClientApplicantWelcome = 'ClientApplicant_New_Member_Welcome_Email';
    public final static String Email_DelegateWelcome = 'DelegateWelcomeEmailTemplate';
    public final static String Email_SpecialistWelcome = 'SpecialistWelcomeEmailTemplate';
    public final static String Email_SpecialisDelegateWelcome = 'SpecialistDelegateWelcomeEmailTemplate';
    public final static String Email_SubmittedConfirmation = 'Claim_Submitted';

    public final static String REF_DATA_ReferringDelegateAuthorisationInfo = 'Referring Delegate Authorisation Info';
    public final static String REF_DATA_RecTypeClaim = 'Claim_Information';
}