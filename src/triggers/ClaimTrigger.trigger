trigger ClaimTrigger on Claim__c (before insert, after insert, before update, after update, before delete, after delete)  
{
    if(Trigger.isBefore)
    {
        if(Trigger.isDelete)
        {
            Utilities.syncClaimViews(Trigger.old, true);
        }
        if(Trigger.isInsert || Trigger.isUpdate)
        {
            if(Trigger.isUpdate) {
                Utilities.updateAccountDetails(Trigger.new, Trigger.oldMap);
            }
            Utilities.validateBSBs(Trigger.new);
            Utilities.validateLocations(Trigger.new);
            Utilities.setClaimClients(Trigger.new);

            Map<String, RecordTypeInfo> recTypesByName = Schema.sObjectType.Claim__c.getRecordTypeInfosByName();
            for(Claim__c claim : Trigger.new)
            {
                claim.Home_Phone__c = Utilities.formatPhoneNumber(claim.Home_Phone__c);
                claim.Mobile_Phone__c = Utilities.formatPhoneNumber(claim.Mobile_Phone__c);

                claim.Medicare_Card_Number_Claim__c = Utilities.formatMedicareNumber(claim.Medicare_Card_Number_Claim__c);
                claim.Bank_Account_Number__c = Utilities.formatBankAccountNumber(claim.Bank_Account_Number__c);
                if(claim.Status__c == 'Created')
                {
                    claim.Date_submitted__c = null;
                    if(claim.RecordTypeId == recTypesByName.get('Submitted').getRecordTypeId())
                    {
                        // may be changed again below, depending on delegate/specialist authorisation
                        claim.RecordTypeId = recTypesByName.get('Created').getRecordTypeId();
                    }
                }
                if(claim.Spec_Authorised__c)
                {
                    if(claim.Doc_Authorised__c)
                    {
                        claim.RecordTypeId = recTypesByName.get('RDA & SA').getRecordTypeId();
                    }
                    else
                    {
                        claim.RecordTypeId = recTypesByName.get('SA').getRecordTypeId();
                    }
                }
                else if(claim.Doc_Authorised__c)
                {
                    claim.RecordTypeId = recTypesByName.get('RDA').getRecordTypeId();
                }
            }

            if ( UserInfo.getUserType() != 'CspLitePortal' && Trigger.new.size() == 1) {
                ClaimDistanceUtil.setEligibilityStatus((Claim__c) Trigger.new.get(0));
            }
            
        }
    }
    if(Trigger.isAfter)
    {
        if(Trigger.isInsert)
        {
            Utilities.syncClaimViews(Trigger.new, false);
        }

        if(Trigger.isUpdate)
        {
            Utilities.validateClaimItems(Trigger.newMap, Trigger.oldMap);
        }


        // only can this if we have a single record and the action has been perfored by an internal user.
        if ((Trigger.isInsert || Trigger.isUpdate) && UserInfo.getUserType() != 'CspLitePortal' && Trigger.new.size() == 1) {
            ClaimDistanceUtil.calculateClaimableDistance((Claim__c) Trigger.new.get(0));
        }
    }
}