/**
 * Created by petermoustrides on 19/05/2016.
 */
trigger PaymentBatchItem on Payment_Batch_Item__c (before insert, before update, before delete, after insert, after update, after delete)
{
    system.debug(Trigger.isDelete);
    system.debug(Trigger.isInsert);
    system.debug(Trigger.isUpdate);
    system.debug(Trigger.oldMap);
    system.debug(Trigger.newMap);
    List<Payment_Batch_Item__c> paymentBatchItems = Trigger.isDelete ? Trigger.old : Trigger.new;
    List<Batch_Item_Claim_Item__c> claimItems = new List<Batch_Item_Claim_Item__c>();
    Map<ID, Payment_Batch_Item__c> batchItemsById = new Map<ID, Payment_Batch_Item__c>();

    if (Trigger.isAfter && Trigger.isUpdate) {
       claimItems = [
                Select Payment_Batch_Item__c,
                        Claim_Item__c,
                        Claim_Item__r.Payment_Batch_Item__c,
                        Claim_Item__r.Name,
                        Claim_Item__r.Payment_Batch_Item__r.Name
                from Batch_Item_Claim_Item__c
                where Payment_Batch_Item__c in :paymentBatchItems
        ];
    }
    if(Trigger.isBefore && Trigger.isUpdate) {
        batchItemsById = new Map<ID, Payment_Batch_Item__c>(
        [
                SELECT ID,
                        (SELECT Total_Approved_to_Pay__c,
                                Payee_Invoice_Number__c,
                                Total_Travel_Payable__c,
                                Claim__r.Name,
                                Claim__r.Applicant_Name__r.Name,
                                Claim__r.Client_Name__r.Name,
                                Claim__r.Specialist_Appointment_Date__c,
                                Claim__r.Client_Name__r.PATS_Client_Number__c
                        FROM Claim_Items__r)
                FROM Payment_Batch_Item__c
                WHERE ID IN :Trigger.new
        ]);
    }

    Map<Id, List<Batch_Item_Claim_Item__c>> claimItemMap = new Map<Id, List<Batch_Item_Claim_Item__c>>();
    for (Batch_Item_Claim_Item__c claimItem : claimItems)
    {
        if (!claimItemMap.containsKey(claimItem.Payment_Batch_Item__c))
        {
            claimItemMap.put(claimItem.Payment_Batch_Item__c, new List<Batch_Item_Claim_Item__c>());
        }
        claimItemMap.get(claimItem.Payment_Batch_Item__c).add(claimItem);
    }

    List<Claim_Item__c> claimItemsToUpdate = new List<Claim_Item__c>();
    for (Payment_Batch_Item__c paymentBatchItem : paymentBatchItems)
    {
        if (Trigger.isBefore && Trigger.isUpdate)
        {
            paymentBatchItem.Claim_Item_Approved_Total__c = 0;
            Set<String> claimNumbers = new Set<String>();
            Set<String> payees = new Set<String>();
            Set<String> pATSClientNumbers = new Set<String>();
            Set<String> appointmentDates = new Set<String>();
            Set<String> clientName = new Set<String>();
            Set<String> invoiceNumber = new Set<String>();
            Decimal travelTotal = 0;

            Payment_Batch_Item__c pbi = batchItemsById.get(paymentBatchItem.ID);
            if(pbi.Claim_Items__r != null)
            {
                for(Claim_Item__c ci : pbi.Claim_Items__r)
                {
                    paymentBatchItem.Claim_Item_Approved_Total__c += (ci.Total_Approved_to_Pay__c == null ? 0 : ci.Total_Approved_to_Pay__c);
                    claimNumbers.add(ci.Claim__r.Name);
                    payees.add(ci.Claim__r.Applicant_Name__r.Name);
                    pATSClientNumbers.add(ci.Claim__r.Client_Name__r.PATS_Client_Number__c);
                    if(ci.Claim__r.Specialist_Appointment_Date__c != null)
                    {
                        String d = ci.Claim__r.Specialist_Appointment_Date__c.day() + '/' + ci.Claim__r.Specialist_Appointment_Date__c.month() + '/' + ci.Claim__r.Specialist_Appointment_Date__c.year();
                        appointmentDates.add(d);
                    }

                    clientName.add(ci.Claim__r.Client_Name__r.Name);
                    invoiceNumber.add(ci.Payee_Invoice_Number__c);
                    if(ci.Total_Travel_Payable__c != null && ci.Total_Travel_Payable__c != 0)
                    {
                        travelTotal += ci.Total_Travel_Payable__c;
                    }

                }
            }
            paymentBatchItem.Claim_Numbers__c = String.join(new List<String>(claimNumbers), ';');
            paymentBatchItem.Payee__c = String.join(new List<String>(payees), ';');
            paymentBatchItem.PATS_Client_Number__c = String.join(new List<String>(pATSClientNumbers), ';');
            paymentBatchItem.Appointment_Date__c = String.join(new List<String>(appointmentDates), ';');
            paymentBatchItem.Client_Name__c = String.join(new List<String>(clientName), ';');
            paymentBatchItem.Invoice_Number__c = String.join(new List<String>(invoiceNumber), ';');
            paymentBatchItem.Total_Travel_Payable__c = travelTotal;
        }
        if (Trigger.isAfter && Trigger.isUpdate)
        {
            if(paymentBatchItem.Failed__c != Trigger.oldMap.get(paymentBatchItem.Id).Failed__c) {
                if(claimItemMap.containsKey(paymentBatchItem.Id)) {
                    string errorString = '';
                    for(Batch_Item_Claim_Item__c c: claimItemMap.get(paymentBatchItem.Id)) {
                        if(c.Claim_Item__r.Payment_Batch_Item__c != paymentBatchItem.id && c.Claim_Item__r.Payment_Batch_Item__c != null) {
                            errorString += string.format('{0} has succesfully been processed by Payment Batch Item {1}<br/>', new List<String>{
                                    c.Claim_Item__r.Name, c.Claim_Item__r.Payment_Batch_Item__r.Name
                            });
                            continue;
                        }

                        Claim_Item__c ciToUpdate = new Claim_Item__c(id = c.Claim_Item__c, Payment_Batch_Item__c = (paymentBatchItem.Failed__c ? null : paymentBatchItem.id));
                        claimItemsToUpdate.add(ciToUpdate);
                        //Approval.Lock(c.Claim_Item__c);
                    }

                    if(errorString.length() > 0) {
                        if(!paymentBatchItem.Failed__c)
                            paymentBatchItem.Failed_Reason__c.addError(errorString, false);
                    }
                }
            }
        }
    }
    if(!claimItemsToUpdate.isEmpty()) {
        update claimItemsToUpdate;
    }
}