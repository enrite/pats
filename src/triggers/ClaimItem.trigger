trigger ClaimItem on Claim_Item__c (before insert, before update, before delete, after insert, after update, after delete, after undelete)
{
    if(Trigger.isBefore)
    {
        if(Trigger.isDelete)
        {
            system.debug('in del');
            for(Claim_Item__c ci : Trigger.old)
            {
               if(ci.Claim_Item_Status__c == 'Approved' && !String.IsBlank(ci.Payment_Batch_Item__c) )
               {
                   ci.addError('Approved claim items cannot be deleted');
               }
            }
        }
    }
    if(Trigger.isAfter)
    {
        Set<ID> batchItemIds = new Set<ID>();
        for(Claim_Item__c ci : (Trigger.isDelete ? Trigger.old : Trigger.new))
        {
            batchItemIds.add(ci.Payment_Batch_Item__c);
            if(Trigger.isUpdate)
            {
                //also get previous batch item if changed
                ID batchItemID = Trigger.oldMap.get(ci.ID).Payment_Batch_Item__c;
                batchItemIds.add(batchItemID);
            }

        }
        batchItemIds.remove(null);
        if(!batchItemIds.isEmpty())
        {
            List<Payment_Batch_Item__c> batchItems = new List<Payment_Batch_Item__c>();
            for(Id batchId : batchItemIds)
            {
                batchItems.add(new Payment_Batch_Item__c(ID = batchId));
            }
            update batchItems;
        }
    }
}