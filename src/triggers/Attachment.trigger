trigger Attachment on Attachment (before insert, before update, before delete, after insert, after update, after delete)
{
    Set<String> allowedTypes = new Set<String>{'text/plain','text/csv','application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/vnd.ms-excel','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/vnd.ms-powerpoint','application/vnd.openxmlformats-officedocument.presentationml.presentation','message/rfc822','application/rtf','image/jpeg','image/png','image/gif','image/bmp'};
    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate))
    {
        for (Attachment a : Trigger.new)
        {
            System.Debug(a.Name);
            System.Debug(a.ContentType);
            Boolean validFile = false;
            validFile = a.ParentId.getSobjectType() == Payment_Batch__c.SObjectType && (a.Name.endsWith('.REM') || a.Name.endsWith('.eft'));
            if (!validFile)
            {
                validFile = allowedTypes.contains(a.ContentType) || a.Name.endsWith('.msg');
            }
            if (!validFile)
            {
                a.addError('File type not allowed');
            }
        }
    }
}