trigger Contact on Contact (before insert, after insert, before update, after update, before delete, after delete)
{
    List<Contact> contacts = Trigger.isDelete ? Trigger.old : Trigger.new;
    Map<String, RecordTypeInfo> recTypesByName = Schema.sObjectType.Contact.getRecordTypeInfosByName();
    Map<String, set<String>> medicareNumbers = new Map<String, Set<String>>();
    Set<String> transactionMedicareNumbers = new Set<String>();
    Map<Id, User> userMap = new Map<Id, User>();
    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate))
    {
        Utilities.validateBSBs(Trigger.new);
        Utilities.validateLocations(Trigger.new);

        if (Trigger.isUpdate) {
            for (User usr : [SELECT Id, ContactId FROM User WHERE ContactId IN :Trigger.newMap.keySet()]) {
                userMap.put(usr.ContactId, usr);
            }
        }
    }

    for (Contact c : contacts)
    {
        if(Trigger.isBefore)    
        {
            if (!c.Automatically_create_user__c && Trigger.isUpdate) {
                c.Automatically_create_user__c = userMap.containsKey(c.Id);
            }

            if (Trigger.isInsert || (Trigger.isUpdate && Trigger.oldMap.get(c.Id).Medicare_Card_Number__c != c.Medicare_Card_Number__c))
            {
                c.Medicare_Card_Number__c = Utilities.formatMedicareNumber(c.Medicare_Card_Number__c);
            }
            if (Trigger.isInsert || (Trigger.isUpdate && Trigger.oldMap.get(c.Id).Bank_Account_Number__c != c.Bank_Account_Number__c))
            {
                c.Bank_Account_Number__c = Utilities.formatBankAccountNumber(c.Bank_Account_Number__c);
            }

        }
        if(c.Medicare_Card_Number__c != null && c.Medicare_Card_Number__c != '')
        {
            transactionMedicareNumbers.add(Utilities.firstNineMedicareNumber('%' + c.Medicare_Card_Number__c) + '%');
        }
    }
    if (Trigger.isInsert || Trigger.isUpdate)
    {
        for (Contact c :
        [
                SELECT ID, Medicare_Card_Number__c, Medicare_Sequence_Number__c, Medicare_Number_Trimmed__c
                FROM Contact
                WHERE RecordType.DeveloperName = 'Applicant_Client' AND
                Medicare_Number_Trimmed__c LIKE :transactionMedicareNumbers AND
                Medicare_Number_Trimmed__c != null
        ])
        {
            system.debug('the contacts medicare is: ' + c.Medicare_Number_Trimmed__c);
            if (!medicareNumbers.containsKey(Utilities.firstNineMedicareNumber(c.Medicare_Card_Number__c)))
            {
                medicareNumbers.put(Utilities.firstNineMedicareNumber(c.Medicare_Card_Number__c), new set<String>());
            }

            if (!medicareNumbers.get(Utilities.firstNineMedicareNumber(c.Medicare_Card_Number__c)).contains(c.Medicare_Sequence_Number__c))
                medicareNumbers.get(Utilities.firstNineMedicareNumber(c.Medicare_Card_Number__c)).add(c.Medicare_Sequence_Number__c);
        }
    }

    for (Contact c : contacts)
    {
        if (Trigger.isBefore)
        {
            if (!Trigger.isDelete && UserInfo.getUserType() == 'Guest')
            {
                if (c.AccountID == null)
                {
                    c.AccountID = Utilities.DefaultAccount.ID;  
                }
                if (c.Application_For_Access_As__c == 'Referring Delegate')
                {
                    c.RecordTypeId = recTypesByName.get('Referring Delegate').getRecordTypeId();
                }
                else if (c.Application_For_Access_As__c == 'Specialist')
                {
                    c.RecordTypeId = recTypesByName.get('Specialist').getRecordTypeId();
                }
            }

            if ((Trigger.isInsert || Trigger.isUpdate) && c.RecordTypeId == recTypesByName.get('Applicant / Client').getRecordTypeId())
            {
                if (Trigger.isUpdate && (Utilities.firstNineMedicareNumber(Trigger.oldMap.get(c.Id).Medicare_Card_Number__c) == Utilities.firstNineMedicareNumber(c.Medicare_Card_Number__c)))
                    continue;
                String medicareWithoutSpaces = c.Medicare_Card_Number__c == null ? '' : c.Medicare_Card_Number__c.deleteWhitespace();
                if (medicareNumbers.containsKey(Utilities.firstNineMedicareNumber(c.Medicare_Card_Number__c)) && medicareWithoutSpaces.length() > 8)
                {
                    if (medicareNumbers.get(Utilities.firstNineMedicareNumber(c.Medicare_Card_Number__c)).contains(c.Medicare_Sequence_Number__c)) {
                        c.Medicare_Card_Number__c.addError(Label.Medicare_Not_Unique);
                        //c.Medicare_Sequence_Number__c.addError(Label.Medicare_Not_Unique);
                    }
                }
            }
        }


        if (Trigger.isAfter && Trigger.isUpdate)
        {
            if (Trigger.oldMap.get(c.Id).Status__c != 'Approved' && c.Status__c == 'Approved')
            {
                UserManagement.createCommunityUser(c.Id);
            }
        }
    }

    if (Trigger.isAfter && Trigger.isUpdate && Trigger.new.size()==1)
    {
        Contact c = Trigger.new[0];
        if(c.Email != Trigger.oldMap.get(c.id).Email)
        {
            Utilities.updateUserEmail(c.Id);
        }
    }

    if(Trigger.isAfter && (Trigger.isInsert ||
                            Trigger.isUpdate))
    {
        Set<ID> contactIds = new Set<ID>();
        for(Contact c : Trigger.new)
        {
            if(c.Automatically_Create_User__c &&
                    (Trigger.isInsert ||
                            (Trigger.isUpdate &&
                                    !Trigger.oldMap.get(c.ID).Automatically_Create_User__c)))
            {
                contactIds.add(c.ID);
            }
        }
        if(!contactIds.isEmpty())
        {
            UserManagement.createCommunityUsers(contactIds);
        }
    }
}