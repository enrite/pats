<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Claim_Has_been_Pre_Approved</fullName>
        <description>Claim Has been Pre Approved</description>
        <protected>false</protected>
        <recipients>
            <field>Applicant_Name__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PATS_Portal/Claim_Pre_Approved</template>
    </alerts>
    <fieldUpdates>
        <fullName>Claims_Ready_for_Assessment</fullName>
        <field>OwnerId</field>
        <lookupValue>Assessor</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Claims Ready for Assessment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Client_Authorised</fullName>
        <field>Client_Authorised__c</field>
        <literalValue>0</literalValue>
        <name>Clear Client Authorised</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Client_Certification</fullName>
        <field>Client_Certification_Date__c</field>
        <name>Clear Client Certification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Date_Submitted</fullName>
        <field>Date_submitted__c</field>
        <name>Clear Date Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Claim_Status_Created</fullName>
        <field>Status__c</field>
        <literalValue>Created (Not Yet Submitted to PATS)</literalValue>
        <name>Set Claim Status Created</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RecordType_Created</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Created</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set RecordType Created</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status</fullName>
        <field>Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Set Claim Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Pending</fullName>
        <field>Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Set Status to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Claim Pre Approved</fullName>
        <actions>
            <name>Claim_Has_been_Pre_Approved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Claim__c.Status__c</field>
            <operation>equals</operation>
            <value>Pre Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Claim__c.Third_Party_Submitted__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Claims Ready for Assessment</fullName>
        <actions>
            <name>Claims_Ready_for_Assessment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Claim__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted,Pending</value>
        </criteriaItems>
        <description>Assign all claims not assigned to anyone with a claim status of submitted or pending to the queue &apos;Claims Ready for Assessment&apos;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Default Fields Cloning</fullName>
        <actions>
            <name>Clear_Client_Authorised</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Client_Certification</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Date_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Claim_Status_Created</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_RecordType_Created</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Used for testing in DEV, not used elsewhere</description>
        <formula>ISCLONE() &amp;&amp; ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Status to Pending</fullName>
        <actions>
            <name>Set_Status_to_Pending</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Claim__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Claim__c.Assigned_To__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When a claim is assigned to a user then set the status to Pending if the claim has been submitted</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
