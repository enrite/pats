<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ReSet_Total_Approved_to_Pay1</fullName>
        <field>Total_Approved_to_Pay__c</field>
        <name>ReSet Total Approved to Pay</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Approved</fullName>
        <field>Claim_Item_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Created</fullName>
        <field>Claim_Item_Status__c</field>
        <literalValue>Created</literalValue>
        <name>Set Status Created</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Pending</fullName>
        <field>Claim_Item_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Set Status Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Rejected</fullName>
        <field>Claim_Item_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Created</fullName>
        <field>Claim_Item_Status__c</field>
        <literalValue>Created</literalValue>
        <name>Set Status to Created</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Total_Approved_to_Pay</fullName>
        <field>Total_Approved_to_Pay__c</field>
        <formula>IF(   ISPICKVAL(Claim_Item_Status__c,&quot;Approved&quot; ) ,

IF( RecordType.Name = &quot;Accommodation&quot;,  Total_to_Pay__c ,  Total_Travel_Payable__c )
,
0)</formula>
        <name>Set Total Approved to Pay</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Total_Approved_to_Pay1</fullName>
        <field>Total_Approved_to_Pay__c</field>
        <formula>IF( RecordType.Name = &quot;Accommodation&quot;, Total_to_Pay__c , Total_Travel_Payable__c )</formula>
        <name>Set Total Approved to Pay1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Total_Approved_to_Pay_Recall</fullName>
        <field>Total_Approved_to_Pay__c</field>
        <name>Set Total Approved to Pay - Recall</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Total_Approved_to_Pay_Rejected</fullName>
        <field>Total_Approved_to_Pay__c</field>
        <name>Set Total Approved to Pay - Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Total_Approved_to_Pay_v2</fullName>
        <field>Total_Approved_to_Pay__c</field>
        <formula>IF( RecordType.Name = &quot;Accommodation&quot;, Total_to_Pay__c , Total_Travel_Payable__c )</formula>
        <name>Set Total Approved to Pay</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Set Claim Item Status</fullName>
        <actions>
            <name>Set_Status_to_Created</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Total Approved to Pay</fullName>
        <actions>
            <name>Set_Total_Approved_to_Pay</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
